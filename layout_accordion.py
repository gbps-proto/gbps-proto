


	def get_accordion_height(n,current):
		global disable_draw_prs
		
		disable_draw_prs = True
		y = layout_accordion(n,current,0)
		disable_draw_prs = False
		
		return y



	def layout_accordion(n,current,y):
		global state
		
		x = xs[7] + axs[0]
		w = xs[8] - xs[7] - 2 * axs[0]
		
		def action_parse(arg):
			global request_parse,request_layout
			
			request_parse = True
			
			request_layout |= {0}
		
		def action_layout(arg):
			global request_layout
			
			if ctx1[ct]\
			and not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
				ctx1[ct]["highlighted"] = False
			
			request_layout |= {0}
		
		if n == 0: # Dateien / Tabs
			if current:
				y += ays[0]
				
				y += text_paragraph((x,y,w),blk,f1,u"Titel: %s" % titles[ct])
				
				y += blank(1)
				
				y += text_paragraph((x,y,w),blk,f1,u"Dateipfad: keiner")
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				
				def cont1((x,y,w),i,j):
					y_prev = y
					
					if j: y += ays[0] * 2
					
					# Tab speichern unter -> Tab öffnen + sha256 lookup
					# Tab zurücksetzen -> Änderung automatisch erkennen + Rückfrage
					
					if j == 0 and i == 0:
						y += button((x,y,w),u"Neuer Tab",actionNew)
					elif j == 0 and i == 1:
						y += button((x,y,w),u"Tab öffnen",actionOpen)
					elif j == 1 and i == 0:
						y += button((x,y,w),u"Tab speichern",actionSave)
					elif j == 1 and i == 1:
						y += button((x,y,w),u"Alle Tabs speichern",actionSaveAll)
					elif j == 2 and i == 0:
						y += button((x,y,w),u"Tab schließen",actionClose)
					elif j == 2 and i == 1:
						y += button((x,y,w),u"Tab wiederherstellen",actionRestore)
					elif j == 3 and i == 0:
						y += button((x,y,w),u"Titel festlegen",actionTitle)
					elif j == 3 and i == 1:
						y += button((x,y,w),u"Einstellungen",actionSettings)
					
					return y - y_prev
				
				
				y += table((x,y,w),(-1,-1),((cont1,) * 2,) * 4)
				
				y += ays[0]
			else:
				y += text_paragraph((x,y,w),blk,f1,u"Dateipfad: keiner")
		
		elif n == 1: # Abfrage
			if current:
				y += ays[0]
				
				labels = (
					u"Division durch eine Variable",
					u"Datenzugriff mit einer Variablen als Schlüssel",
					u"Vergleich mit einem Literal",
					u"Funktionsaufruf innerhalb einer Schleife",
				)
				
				name = u"Abfrage.choose"
				if name not in state: state[name] = {}
				
				for i,label in enumerate(labels):
					if i: y += blank(1)
					
					if i not in state[name]: state[name][i] = 0
					y += choose((x,y,w),name,i,label,False,action = action_layout)
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				y += text_paragraph((x,y,w),blk,f1,u"Tabelle mit Ergebnissen")
				
				y += ays[0]
			else:
				y += text_paragraph((x,y,w),gr1,f1,u"Keine Abfrage ausgewählt")
		
		elif n == 2: # Umschreiben / Generieren
			if current:
				y += ays[0]
				
				name = u"Umschreiben.active"
				if name not in state: state[name] = 1
				y += switch((x,y,w),name,u"Umschreiben aktivieren",action = action_parse)
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				labels = (
					u"Spezielle Formatierung anwenden",
					u"Fehlertoleranz für bestimmte Konstrukte einfügen",
					u"Domänenspezifische Befehle umsetzen",
				)
				
				name = u"Umschreiben.choose"
				if name not in state: state[name] = {}
				
				for i,label in enumerate(labels):
					if i: y += blank(1)
					
					if i not in state[name]: state[name][i] = 0
					y += choose((x,y,w),name,i,label,action = action_parse,enabled = state[u"Umschreiben.active"])
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				y += text_paragraph((x,y,w),blk,f1,u"Tabellen mit umgeschriebenen Stellen")
				
				y += ays[0]
			else:
				y += text_paragraph((x,y,w),gr1,f1,u"Umschreiben deaktiviert")
		
		elif n == 3: # Merkmale
			if current:
				y += ays[0]
				
				features = [None] * 3
				
				for i in xrange(len(features)):
					features[i] = u"Merkmal %d %s" % (i + 1,u" Extra" * i) #TODO
				
				occurrences = (
					[[0,0,1,],None],
					[[1,0,1,],None],
					[[1,1,0,],None],
				)
				
				for j,occurrence in enumerate(occurrences):
					occurrence[1] = u"Vorkommen %d %s" % (j + 1,u" Extra" * j) #TODO
				
				y += feature_table((x,y,w),features,occurrences)
				
				y += ays[0]
			else:
				
				
				def cont1((x,y,w),i,j):
					y_prev = y
					
					if j: y += blank(0)
					
					if j == 0 and i == 0:
						y += text_paragraph((x,y,w),blk,f1,u"Schleifen:")
					elif j == 0 and i == 1:
						y += text_paragraph((x,y,w),blk,f1,u"Arithmetik:")
					elif j == 0 and i == 2:
						y += text_paragraph((x,y,w),blk,f1,u"Datenzugriffe:")
					elif j == 1 and i == 0:
						y += text_paragraph((x,y,w),blk,f1,u"777")
					elif j == 1 and i == 1:
						y += text_paragraph((x,y,w),blk,f1,u"888")
					elif j == 1 and i == 2:
						y += text_paragraph((x,y,w),blk,f1,u"999")
					
					return y - y_prev
				
				
				y += table((x,y,w),(-1,-1,-1),((cont1,) * 3,) * 2)
		
		elif n == 4: # Konfigurieren
			if current:
				y += ays[0]
				
				name = u"Konfigurieren.active"
				if name not in state: state[name] = 1
				y += switch((x,y,w),name,u"Konfigurationen anwenden",action = action_parse)
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				labels = (
					u"Fehlertoleranz für Datenzugriffe einfügen",
					u"Toleranz bei Vergleich von Gleitkommazahlen",
					u"Algorithmen unterbrechbar machen",
				)
				
				name = u"Konfigurieren.choose"
				if name not in state: state[name] = {}
				
				for i,label in enumerate(labels):
					if i: y += blank(1)
					
					if i not in state[name]: state[name][i] = 0
					y += choose((x,y,w),name,i,label,action = action_parse,enabled = state[u"Konfigurieren.active"])
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				y += text_paragraph((x,y,w),blk,f1,u"Tabellen mit Anwendungen der Konfigurationen")
				
				y += ays[0]
			else:
				y += text_paragraph((x,y,w),gr1,f1,u"Konfiguration deaktiviert")
		
		elif n == 5: # Richtlinien
			
			values = (
				[5,6,1],
				[2,5,1],
				[1,200,1],
			)
			
			if current:
				y += ays[0]
				
				name = u"Richtlinien.choose"
				if name not in state: state[name] = {}
				
				for i in xrange(3):
					if i: y += blank(1)
					
					if i not in state[name]: state[name][i] = 0
					y += choose((x,y,w),name,i,u"Richtlinie %d" % (i + 1),action = action_layout) #TODO
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				for i,value in enumerate(values):
					if i: y += blank(2)
					
					y += pie_chart((x,y,w),u"Vorkommen nach Richtlinie %d" % (i + 1),value) #TODO
				
				y += ays[0]
			else:
				y += hbar_chart((x,y,w),[0,0,sum(float(n) / d if d > 0 else v for (n,d,v) in values) / len(values)])
		
		elif n == 6: # Code-Schemata
			
			values = (
				[5,6,1],
				[2,5,1],
				[1,200,1],
			)
			
			if current:
				y += ays[0]
				
				name = u"Schemata.choose"
				if name not in state: state[name] = {}
				
				for i in xrange(3):
					if i: y += blank(1)
					
					if i not in state[name]: state[name][i] = 0
					y += choose((x,y,w),name,i,u"Schema %d" % (i + 1),action = action_layout) #TODO
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				for i,value in enumerate(values):
					if i: y += blank(2)
					
					y += pie_chart((x,y,w),u"Vorkommen nach Schema %d" % (i + 1),value) #TODO
				
				y += ays[0]
			else:
				y += hbar_chart((x,y,w),[0,0,sum(float(n) / d if d > 0 else v for (n,d,v) in values) / len(values)])
		
		elif n == 7: # Orthogonale Bereiche
			if current:
				y += ays[0]
				
				
				def action1(arg):
					pass # TODO
				
				
				for i in xrange(2):
					if i: y += blank(1)
					
					#TODO Auswahl aus Gruppe %d entfernen
					y += button((x,y,w),u"Auswahl zu Gruppe %d hinzufügen" % (i + 1),action1,i)
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				for i in xrange(2):
					if i: y += blank(1)
					
					name = u"Orthogonale Bereiche.placeholder %d" % i
					if name not in state: state[name] = 0
					y += switch((x,y,w),name,u"Platzhalter für Gruppe %d aktivieren" % (i + 1),action = action_layout)
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				y += text_paragraph((x,y,w),blk,f1,u"Tabelle mit Vorkommen von Bereichen in Gruppe 1")
				y += blank(1)
				y += text_paragraph((x,y,w),blk,f1,u"Tabelle mit Vorkommen von Bereichen in Gruppe 2")
				
				y += ays[0]
			else:
				
				
				def cont1((x,y,w),i,j):
					y_prev = y
					
					if j: y += blank(0)
					
					if j == 0 and i == 0:
						y += text_paragraph((x,y,w),blk,f1,u"Gruppe 1:")
					elif j == 0 and i == 1:
						y += text_paragraph((x,y,w),blk,f1,u"Gruppe 2:")
					elif j == 1 and i == 0:
						y += text_paragraph((x,y,w),blk,f1,u"0")
					elif j == 1 and i == 1:
						y += text_paragraph((x,y,w),blk,f1,u"0 (Platzhalter)")
					
					return y - y_prev
				
				
				y += table((x,y,w),(-1,-1),((cont1,) * 2,) * 2)
		
		elif n == 8: # Komplexitätsklassen
			if current:
				y += ays[0]
				
				y += text_paragraph((x,y,w),blk,f1,u"Keine Schleife ausgewählt")
				
				y += ays[0]
			else:
				y += text_paragraph((x,y,w),gr1,f1,u"Keine Schleife ausgewählt")
		
		elif n == 9: # Datenfluss
			if current:
				y += ays[0]
				
				y += text_paragraph((x,y,w),blk,f1,u"Keine Variable ausgewählt")
				
				y += ays[0]
			else:
				y += text_paragraph((x,y,w),gr1,f1,u"Keine Variable ausgewählt")
		
		elif n == 10: # Erläuterung des Programmcodes
			if current:
				y += ays[0]
				
				y += text_paragraph((x,y,w),blk,f1,u"Keine Beschreibung verfügbar")
				
				y += ays[0]
			else:
				y += text_paragraph((x,y,w),gr1,f1,u"Keine Beschreibung verfügbar")
		
		elif n == 11: # Knoten-Eigenschaften
			y += ays[0]
			
			x_prev = x
			
			colorss = [[blk,gr1,gr2,gr3,grn,org,red,yl1,yl2,bl1,bl2,bl3,bl4],[(.8,0,.2),(.9,.6,0),(0,.6,0),(.25,.5,1),blk,(.09,.53,.62),(.67,.48,.13),(.1,.3,.6),(.85,.8,.72),(.25,.5,1),(.7,.8,1)]]
			for colors in colorss:
				for color in colors:
					fcirc(x + 5,y + 5,5,color)
					x += 20
				
				x = x_prev
				y += 20
			
			y -= 10
			
			y += ays[0]
		
		elif n == 12: # Über dieses Programm
			if current:
				y += ays[0]
				
				y += text_paragraph((x,y,w),blk,f1,u"Gbps-proto ist ein Prototyp des graphgestützten Programmiersystems, einem Werkzeugkasten zur Herstellung leistungsfähigerer Software. Die enthaltenen Funktionen geben einen Ausblick auf die Möglichkeiten dieser neuen Technologie. Es handelt sich um kein vollständiges Produkt, insbesondere sind weder Datenbank noch Programmanalyse enthalten. Viele der enthaltenen Werkzeuge funktionieren nur im Rahmen dieser Demonstration.")
				
				y += blank(1)
				
				y += text_paragraph((x,y,w),blk,f1,u"Bitte verwenden Sie gbps-proto nicht im produktiven Umfeld.")
				
				y += ays[0] * 2
				y += hline((x,y,w))
				y += ays[0] * 2
				
				
				def action1(arg):
					webbrowser.open("mailto:aaron.lorey@gmail.com",new = 2)
				
				
				y += button((x,y,w),u"E-Mail an den Autor senden",action1)
				
				y += ays[0] * 2
				
				
				def action1(arg):
					webbrowser.open("https://drive.google.com/drive/folders/1Hpoeb2pOeTGcAD1XpKgKiNcjKz2dut8P",new = 2)
				
				
				y += button((x,y,w),u"Informationsmaterial zur Geschäftsidee",action1)
				
				y += ays[0]
			else:
				pass
		
		elif n == 13: # Alle Reiter schließen
			y += ays[0]
			
			y += text_paragraph((x,y,w),blk,f1,u"Wählen Sie diesen Reiter, um alle anderen Reiter zu schließen und so alle Kurz-Infos im Blick zu haben.")
			
			y += ays[0]
		
		elif n == 14: # Interaktive Ausführung
			if current:
				pass
			else:
				pass
		
		elif n == 15: # Datei-Verlauf
			
			# Dateien (Pfad, akt. sha256, anzahl Snapshots, Speicherplatz) -> Snapshots (Datum, Speicherplatz)
			
			# Snapshot löschen
			# alle Snapshots löschen  1x confirm
			
			if current:
				pass
			else:
				pass
		
		elif n == 16: # Knoten-Navigator
			if current:
				y += ays[0]
				
				if "no parse" in files[ct][3] and files[ct][3]["no parse"]\
				or not ast_selected1[1]:
					
					y += text_paragraph((x,y,w),blk,f1,u"Kein Knoten ausgewählt")
				
				else:
					
					
					def action1(line):
						global request_layout
						
						if 4 < len(line)\
						and line[4]:
							
							ast_selected1[1] = line[4]
							pos = ctx1[ct]["positions"][id(line[4])][0]
							
							select = selects[ct]
							
							select[:] = [[pos[0],pos[1],pos[1]],[pos[0],pos[1],pos[1]]]
							
							validate_caret(0)
							validate_caret(1)
							
							request_layout |= {0}
					
					
					siblings = ((),())
					if id(ast_selected1[1]) in ctx1[ct]["parents"]:
						parent = ctx1[ct]["parents"][id(ast_selected1[1])]
						index = tuple(i for i,z in enumerate(parent[1]) if z is ast_selected1[1]) or (0,)
						siblings = (
							tuple(node for node in parent[1][:index[0]] if isinstance(node,list) and node[0][0] not in {"comment","whitespace","keyword"}),
							tuple(node for node in parent[1][index[0] + 1:] if isinstance(node,list) and node[0][0] not in {"comment","whitespace","keyword"}),
						)
					children = tuple(node for node in ast_selected1[1][1] if isinstance(node,list) and node[0][0] not in {"comment","whitespace","keyword"})
					
					path = list(reversed(ast_ancestor(ast_selected1[1],(),ctx1[ct])))
					if path and path[0] == ctx1[ct]["pars"][0]: path = path[1:]
					path = path[:-1]
					
					
					liness = []
					
					for i,node in enumerate(path):
						liness += [[0,1,f1,ast_name(node,ctx1[ct]),node]]
					
					if path:
						liness += [[-1]]
					
					for node in siblings[0]:
						liness += [[1,1,f1,ast_name(node,ctx1[ct]),node]]
					
					if siblings[0]:
						liness += [[-1]]
					
					liness += [[1,1,f2,ast_name(ast_selected1[1],ctx1[ct]),ast_selected1[1]]]
					
					if children:
						liness += [[-1]]
					
					for node in children:
						liness += [[2,1,f1,ast_name(node,ctx1[ct]),node]]
					
					if siblings[1]:
						liness += [[-1]]
					
					for node in siblings[1]:
						liness += [[1,1,f1,ast_name(node,ctx1[ct]),node]]
					
					y += indent((x,y,w),liness,True,action = action1)
				
				y += ays[0]
			else:
				pass
		
		elif n == 17: # Knoten-Liste
			if current:
				pass
			else:
				pass
		
		return y


