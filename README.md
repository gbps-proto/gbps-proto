&nbsp;


# Running gbps-proto

This is a step-by-step guide for running gbps-proto.


&nbsp;


### Step 1: Download the repository

Download the repository as a ZIP file: [https://gitlab.com/gbps-proto/gbps-proto/-/archive/master/gbps-proto-master.zip](https://gitlab.com/gbps-proto/gbps-proto/-/archive/master/gbps-proto-master.zip)

### Step 2: Enable Windows Subsystem for Linux (WSL)

1. Open the Start Menu and search **Turn Windows features on or off**

2. Select **Windows Subsystem for Linux**

3. Click OK

4. Restart your computer when prompted

### Step 3: Extract the ZIP file

Right-click **gbps-proto-master.zip** in your Downloads folder, select **Extract All...** and follow the instructions.

### Step 4: Set up a Python environment

Go to the newly created folder, right-click **set_up_python_environment.ps1** and select **Run with PowerShell**.

### Step 5: Run gbps-proto

Right-click **run_gbps_proto.ps1** and select **Run with PowerShell**.


&nbsp;


# Ausführen von gbps-proto

Dies ist eine Schritt-für-Schritt-Anleitung zum Ausführen von gbps-proto.


&nbsp;


### Schritt 1: Laden Sie das Repository herunter

Laden Sie das Repository als ZIP-Datei herunter: [https://gitlab.com/gbps-proto/gbps-proto/-/archive/master/gbps-proto-master.zip](https://gitlab.com/gbps-proto/gbps-proto/-/archive/master/gbps-proto-master.zip)

### Schritt 2: Aktivieren Sie das Windows-Subsystem für Linux (WSL)

1. Öffnen Sie das Start-Menü und suchen Sie **Windows-Features aktivieren oder deaktivieren**

2. Wählen Sie **Windows-Subsystem für Linux**

3. Klicken Sie auf OK

4. Starten Sie Ihren Computer neu, sobald Sie dazu aufgefordert werden

### Schritt 3: Extrahieren Sie die ZIP-Datei

Klicken Sie mit rechts auf **gbps-proto-master.zip** in Ihrem Download-Ordner, wählen Sie **Alle extrahieren...** und folgen Sie den Anweisungen.

### Schritt 4: Richten Sie eine Python-Umgebung ein

Öffnen Sie den neu erstellten Ordner, klicken Sie mit rechts auf **set_up_python_environment.ps1** und wählen Sie **Mit PowerShell ausführen**.

### Schritt 5: Führen Sie gbps-proto aus

Klicken Sie mit rechts auf **run_gbps_proto.ps1** und wählen Sie **Mit PowerShell ausführen**.


&nbsp;

