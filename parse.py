


# token types:
#
# comment
# whitespace
# literal
# keyword
# identifier
# brackets
# statement
# mark
# prop group
#


# statement types:
#
# expression
# control (contains python structure)
# structure (java only)
#



match_statement = lambda statement,k: ("t",statement,k)

match_list = lambda pattern,k: ("p",(
	pattern,
	("*",(
		",",
		pattern,
	)),
),k)

match_expression = lambda k: ("|",(
	(
		match_statement(("statement","expression"),k),
	),
	(
		match_statement(("literal",),k),
	),
	(
		match_statement(("identifier",),k),
	),
	(
		match_statement(("brackets","("),k),
	),
	(
		match_statement(("brackets","{"),k),
	),
	(
		match_statement(("brackets","indentation"),k),
	),
))



def op_unary_pre(op):
	return [("op",op,"pre"),(op,match_expression("x")),{"no newline","no preceding expression"}]



def ops_unary_pre(ops,extra = []):
	statements = []
	for op in ops:
		statements += [op_unary_pre(op)]
	
	return ["rtl",statements + extra]



def op_unary_post(op):
	return [("op",op,"post"),(match_expression("x"),op),{"no newline","no succeeding expression"}]



def ops_unary_post(ops,extra = []):
	statements = []
	for op in ops:
		statements += [op_unary_post(op)]
	
	return ["ltr",statements + extra]



def op_binary(op):
	return [("op",op,"binary"),(match_expression("x"),op,match_expression("x"))]



def ops_binary(ops,extra = []):
	statements = []
	for op in ops:
		statements += [op_binary(op)]
	
	return ["ltr",statements + extra]



def in_1(ins):
	return [(ins,),(ins,match_expression("x"))]



def in_01(ins):
	return [(ins,),(ins,("?",(match_expression("x"),))),{"no newline"}]



def java_declaration(visibilities,modifiers,types):
	statements = []
	
	pattern1 = []
	pattern1 += [("*",(("k",visibilities,"visibilities"),))]
	pattern1 += [("*",(("k",modifiers,"modifiers"),))]
	
	# variable
	
	pattern2 = []
	pattern2 += pattern1
	pattern2 += [("|",((("k",types,"type"),),(match_statement(("identifier",),"type"),)))]
	pattern2 += [match_list(("p",(("|",((match_statement(("statement","expression","op","="),"x"),),(match_statement(("identifier",),"x"),))),),None),None)]
	
	statements += [[("declaration","variable"),tuple(pattern2),{"no lookahead"}]]
	
	# method
	
	pattern2 = []
	pattern2 += pattern1
	pattern2 += [("?",(("|",((("k",types,"type"),),(match_statement(("identifier",),"type"),))),))]
	pattern2 += [match_statement(("statement","expression","("),"signature"),("?",("throws",match_list(("p",(match_statement(("identifier",),"throws"),),None),None))),match_statement(("brackets","{"),"{")]
	
	statements += [[("declaration","method"),tuple(pattern2),{"no lookahead"}]]
	
	# class
	
	pattern2 = []
	pattern2 += pattern1
	pattern2 += ["class",match_statement(("identifier",),"identifier"),("?",(match_statement(("brackets","<"),"generic"),)),("?",("extends",match_list(("p",(match_statement(("identifier",),"extends"),),None),None))),("?",("implements",match_list(("p",(match_statement(("identifier",),"implements"),),None),None))),match_statement(("brackets","{"),"{")]
	
	statements += [[("declaration","class"),tuple(pattern2)]]
	
	# interface
	
	pattern2 = []
	pattern2 += pattern1
	pattern2 += ["interface",match_statement(("identifier",),"identifier"),("?",("extends",match_list(("p",(match_statement(("identifier",),"extends"),),None),None))),match_statement(("brackets","{"),"{")]
	
	statements += [[("declaration","interface"),tuple(pattern2)]]
	
	# enum
	
	pattern2 = []
	pattern2 += pattern1
	pattern2 += ["enum",match_statement(("identifier",),"identifier"),match_statement(("brackets","{"),"{")]
	
	statements += [[("declaration","enum"),tuple(pattern2)]]
	
	return ["ltr",statements]



def parse(ctx,rewrite = False):
	global statement_names
	
	
	#ctx["text"]
	ctx["ast"] = None
	ctx["pars"] = [None]
	ctx["nodes"] = []
	ctx["types"] = {}
	ctx["parents"] = {}
	ctx["tplparams"] = {}
	ctx["positions"] = {}
	ctx["lines"] = None
	
	
	ast = ctx["text"]
	ast = re.split("([\\W\\d])",ast)
	ast = [x for x in ast if x]
	
	
	# reduce comments
	# reduce string literals (count escape)
	
	
	ast2 = []
	i = 0
	while i < len(ast):
		x = ast[i]
		
		if (\
			x == "#"\
		): # single line comment
		#or (\
		#	x == "/"\
		#	and i < len(ast) - 1\
		#	and ast[i + 1] == "/"\
		#):
			
			j = i + 1
			while j < len(ast):
				y = ast[j]
				
				if y == "\n":
					break
				
				j += 1
			
			ast2 += [[("comment",),["".join(ast[i:j])]]] # type, tokens
			i = j
		elif x == "/"\
		and i < len(ast) - 1\
		and ast[i + 1] == "*": # multi line comment
			
			j = i + 1
			while j < len(ast):
				y = ast[j]
				
				if y == "*"\
				and j < len(ast) - 1\
				and ast[j + 1] == "/":
					break
				
				j += 1
			
			ast2 += [[("comment",),["".join(ast[i:j + 2])]]] # type, tokens
			i = j + 2
		elif x == "\""\
		or x == "'": # string literal
			
			j = i + 1
			while j < len(ast):
				y = ast[j]
				
				if y == x:
					break
				
				if y == "\\":
					j += 1
				
				j += 1
			
			ast2 += [[("literal","string"),["".join(ast[i:j + 1])]]] # type, tokens
			i = j + 1
		#elif x == "\\"\
		#and i < len(ast) - 1\
		#and ast[i + 1] == "\n": # escape before newline
		#	i += 1
		else:
			ast2 += [x]
			i += 1
	
	ast = ast2
	
	
	# reduce whitespace
	# read escape character as whitespace
	
	
	ast2 = []
	i = 0
	while i < len(ast):
		x = ast[i]
		
		if not isinstance(x,list)\
		and re.match("[\\s\\\\]",x):
			
			j = i + 1
			while j < len(ast):
				y = ast[j]
				
				if isinstance(y,list)\
				or not re.match("[\\s\\\\]",y):
					break
				
				j += 1
			
			ast2 += [[("whitespace",),["".join(ast[i:j])],0]] # type, tokens, relative-indentation
			i = j
		else:
			ast2 += [x]
			i += 1
	
	ast = ast2
	
	
	# reduce number literals
	# reduce identifiers with digits
	
	
	ast2 = []
	i = 0
	while i < len(ast):
		x = ast[i]
		
		if not isinstance(x,list)\
		and re.match("[\\w\\.]",x,re.UNICODE):
			dots = [i - 1]
			is_number = False
			is_exp = False ## TODO NIY
			
			j = i
			k = 0
			while j < len(ast):
				y = ast[j]
				
				if isinstance(y,list)\
				or not re.match("[\\w\\.]",y,re.UNICODE):
					break
				
				if y == ".":
					dots += [j]
					k = -1
				elif not k\
				and not isinstance(y,list)\
				and re.match("\\d",y):
					is_number = True
				
				j += 1
				k += 1
			
			dots += [j]
			
			if is_number:
				ast2 += [[("literal","number"),["".join(ast[i:j])]]] # type, tokens
			else:
				for k in xrange(len(dots) - 1):
					if dots[k] + 1 < dots[k + 1]:
						ast2 += ["".join(ast[dots[k] + 1:dots[k + 1]])]
					
					ast2 += ["."]
				
				ast2.pop()
			
			i = j
		else:
			ast2 += [x]
			i += 1
	
	ast = ast2
	
	
	# reduce string literal prefixes
	
	
	ast2 = []
	i = 0
	while i < len(ast):
		x = ast[i]
		
		if not isinstance(x,list)\
		and re.match("\\w",x,re.UNICODE)\
		and i < len(ast) - 1\
		and isinstance(ast[i + 1],list)\
		and ast[i + 1][0][:2] == ("literal","string"):
			ast2 += [[ast[i + 1][0],[x + ast[i + 1][1][0]]]] # type, tokens
			i += 2
		else:
			ast2 += [x]
			i += 1
	
	ast = ast2
	
	
	# reduce operators (ltr)
	
	
	op_chrs2 = {
		"+": {"=","+"},
		"-": {"=","-",">"},
		"*": {"=","*"},
		"/": {"=","/"},
		"<": {"=","<"},
		">": {"=",">"},
		"&": {"=","&"},
		"|": {"=","|"},
		"!": {"="},
		"=": {"="},
		":": {"=",":"},
		"@": {"="},
		"%": {"="},
		"^": {"="},
	}
	op_chrs3 = {
		">": {
			">": {"=",">"},
		},
		"*": {
			"*": {"="},
		},
		"/": {
			"/": {"="},
		},
		"<": {
			"<": {"="},
		},
		"=": {
			"=": {"="},
		},
		".": {
			".": {"."},
		},
	}
	op_chrs4 = {
		">": {
			">": {
				">": {"="},
			},
		},
	}
	
	
	ast2 = []
	i = 0
	while i < len(ast):
		x = ast[i]
		
		if i < len(ast) - 3\
		and not isinstance(x,list)\
		and not isinstance(ast[i + 1],list)\
		and not isinstance(ast[i + 2],list)\
		and not isinstance(ast[i + 3],list)\
		and x in op_chrs4\
		and ast[i + 1] in op_chrs4[x]\
		and ast[i + 2] in op_chrs4[x][ast[i + 1]]\
		and ast[i + 3] in op_chrs4[x][ast[i + 1]][ast[i + 2]]:
			
			ast2 += ["".join(ast[i:i + 4])]
			i += 4
		elif i < len(ast) - 2\
		and not isinstance(x,list)\
		and not isinstance(ast[i + 1],list)\
		and not isinstance(ast[i + 2],list)\
		and x in op_chrs3\
		and ast[i + 1] in op_chrs3[x]\
		and ast[i + 2] in op_chrs3[x][ast[i + 1]]:
			
			ast2 += ["".join(ast[i:i + 3])]
			i += 3
		elif i < len(ast) - 1\
		and not isinstance(x,list)\
		and not isinstance(ast[i + 1],list)\
		and x in op_chrs2\
		and ast[i + 1] in op_chrs2[x]:
			
			ast2 += ["".join(ast[i:i + 2])]
			i += 2
		else:
			ast2 += [x]
			i += 1
	
	ast = ast2
	
	
	# reduce identifiers and other literals
	# reduce multiword keywords
	
	
	lits = {
		"false","none","true",
		"false","null","true",
	}
	keywords = {
		"and","as","assert","async","await","break","class",
		"continue","def","del","elif","else","except",
		"finally","for","from","global","if","import","in",
		"is","lambda","nonlocal","not","or","pass",
		"raise","return","try","while","with","yield",
		"abstract","assert","boolean","break","byte","case","catch","char","class","const",
		"continue","default","do","double","else","enum","extends","final","finally","float",
		"for","if","goto","implements","import","instanceof","int","interface","long","native",
		"new","package","private","protected","public","return","short","static","strictfp","super",
		"switch","synchronized","this","throw","throws","transient","try","void","volatile","while",
		"_","var","yield"
	}
	
	
	ast2 = []
	i = 0
	while i < len(ast):
		x = ast[i]
		
		if not isinstance(x,list)\
		and x.lower() in lits:
			ast2 += [[("literal","other"),[x]]] # type, tokens
			i += 1
		elif not isinstance(x,list)\
		and x.lower() in keywords:
			ast2 += [x]
			i += 1
		elif not isinstance(x,list)\
		and re.match("\\w",x,re.UNICODE):
			ast2 += [[("identifier",),[x],None]] # type, tokens, indentation
			i += 1
		else:
			ast2 += [x]
			i += 1
	
	ast = ast2
	
	
	keywords2 = {
		"not": {"in"},
		"is": {"not"},
	}
	
	
	ast2 = []
	i = 0
	while i < len(ast):
		x = ast[i]
		
		if i < len(ast) - 2\
		and not isinstance(x,list)\
		and isinstance(ast[i + 1],list)\
		and not isinstance(ast[i + 2],list)\
		and ast[i + 1][0][:1] == ("whitespace",)\
		and x.lower() in keywords2\
		and ast[i + 2].lower() in keywords2[x.lower()]:
			
			ast2 += [[("keyword"," ".join((x,ast[i + 2]))),ast[i:i + 3]]] # type, tokens
			i += 3
		else:
			ast2 += [x]
			i += 1
	
	ast = ast2
	
	
	# reduce matching brackets and indentation (ltr, top-down)
	
	
	i = 0
	curr_ind = 0
	while i < len(ast):
		x = ast[i]
		
		if isinstance(x,list)\
		and x[0][:1] == ("whitespace",)\
		and (not i or x[1][0].count("\n")):
			match = re.search("(\\t*)[^\\n\\t]*\\Z",x[1][0])
			ind = len(match.group(1))
			
			if ind - curr_ind:
				ast[i][2] = ind - curr_ind
			
			curr_ind = ind
		
		i += 1
	
	
	par_chrs = {
		"<": ">",
		"{": "}",
		"(": ")",
		"[": "]",
	}
	
	
	def reduce_pars(ast,ctx_pars):
		
		ast2 = []
		i = 0
		while i < len(ast):
			x = ast[i]
			
			if x == "<": # TODO  f(g<a,b>(c)) vs f((g<a),(b>c))
				
				j = i + 1
				depth = 1
				while j < len(ast):
					y = ast[j]
					
					if y == x:
						depth += 1
					elif y == par_chrs[x]:
						depth -= 1
					elif y != ","\
					and not isinstance(y,list)\
					and y[0][:1] != ("whitespace",)\
					and y[0][:1] != ("identifier",):
						break
					
					j += 1
					
					if depth <= 0:
						break
				
				if depth <= 0:
					ast3 = [("brackets",x),[ast[i]] + reduce_pars(ast[i + 1:j],ctx_pars)] # type, tokens
					ctx_pars += [ast3]
					ast2 += [ast3]
					i = j
				else:
					ast2 += [x]
					i += 1
			elif not isinstance(x,list)\
			and x in par_chrs:
				
				j = i + 1
				depth = 1
				while j < len(ast):
					y = ast[j]
					
					if y == x:
						depth += 1
					elif y == par_chrs[x]:
						depth -= 1
					
					j += 1
					
					if depth <= 0:
						break
				
				ast3 = [("brackets",x),[ast[i]] + reduce_pars(ast[i + 1:j],ctx_pars)] # type, tokens
				ctx_pars += [ast3]
				ast2 += [ast3]
				i = j
			elif isinstance(x,list)\
			and x[0][:1] == ("whitespace",)\
			and x[2] > 0:
				
				j = i + 1
				depth = x[2]
				while j < len(ast):
					y = ast[j]
					
					if isinstance(y,list)\
					and y[0][:1] == ("whitespace",):
						depth += y[2]
					
					j += 1
					
					if depth <= 0:
						break
				
				ast3 = [("brackets","indentation"),[ast[i]] + reduce_pars(ast[i + 1:j],ctx_pars)] # type, tokens
				ctx_pars += [ast3]
				ast2 += [ast3]
				i = j
			else:
				ast2 += [x]
				i += 1
		
		return ast2
	
	
	ast = reduce_pars(ast,ctx["pars"])
	ctx["pars"][0] = [("brackets","{"),ast] # type, tokens
	
	
	# reduce statements (ltr and rtl, with precedence, bottom-up)
	
	
	# TODO performance: keyword look ahead before statement -> match_pattern()
	# TODO use "statement in no_keyword or statement in next_keyword"
	
	
	statementsss = [
		["expression",[
			["ltr",[
				[("[",),(match_expression("x"),match_statement(("brackets","["),"x")),{"no lookahead"}],
				[("(",),(match_expression("x"),match_statement(("brackets","("),"x")),{"no lookahead"}], # TODO (a)(b)c
				[(".",),(match_expression("x"),".",match_statement(("identifier",),"identifier"))],
			]],
			ops_unary_pre([
				"await",
			]),
			ops_unary_post([
				"++","--", # special case
			]),
			ops_unary_pre([
				"++","--",
			]),
			ops_binary([
				"**",
			]),
			ops_unary_pre([
				"+","-", #TODO TEST
				"~","!",
				"new",
			],[
				[("cast",),(match_statement(("brackets","("),"type"),match_expression("x")),{"no newline","no lookahead"}], # special case
			]),
			ops_binary([
				"*","@","/","//","%",
			]),
			ops_binary([
				"+","-",
			]),
			ops_binary([
				"<<",">>",">>>",
			]),
			ops_binary([ #TODO TEST
				#"<","<=",">",">=",
				#"instanceof",
			]),
			ops_binary([ #TODO TEST
				"!=","==",
			]),
			ops_binary([
				"&",
			]),
			ops_binary([
				"^",
			]),
			ops_binary([
				"|",
			]),
			ops_binary([
				"in","not in","is","is not",
				"<","<=",">",">=","!=","==",
				"instanceof",
			]),
			ops_unary_pre([
				"not",
			]),
			ops_binary([
				"and",
				"&&",
			]),
			ops_binary([
				"or",
				"||",
			]),
			["ltr",[
				[("lambda","py"),("lambda",match_expression("signature"),":",match_expression("x"))],
			]],
			["rtl",[
				[("if","java"),(match_expression("if"),"?",match_expression("x"),":",match_expression("x"))],
			]],
			["ltr",[
				[("if","py"),(match_expression("x"),("+",("if",match_expression("if"),"else",match_expression("x")))),{"no newline"}], # special case
			]],
			["ltr",[
				[("for",),(match_expression("x"),("+",(("p",("for",match_statement(("statement","expression","op","in"),"x"),("*",("if",match_expression("if")))),"for"),))),{"no newline"}], # special case
			]],
			ops_binary([
				":=",
			]),
			ops_binary([
				"=",
				"**=",
				"*=","@=","/=","//=","%=",
				"+=","-=",
				"<<=",">>=",">>>=",
				"&=",
				"^=",
				"|=",
			]),
			["ltr",[
				[("lambda","java"),(match_expression("signature"),"->",match_expression("x"))],
			]],
		]],
		["control",[
			["ltr",[
				[("import",),(("?",("from",match_expression("from"))),"import",("|",((match_list(("p",(match_expression("x"),("?",("as",match_expression("as")))),"x"),None),),("*",))))],
			]],
			["rtl",[
				# as
				[("assert",),("assert",match_list(("p",(match_expression("x"),("?",(":",match_expression(":")))),"x"),None))],
				in_1("async"),
				in_01("break"), # special case
				[("declaration","class"),("class",match_expression("x"),":",match_expression(":"))],
				in_01("continue"), # special case
				[("declaration","method"),("def",match_statement(("statement","expression","("),"signature"),("?",("->",match_expression("->"))),":",match_expression(":"))],
				in_1("del"),
				# elif
				# else
				# except
				# finally
				[("for","py"),("for",match_statement(("statement","expression","op","in"),"x"),":",match_expression("do"),("?",("else",":",match_expression("do"))))],
				[("global",),("global",match_list(("p",(match_expression("x"),),None),None))],
				[("if","py"),("if",match_expression("x"),":",match_expression("do"),("*",("elif",match_expression("x"),":",match_expression("do"))),("?",("else",":",match_expression("do"))))],
				[("nonlocal",),("nonlocal",match_list(("p",(match_expression("x"),),None),None))],
				[("pass",),("pass",)],
				[("raise",),("raise",match_expression("x"),("?",("from",match_expression("from"))))],
				in_01("return"), # special case
				[("try","py"),("try",":",match_expression("do"),("*",(("p",("except",("?",(match_expression("x"),("?",("as",match_expression("as"))))),":",match_expression("do")),"except"),)),("?",("else",":",match_expression("else"))),("?",("finally",":",match_expression("finally"))))],
				[("while","py"),("while",match_expression("x"),":",match_expression("do"),("?",("else",":",match_expression("do"))))],
				[("with",),("with",match_list(("p",(match_expression("x"),("?",("as",match_expression("as")))),"x"),None),":",match_expression("do"))],
				in_1("yield"),
				[("decorated",),(("+",(match_statement(("decorator",),"decorator"),)),match_expression("x")),{"no lookahead"}],
				#[("labeled",),(match_statement(("identifier",),"identifier"),":",match_expression(":"))],
				
				
				# assert
				# break
				#[("case",),("case",match_list(("p",(match_expression("x"),),None),None),":",match_expression("do"))], #TODO
				#[("case","default"),("default",":",match_expression("do"))], #TODO
				# catch
				# class
				# continue
				# else
				# enum
				# finally
				[("for","java"),("for",match_statement(("brackets","("),"x"),match_expression("do"))],
				in_1("goto"),
				[("if","java"),("if",match_statement(("brackets","("),"x"),match_expression("do"),("*",("elif",match_statement(("brackets","("),"x"),match_expression("do"))),("?",("else",match_expression("do"))))],
				# import
				# interface
				in_1("package"), #TODO
				# return
				# super
				[("switch",),("switch",match_statement(("brackets","("),"x"),match_statement(("brackets","{"),"do"))],
				[("synchronized",),("synchronized",match_statement(("brackets","("),"x"),match_statement(("brackets","{"),"do"))],
				# this
				in_1("throw"),
				#[("try","java"),("try",("?",(match_statement(("statement","structure","declaration","variable"),"x"),("*",(";",match_statement(("statement","structure","declaration","variable"),"x"))))),match_statement(("brackets","{"),"do"),("*",("catch",match_statement(("brackets","("),"x"),match_statement(("brackets","{"),"do"))),("?",("finally",match_statement(("brackets","{"),"do"))))], #TODO
				[("while","java"),("while",match_statement(("brackets","("),"x"),match_expression("do"))],
				[("while","do"),("do",match_expression("do"),"while",match_statement(("brackets","("),"x"))],
				# _
				# decorated
				# labeled
			]],
		]],
		["structure",[
			java_declaration(
			
			# extends
			# implements
			# throws
			
			{
				#"default",
				"private",
				"protected",
				"public",
			},
			{
				"abstract",
				"const",
				"final",
				"native",
				"static",
				"strictfp",
				"synchronized",
				"transient",
				"volatile",
			},
			{
				"boolean",
				"byte",
				"char",
				"double",
				"float",
				"int",
				"long",
				"short",
				"var",
				"void",
			}),
		]],
	]
	
	statementsss_properties = {
	}
	
	
	statement_names = {}
	
	for statementss in statementsss:
		for statements in statementss[1]:
			for statement in statements[1]:
				
				statement_names[statement[0]] = statement
				
				assert len(statement) >= 2
				if len(statement) <= 2:
					statement += [set()]
				statement += [set()]
				
				
				def traverse_recur(statement,pattern):
					
					for y in pattern:
						if isinstance(y,tuple):
							if y[0] in {"p","?","*","+"}:
								traverse_recur(statement,y[1])
							elif y[0] == "|":
								for z in y[1]:
									traverse_recur(statement,z)
							elif y[0] == "k":
								statement[3] |= y[1]
						else:
							statement[3] |= {y}
				
				traverse_recur(statement,statement[1])
	
	
	#print statement_names
	
	
	def accept_props(prop,prop2):
		for k in prop2:
			if k not in prop:
				prop[k] = []
			prop[k] += prop2[k]
		prop2.clear()
	
	
	def append_props(prop,prop2,k):
		if not k:
			accept_props(prop,prop2)
		else:
			if k not in prop:
				prop[k] = []
			prop[k] += [[("prop group",),None,prop2]] # type, None, props
			#prop2.clear()
	
	
	def match_pattern(ast,i,statement,pattern,prop):
		
		
		if "no preceding expression" in statement[2]:
			j = i - 1
			while j >= 0\
			and isinstance(ast[j],list)\
			and ast[j][0][0] in {"comment","whitespace"}: # no preceding expression: skip comment and whitespace
				
				if "no newline" in statement[2]\
				and ast[j][0][:1] == ("whitespace",)\
				and ast[j][1][0].count("\n")\
				and (ast[j][1][0][:1] == "\n" or re.search("[^\\\\]\n",ast[j][1][0])): # no preceding expression: do not skip newline
					break
				
				j -= 1
			
			if j >= 0\
			and isinstance(ast[j],list)\
			and ast[j][0][0] in {"statement","literal","identifier","brackets"}:
				return -1
		
		
		i_noskip = i
		j = 0
		while i < len(ast)\
		and j < len(pattern):
			
			x = ast[i]
			y = pattern[j]
			
			
			if "no newline" in statement[2]\
			and isinstance(x,list)\
			and x[0][:1] == ("whitespace",)\
			and x[1][0].count("\n")\
			and (x[1][0][:1] == "\n" or re.search("[^\\\\]\n",x[1][0])): # skip optional or reject
				break
			
			
			if isinstance(x,list)\
			and x[0][0] in {"comment","whitespace"}: # skip comment and whitespace
				i += 1
				continue
			
			
			if isinstance(x,list)\
			and x[0][:1] == ("keyword",):
				x = x[0][1]
			
			
			if isinstance(y,tuple):
				prop2 = {}
				
				if y[0] == "p":
					k = match_pattern(ast,i,statement,y[1],prop2)
					if k < 0:
						return -1
					i = k
					append_props(prop,prop2,y[2])
					i -= 1
				elif y[0] == "?":
					k = match_pattern(ast,i,statement,y[1],prop2)
					if k >= 0:
						i = k
						accept_props(prop,prop2)
					i -= 1
				elif y[0] == "|":
					k = 0
					for z in y[1]:
						k = match_pattern(ast,i,statement,z,prop2)
						if k >= 0:
							i = k
							accept_props(prop,prop2)
							break
					if k < 0:
						return -1
					i -= 1
				elif y[0] == "*":
					k = i
					while k >= 0:
						i = k
						accept_props(prop,prop2)
						k = match_pattern(ast,i,statement,y[1],prop2)
					i -= 1
				elif y[0] == "+":
					k = match_pattern(ast,i,statement,y[1],prop2)
					if k < 0:
						return -1
					while k >= 0:
						i = k
						accept_props(prop,prop2)
						k = match_pattern(ast,i,statement,y[1],prop2)
					i -= 1
				elif y[0] == "t":
					if not isinstance(x,list)\
					or y[1] != x[0][:len(y[1])]:
						return -1
					
					
					if "no newline" in statement[2]\
					and x[0][:2] == ("brackets","indentation"): # skip optional or reject
						break
					
					
					if y[2] not in prop:
						prop[y[2]] = []
					prop[y[2]] += [x]
				elif y[0] == "k":
					if isinstance(x,list)\
					or x.lower() not in y[1]:
						return -1
					
					if y[2]:
						if y[2] not in prop:
							prop[y[2]] = []
						prop[y[2]] += [x]
			
			elif not isinstance(x,list)\
			and y == x.lower():
				pass
			else:
				return -1
			
			i += 1
			j += 1
			i_noskip = i
		
		
		while j < len(pattern): # skip optional or reject
			
			y = pattern[j]
			
			if isinstance(y,tuple)\
			and y[0] in {"?","*"}:
				j += 1
			else:
				return -1
		
		
		if "no succeeding expression" in statement[2]:
			j = i
			while j < len(ast)\
			and isinstance(ast[j],list)\
			and ast[j][0][0] in {"comment","whitespace"}: # no succeeding expression: skip comment and whitespace
				
				if "no newline" in statement[2]\
				and ast[j][0][:1] == ("whitespace",)\
				and ast[j][1][0].count("\n")\
				and (ast[j][1][0][:1] == "\n" or re.search("[^\\\\]\n",ast[j][1][0])): # no succeeding expression: do not skip newline
					break
				
				j += 1
			
			if j < len(ast)\
			and isinstance(ast[j],list)\
			and ast[j][0][0] in {"statement","literal","identifier","brackets"}:
				return -1
		
		
		return i_noskip
	
	
	def keyword_lookahead(ast,i):
		if not isinstance(ast[i],list):
			return ast[i].lower()
		
		for j in xrange(2):
			while i < len(ast)\
			and isinstance(ast[i],list)\
			and ast[i][0][0] in {"comment","whitespace"}: # skip comment and whitespace
				i += 1
			
			if i == len(ast):
				break
			
			x = ast[i]
			
			if isinstance(x,list)\
			and x[0][:1] == ("keyword",):
				x = x[0][1]
			
			if not isinstance(x,list):
				return x.lower()
			
			i += 1
		
		return None
	
	
	extract_properties = []
	
	for ctx_par in ctx["pars"]: # time in O(ast*statements)
		
		for statementss in statementsss:
			
			for statements in statementss[1]:
				ast = ctx_par[1]
				
				if statements[0] == "ltr":
					
					i = 0
					while i < len(ast):
						
						if isinstance(ast[i],list)\
						and ast[i][0][0] in {"comment","whitespace"}: # skip comment and whitespace
							i += 1
							continue
						
						lookahead = keyword_lookahead(ast,i)
						#print lookahead
						
						j = -1
						for statement in statements[1]:
							if "no lookahead" not in statement[2]\
							and lookahead not in statement[3]:
								continue
							
							prop = {}
							j = match_pattern(ast,i,statement,statement[1],prop)
							
							if j > i:
								break
						
						if j > i:
							ast[i:j] = [[("statement",statementss[0]) + statement[0],ast[i:j],prop]] # type, tokens, props
						else:
							i += 1
				
				elif statements[0] == "rtl":
					
					i = len(ast) - 1
					while i >= 0:
						
						if isinstance(ast[i],list)\
						and ast[i][0][0] in {"comment","whitespace"}: # skip comment and whitespace
							i -= 1
							continue
						
						lookahead = keyword_lookahead(ast,i)
						#print lookahead
						
						j = -1
						for statement in statements[1]:
							if "no lookahead" not in statement[2]\
							and lookahead not in statement[3]:
								continue
							
							prop = {}
							j = match_pattern(ast,i,statement,statement[1],prop)
							
							if j > i:
								break
						
						if j > i:
							ast[i:j] = [[("statement",statementss[0]) + statement[0],ast[i:j],prop]] # type, tokens, props
						else:
							i -= 1
	
	
	# extract properties
	
	#TODO ...
	
	
	# create index dictionaries
	
	
	def update_indices():
		ctx["pars"][1:] = []
		ctx["nodes"][:] = []
		ctx["types"].clear()
		ctx["parents"].clear()
		
		
		def build_index(curr,path):
			nodes = None
			for x in path:
				if x not in curr:
					curr[x] = [{},[]]
				nodes = curr[x][1]
				curr = curr[x][0]
			
			return nodes
		
		
		indent = [0]
		def traverse_recur(ast,parent,ctx):
			
			for x in ast:
				if isinstance(x,list):
					
					if x[0][:1] == ("brackets",):
						ctx["pars"] += [x]
					
					ctx["nodes"] += [x]
					nodes = build_index(ctx["types"],x[0])
					nodes += [x]
					ctx["parents"][id(x)] = parent
					
					if x[0][:1] == ("identifier",):
						x[2] = indent[0]
					
					if x[0][:1] == ("whitespace",):
						indent[0] += x[2]
					
					traverse_recur(x[1],x,ctx)
		
		traverse_recur(ctx["pars"][0][1],ctx["pars"][0],ctx)
	
	update_indices()
	
	if not rewrite\
	and "identifier" in ctx["types"]:
		for x in ctx["types"]["identifier"][1]:
			if x[1][0] not in ctx["tplparams"]:
				ctx["tplparams"][x[1][0]] = []
			ctx["tplparams"][x[1][0]] += [x]
	
	#print ctx["nodes"]
	#print ctx["types"]["literal"]
	#print ctx["parents"]
	#print ctx["tplparams"]
	
	
	# rewrite ast
	
	
	if rewrite: #TODO
		
		
		if u"Umschreiben.choose" in state:
			if state[u"Umschreiben.choose"][0]: # Spezielle Formatierung anwenden
				pass
			
			if state[u"Umschreiben.choose"][1]: # Fehlertoleranz für bestimmte Konstrukte einfügen
				pass
			
			if state[u"Umschreiben.choose"][2]: # Domänenspezifische Befehle umsetzen
				pass
		
		
		if u"Konfigurieren.active" in state\
		and state[u"Konfigurieren.active"]\
		and u"Konfigurieren.choose" in state:
			if state[u"Konfigurieren.choose"][0]: # Fehlertoleranz für Datenzugriffe einfügen
				statements = tuple(("statement","expression",statement) for statement in ("[",".",))
				ast = tuple(
					x for statement in statements\
					for x in parse_query(ctx).type(statement).sel
				)
				for i,x in enumerate(ast):
					tpl = copy.deepcopy(tpls[{("statement","expression","["): "config 0-1",("statement","expression","."): "config 0-2"}[x[0][:3]]])
					tpl["parents"] = {id(y):y for y in tpl["parents"].values()}
					tpl["positions"] = {id(y):y for y in tpl["positions"].values()}
					for y in tpl["tplparams"]["a"]:
						y[:] = x
					for y in tpl["tplparams"]["b"]:
						y[:] = copy.deepcopy(x[2][0][0])
					for y in tpl["tplparams"]["c"]:
						ast = tpl["parents"][id(y)][1]
						index = next(i for i,z in enumerate(ast) if z is y)
						ast[index:index + 1] = copy.deepcopy(x[2][1][0][1])
					x[:] = tpl["ast"][0]
					
					update_indices()
			
			if state[u"Konfigurieren.choose"][1]: # Toleranz bei Vergleich von Gleitkommazahlen
				pass
			if state[u"Konfigurieren.choose"][2]: # Algorithmen unterbrechbar machen
				pass
		
		
		
		
		#parent = ctx["parents"][id(x)]
		#parent[1][index:index + 1] = ast2
		
		#for i in xrange(3):
		#	if u"Umschreiben.choose" in state\
		#	and state[u"Umschreiben.choose"][i]:
		#		tpl = copy.deepcopy(tpls["rewrite %d" % i])
		
		#for i in xrange(3):
		#	if u"Konfigurieren.choose" in state\
		#	and state[u"Konfigurieren.choose"][i]:
		#		tpl = copy.deepcopy(tpls["config %d" % i])
	
	
	# log ast
	
	
	def prop_name(props,x):
		for y in props:
			if id(x) in (id(z) for z in props[y]):
				return " $" + unicode(y)
			else:
				for z in props[y]:
					if isinstance(z,list)\
					and z[0][:1] == ("prop group",):
						path = prop_name(z[2],x)
						if path:
							return " $" + unicode(y) + path
		return ""
	
	
	def log_recur(ast,props = None,depth = 0):
		
		indent = "  " * depth
		for x in ast:
			
			name = ""
			if props:
				name = prop_name(props,x)
			
			if isinstance(x,list)\
			and x[0][:1] == ("brackets",):
				print indent + repr(x[0]) + name + ":"
				log_recur(x[1],None,depth + 1)
			elif isinstance(x,list)\
			and x[0][:1] == ("statement",):
				print indent + repr(x[0]) + name + ":"
				log_recur(x[1],x[2],depth + 1)
			else:
				print indent + repr(x) + name
	
	#log_recur(ctx["pars"][0][1])
	
	
	# return ast
	
	
	ctx["ast"] = ctx["pars"][0][1]



def ast_descendants(ast,typee,ctx,ignore_types = None):
	
	nodes = parse_query(ctx).nodes([x for x in ast if isinstance(x,list)])
	descendants = nodes.descendants(include_original_selection = True)
	
	if ignore_types:
		for ignore_type in ignore_types:
			ignore_desc = descendants.type(ignore_type).descendants(include_original_selection = True)
			descendants = descendants.setMinus(ignore_desc)
	
	descendants = descendants.type(typee)
	
	return descendants.sel



def ast_ancestor(x,types,ctx):
	
	path = parse_query(ctx).nodes([x]).ancestors(include_original_selection = True).sel
	
	if not types:
		return path
	
	for i,x in tuple(enumerate(path))[1:]:
		for typee in types:
			if x[0][:len(typee)] == typee:
				return path[:i + 1]
	
	return []



def ast_isconform(tpl):
	pass



def ast_substitute(tpl,subs): #TODO indent
	pass



def ast_name(x,ctx):
	name = ""
	
	
	def prop_name(props,x):
		for y in props:
			if id(x) in (id(z) for z in props[y]):
				index = tuple(i for i,z in enumerate(props[y]) if z is x) or ("?",)
				return unicode(y) + "[" + unicode(index[0]) + "] "
			else:
				for z in props[y]:
					if isinstance(z,list)\
					and z[0][:1] == ("prop group",):
						path = prop_name(z[2],x)
						if path:
							index = tuple(i for i,zz in enumerate(props[y]) if zz is z) or ("?",)
							return unicode(y) + "[" + unicode(index[0]) + "] " + path
		return ""
	
	
	if id(x) in ctx["parents"]:
		parent = ctx["parents"][id(x)]
		
		name = ""
		if 2 < len(parent):
			name = prop_name(parent[2],x)
		
		else:
			if id(x) in (id(z) for z in parent[1]):
				ast = tuple(y for y in parent[1] if isinstance(y,list) and y[0][0] not in {"comment","whitespace","keyword"})
				index = tuple(i for i,z in enumerate(ast) if z is x) or ("?",)
				name += "[" + unicode(index[0]) + "] "
	
	name += "-".join(x[0])
	
	return name



def printt(ctx):
	ast = ctx["ast"]
	
	
	# print ast, insert marks, track positions
	
	
	ast2 = []
	pos = [0,0]
	def print_recur(ast,ast2,formatting):
		
		for x in ast:
			if isinstance(x,list)\
			and x[0][:1] == ("mark",):
				ast2 += [x]
			elif isinstance(x,list)\
			and x[0][:1] == ("keyword",):
				pos_prev = tuple(pos)
				
				print_recur(x[1],ast2,formatting)
				
				ctx["positions"][id(x)] = (pos_prev,tuple(pos))
			elif isinstance(x,list):
				pos_prev = tuple(pos)
				
				if x[0][:3] == ("statement","expression","("):
					ast2 += [[("mark","text formatting",1),[],{"sc": fmt["call"][0]}]] # type, tokens, props
					print_recur(x[1],ast2,x[0])
					ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
				elif formatting[:2] == ("brackets","("):
					ast2 += [[("mark","text formatting",1),[],{"sc": fmt["default"][0]}]] # type, tokens, props
					print_recur(x[1],ast2,x[0])
					ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
				else:
					print_recur(x[1],ast2,x[0])
				
				ctx["positions"][id(x)] = (pos_prev,tuple(pos))
			else:
				if formatting[:1] == ("statement",):
					if not re.search("[^\\W\\d]",x):
						ast2 += [x]
					else:
						ast2 += [[("mark","text formatting",1),[],{"sc": fmt["keyword"][0],"sf": fmt["keyword"][1]}]] # type, tokens, props
						ast2 += [x]
						ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
				#elif formatting[:1] == ("identifier",):
				#	ast2 += [[("mark","text formatting",1),[],{"sc": fmt["identifier"][0]}]] # type, tokens, props
				#	ast2 += [x]
				#	ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
				elif formatting[:1] == ("literal",):
					ast2 += [[("mark","text formatting",1),[],{"sc": fmt["literal"][0]}]] # type, tokens, props
					ast2 += [x]
					ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
				elif formatting[:1] == ("comment",):
					ast2 += [[("mark","text formatting",1),[],{"sc": fmt["comment"][0]}]] # type, tokens, props
					ast2 += [x]
					ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
				else:
					ast2 += [x]
				
				
				if x.count("\n"):
					pos[0] += x.count("\n")
					match = re.search("[^\\n]*\\Z",x)
					pos[1] = len(match.group(0))
				else:
					pos[1] += len(x)
	
	print_recur(ast,ast2,("brackets","{"))
	
	ast = ast2
	
	
	ast2 = [[u""]]
	i = 0
	while i < len(ast):
		x = ast[i]
		
		if not isinstance(x,list):
			
			j = i + 1
			while j < len(ast)\
			and not isinstance(ast[j],list):
				j += 1
			
			ast3 = "".join(ast[i:j])
			ast3 = ast3.split("\n")
			
			ast2[-1] += [ast3[0]]
			ast2 += [[x] for x in ast3[1:]]
			
			i = j
		else:
			ast2[-1] += [x]
			i += 1
	
	
	#if lines[ct] != ast2: raise Exception()
	ctx["lines"] = ast2



## TODO:



# reduce unary + and - with implicit parentheses
# (NO!) later pull ** out of unary + and -
# reduce different chained operators (eg. < <= ==)
# reduce sign of number literals
# reduce range access
# reduce initializers: tuple, list and dictionary
# reduce implicit tuple
# expand/reduce for-in
# short names for statements
# assign comments to line (from right or top) or block (from top with space)
# track inline-for props
# rearrange inline-if props
# expand/reduce from-import
# expand/reduce type-parmeters < >



# remember original tokens & formatting !
# remember selection
# (NO!) skip comments and whitespace ( token(n) )
# ast, ctx_pars for compile, ctx_tokens for complete traversal, ctx_types for selecting
# named children and parent for adjacent traversal

# rewrite: sync ast,x[1],ctx_pars,ctx_tokens,ctx_types,ctx_parents; format x[1]
# rewrite pre & post: track range -> caret diff, update on print


