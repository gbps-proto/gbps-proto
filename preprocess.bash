
f=$(cat "$1")

while ef=$(echo "$f" | grep -oP '(?<=execfile\(")[^"]+(?="\))' | head -n 1) && [[ "$ef" ]]; do
	f="${f//execfile(\"$ef\")/"$(cat "$ef")"}"
done

echo "$f"
