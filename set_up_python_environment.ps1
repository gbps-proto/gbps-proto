

echo ""
echo ""
echo "1 / 5: Downloading Ubuntu 16.04 for WSL..."

$path1 = "https://wsldownload.azureedge.net/"
$file1 = "Ubuntu_1604.2019.523.0_x64.appx"
$ProgressPreference = "SilentlyContinue"
$wd = $(Get-Location)
$j = Start-Job {Set-Location "$using:wd"; Invoke-WebRequest "$using:path1$using:file1" -OutFile "$using:file1";}
while ($j.State -ne "Completed") {
	Write-Host -NoNewLine "Progress: $([int] ((Get-Item "$file1" 2> $null).length / 208755084 * 100)) % of 199 MB  `r"
	Sleep 1
}
Write-Host "Progress: 100 % of 199 MB"

echo "1 / 5 DONE."
echo ""


echo ""
echo "2 / 5: Installing Ubuntu 16.04 for WSL..."

Add-AppxPackage "$file1"
& "ubuntu1604.exe" install --root

echo "2 / 5 DONE."
echo ""


echo ""
echo "3 / 5: Downloading and installing additional Packages..."

wsl -d "Ubuntu-16.04" sudo apt-get update -qq
wsl -d "Ubuntu-16.04" sudo apt-get install -qq python-cairo python-pygame python-tk fonts-dejavu fonts-droid-fallback > $null
wsl -d "Ubuntu-16.04" sudo apt-get clean -qq

echo "3 / 5 DONE."
echo ""


echo ""
echo "4 / 5: Downloading VcXsrv X Server for Windows..."

$path2 = "https://netcologne.dl.sourceforge.net/project/vcxsrv/vcxsrv/1.20.8.1/"
$file2 = "vcxsrv-64.1.20.8.1.installer.exe"
$ProgressPreference = "SilentlyContinue"
$wd = $(Get-Location)
$j = Start-Job {Set-Location "$using:wd"; Invoke-WebRequest "$using:path2$using:file2" -OutFile "$using:file2";}
while ($j.State -ne "Completed") {
	Write-Host -NoNewLine "Progress: $([int] ((Get-Item "$file2" 2> $null).length / 41973411 * 100)) % of 40 MB  `r"
	Sleep 1
}
Write-Host "Progress: 100 % of 40 MB"

echo "4 / 5 DONE."
echo ""


echo ""
echo "5 / 5: Installing VcXsrv X Server for Windows..."

& ".\$file2" /S

echo "5 / 5 DONE."
echo ""


echo ""
echo "SUCCESS."
echo ""
echo ""


Read-Host "Press Enter to close this window..."

