


	global disable_draw_prs
	disable_draw_prs = False
	
	def clip(x1,x2,y1,y2):
		if disable_draw_prs: return
		
		ctx.reset_clip()
		ctx.rectangle(x1,y1,max(0,x2 - x1),max(0,y2 - y1))
		ctx.clip()
	
	def frect(x1,x2,y1,y2,(r,g,b),a = 1):
		if disable_draw_prs: return
		
		ctx.set_source_rgba(r,g,b,a)
		ctx.rectangle(x1,y1,x2 - x1,y2 - y1)
		ctx.fill()
	
	def srect(x1,x2,y1,y2,(r,g,b),a = 1,lw = 1):
		if disable_draw_prs: return
		
		ctx.set_line_width(lw)
		ctx.set_source_rgba(r,g,b,a)
		ctx.rectangle(x1 + .5,y1 + .5,x2 - x1 - 1,y2 - y1 - 1)
		ctx.stroke()
	
	def fcirc(x,y,rad,(r,g,b)):
		if disable_draw_prs: return
		
		ctx.set_source_rgb(r,g,b)
		ctx.arc(x,y,rad,0,2 * math.pi)
		ctx.fill()
	
	def ftri(x,y,rho,phi,(r,g,b)):
		if disable_draw_prs: return
		
		verts = [
			[rho * math.cos(phi),rho * math.sin(phi)],
			[rho * math.cos(phi + 2./3 * math.pi),rho * math.sin(phi + 2./3 * math.pi)],
			[rho * math.cos(phi + 4./3 * math.pi),rho * math.sin(phi + 4./3 * math.pi)],
		]
		
		ctx.set_source_rgb(r,g,b)
		ctx.move_to(x + verts[0][0],y + verts[0][1])
		ctx.line_to(x + verts[1][0],y + verts[1][1])
		ctx.line_to(x + verts[2][0],y + verts[2][1])
		ctx.fill()
	
	def text_extents((ff,fs,fe,fw),textt):
		ctx.select_font_face(ff,cairo.FONT_SLANT_NORMAL,fw)
		fo = ctx.get_font_options()
		fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
		ctx.set_font_options(fo)
		ctx.set_font_size(fs)
		
		return ctx.text_extents(textt)[2]
	
	def text(x,y,(r,g,b),(ff,fs,fe,fw),textt):
		if disable_draw_prs: return
		
		#srect(x,x + (len(textt) * fe[0] if fe[0] else text_extents((ff,fs,fe,fw),textt)),y,y + fe[2],red)
		
		ctx.set_source_rgb(r,g,b)
		ctx.select_font_face(ff,cairo.FONT_SLANT_NORMAL,fw)
		fo = ctx.get_font_options()
		fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
		ctx.set_font_options(fo)
		ctx.set_font_size(fs)
		ctx.move_to(x,y + fe[2])
		ctx.show_text(textt)
		ctx.fill()
	
	def text_fit(x,y,w,(r,g,b),(ff,fs,fe,fw),textt):
		if disable_draw_prs: return
		
		#srect(x,x + (len(textt) * fe[0] if fe[0] else text_extents((ff,fs,fe,fw),textt)),y,y + fe[2],red)
		
		ctx.set_source_rgb(r,g,b)
		ctx.select_font_face(ff,cairo.FONT_SLANT_NORMAL,fw)
		fo = ctx.get_font_options()
		fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
		ctx.set_font_options(fo)
		ctx.set_font_size(fs)
		
		extent = max(1,ctx.text_extents(textt)[2])
		
		for i in xrange(100):
			scale = min(1,float(w) / extent)
			
			ctx.save()
			ctx.translate(x,y + fe[2] // 2)
			ctx.scale(scale,scale)
			ctx.translate(-x,-(y + fe[2] // 2))
			
			if ctx.text_extents(textt)[2] * scale <= w:
				break
			
			ctx.restore()
			
			extent += 1
		
		ctx.move_to(x,y + fe[2])
		ctx.show_text(textt)
		ctx.fill()
		
		ctx.restore()
	
	def shadow_rect(x1,x2,y1,y2):
		if disable_draw_prs: return
		
		ctx.set_source_surface(surf_tile1)
		ctx.get_source().set_extend(cairo.EXTEND_REPEAT)
		ctx.get_source().set_filter(cairo.FILTER_NEAREST)
		matrix = ctx.get_source().get_matrix()
		matrix.scale(.125,.125)
		ctx.get_source().set_matrix(matrix)
		ctx.rectangle(x1,y1,x2 - x1,y2 - y1)
		ctx.fill()
	
	def shadow_in(x1,x2,y1,y2):
		if disable_draw_prs: return
		
		sb = sb_in
		rect = (x1 - sds_in[2] - sb,x2 + sds_in[0] + sb,y1 - sds_in[1] - sb,y2 + sds_in[3] + sb)
		
		ctx_mask.set_source_rgba(0,0,0,1)
		ctx_mask.rectangle(0,0,width,height)
		ctx_mask.fill()
		
		ctx_mask.save()
		ctx_mask.set_operator(cairo.OPERATOR_DEST_OUT)
		ctx_mask.set_source_rgba(0,0,0,1)
		ctx_mask.rectangle(rect[0],rect[2],rect[1] - rect[0],rect[3] - rect[2])
		ctx_mask.fill()
		ctx_mask.restore()
		
		pattern = cairo.LinearGradient(rect[0],0,rect[0] + sb,0)
		pattern.add_color_stop_rgba(0,0,0,0,1)
		pattern.add_color_stop_rgba(1,0,0,0,0)
		ctx_mask.set_source(pattern)
		ctx_mask.rectangle(rect[0],rect[2],sb,rect[3] - rect[2])
		ctx_mask.fill()
		
		pattern = cairo.LinearGradient(0,rect[3],0,rect[3] - sb)
		pattern.add_color_stop_rgba(0,0,0,0,1)
		pattern.add_color_stop_rgba(1,0,0,0,0)
		ctx_mask.set_source(pattern)
		ctx_mask.rectangle(rect[0],rect[3],rect[1] - rect[0],-sb)
		ctx_mask.fill()
		
		pattern = cairo.LinearGradient(rect[1],0,rect[1] - sb,0)
		pattern.add_color_stop_rgba(0,0,0,0,1)
		pattern.add_color_stop_rgba(1,0,0,0,0)
		ctx_mask.set_source(pattern)
		ctx_mask.rectangle(rect[1],rect[2],-sb,rect[3] - rect[2])
		ctx_mask.fill()
		
		pattern = cairo.LinearGradient(0,rect[2],0,rect[2] + sb)
		pattern.add_color_stop_rgba(0,0,0,0,1)
		pattern.add_color_stop_rgba(1,0,0,0,0)
		ctx_mask.set_source(pattern)
		ctx_mask.rectangle(rect[0],rect[2],rect[1] - rect[0],sb)
		ctx_mask.fill()
		
		ctx.set_source_surface(surf_tile2)
		ctx.get_source().set_extend(cairo.EXTEND_REPEAT)
		ctx.get_source().set_filter(cairo.FILTER_NEAREST)
		matrix = ctx.get_source().get_matrix()
		matrix.scale(.125,.125)
		ctx.get_source().set_matrix(matrix)
		ctx.mask_surface(surf_mask)
	
	def shadow_out(x1,x2,y1,y2):
		if disable_draw_prs: return
		
		sb = sb_out
		rect = (x1 - sds_out[0],x2 + sds_out[2],y1 - sds_out[3],y2 + sds_out[1])
		
		ctx_mask.save()
		ctx_mask.set_operator(cairo.OPERATOR_DEST_OUT)
		ctx_mask.set_source_rgba(0,0,0,1)
		ctx_mask.rectangle(0,0,width,height)
		ctx_mask.fill()
		ctx_mask.restore()
		
		ctx_mask.set_source_rgba(0,0,0,1)
		ctx_mask.rectangle(rect[0],rect[2],rect[1] - rect[0],rect[3] - rect[2])
		ctx_mask.fill()
		
		ctx_mask.save()
		ctx_mask.set_operator(cairo.OPERATOR_DEST_OUT)
		
		pattern = cairo.LinearGradient(rect[0],0,rect[0] + sb,0)
		pattern.add_color_stop_rgba(0,0,0,0,1)
		pattern.add_color_stop_rgba(1,0,0,0,0)
		ctx_mask.set_source(pattern)
		ctx_mask.rectangle(rect[0],rect[2],sb,rect[3] - rect[2])
		ctx_mask.fill()
		
		pattern = cairo.LinearGradient(0,rect[3],0,rect[3] - sb)
		pattern.add_color_stop_rgba(0,0,0,0,1)
		pattern.add_color_stop_rgba(1,0,0,0,0)
		ctx_mask.set_source(pattern)
		ctx_mask.rectangle(rect[0],rect[3],rect[1] - rect[0],-sb)
		ctx_mask.fill()
		
		pattern = cairo.LinearGradient(rect[1],0,rect[1] - sb,0)
		pattern.add_color_stop_rgba(0,0,0,0,1)
		pattern.add_color_stop_rgba(1,0,0,0,0)
		ctx_mask.set_source(pattern)
		ctx_mask.rectangle(rect[1],rect[2],-sb,rect[3] - rect[2])
		ctx_mask.fill()
		
		pattern = cairo.LinearGradient(0,rect[2],0,rect[2] + sb)
		pattern.add_color_stop_rgba(0,0,0,0,1)
		pattern.add_color_stop_rgba(1,0,0,0,0)
		ctx_mask.set_source(pattern)
		ctx_mask.rectangle(rect[0],rect[2],rect[1] - rect[0],sb)
		ctx_mask.fill()
		
		ctx_mask.restore()
		
		ctx.set_source_surface(surf_tile2)
		ctx.get_source().set_extend(cairo.EXTEND_REPEAT)
		ctx.get_source().set_filter(cairo.FILTER_NEAREST)
		matrix = ctx.get_source().get_matrix()
		matrix.scale(.125,.125)
		ctx.get_source().set_matrix(matrix)
		ctx.mask_surface(surf_mask)


