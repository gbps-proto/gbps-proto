


# Fluid Graph Query Interface
# 
# 
# 
# Constructors:
# 
# I1: parse_query(ctx) (select all nodes)
# 
# I2: other.copy() (duplicate query object), other.empty() (without selection), other.init() (select all nodes)
# 
# I3: other.root() (select root node), other.nodes(nodes) (create selection of specific nodes)
# 
# 
# Selectors:
# 
# S1: type(type) (grammar statement hierarchy, also matches prefixes, extended type hierarchy for documentation, recorded data, queries, templates, configurations, guidelines, schemata, models, recipes and additional data)
# 
# S2: clazz(clazz) (e.g. structure declaration, function declaration, variable declaration, parameter declaration, instruction, affects control flow, invokes code, loop, condition, exception handling, expression, accesses data, writes data, reads data, specific language)
# 
# S3: flag(flag) (analyzed features, behavior, purposes and functioning)
# 
# S4: group(group) (orthogonal grouping, variants)
# 
# 
# Properties (Navigate and Filter):
# 
# P1: nodeProperties(properties = *,property_class = None) (navigate along named edges to child nodes; contains results of abstract analysis, e.g. control and data flow)
# 
# Property classes are e.g. identifier, other identifier, declaration, condition, data read, data write, control flow, property source (see below)
# 
# P2: hasNodeProperties(properties = *,property_class = None), hasDataProperties(#) -- (filter by occurrence of named edges)
# 
# P3: isNodePropertyOf(other,properties = *,property_class = None), reverseIsNodePropertyOf(#) (has property with) -- (filter by relations along named edges)
# 
# P4: dataPropertiesTextMatch(pattern,properties = *,property_class = None), dataPropertiesTextRegex(#) -- (filter by attribute values)
# 
# 
# There are 2 types of properties and 4 sources of properties:
# 
# Type 1: Node Properties (child nodes)
# 
# Type 2: Data Properties (attributes)
# 
# Source 1: grammar (syntax tree)
# 
# Source 2: annotations (assigned comments and attributes assigned by the programmer)
# 
# Source 3: semantic (resolved type, variable and function identifiers, scopes of variables)
# 
# Source 4: abstract analysis (control and data flow, features, behavior, purposes and functioning)
# 
# 
# Navigation and Selection: -- (N3, N4, N6, N7 syntax tree only, ignore results of abstract analysis)
# 
# N1: pick(order = Preorder,indices) (indices can be negative), slice(order = Preorder,index,count = +Infinity) (count can be negative) -- (select nodes within previous selection)
# 
# N2: next(order = Preorder,reverse = False,distance = 1,all_following = False,include_original_selection = False) (nodes within specific distance XOR all following nodes in direction) -- (applied simultaneously to all nodes in selection; result is not necessarily contained in previous selection)
# 
# N3: convenience aliases: parent(distance = 1), children(#), leftSibling(#), rightSibling(#), adjacentSibling(#) -- (0 to include original selection, negative for other direction)
# 
# N4: convenience aliases: ancestors(include_original_selection = False), descendants(#), leftSiblings(#), rightSiblings(#), allSiblings(#)
# 
# N5: search(predicate(i,x,y),order = Preorder,count = 1,start = None,search_in_other = None,abort_before_miss = False,abort_after_miss = False) (count can be +Infinity or negative) -- (result is not necessarily contained in previous selection)
# 
# N6: topNodes(), leftmostNode(), rightmostNode(), bottomNodes() -- (select nodes within previous selection)
# 
# N7: lowestCommonAncestor(), leftmostDescendant() (includes previous selection), rightmostDescendant(), bottomDescendants() -- (result is not necessarily contained in previous selection)
# 
# N8: indices(order = Preorder) (dictionary of node -> index) -- (does not return a selection)
# 
# 
# Built-In Orders: -- (O1, O2 syntax tree only, ignore results of abstract analysis)
# 
# O1: Preorder(reverse = False,wrap_at_end = False), Postorder(#), PrintedTokens(#)
# 
# O2: Ancestor(reverse = False), Descendant(#), Sibling(#,wrap_at_end = False) -- (not suitable for pick() and slice())
# 
# O3: Incoming(reverse = False,depth_first = False), Outgoing(#) -- (graph search, consider results of abstract analysis; not suitable for pick() and slice())
# 
# 
# Set Algebra, Filter and Query:
# 
# A1: unique() (remove duplicate entries in selection; necessary, if the selection was manually changed)
# 
# A2: setAnd(other,...) (intersection), setOr(#) (union), setXor(#) (only contained once), setMinus(#) (only contained in first), setNot() (not contained, all minus contained)
# 
# s1.and(s2)....and(sn) == parse_query.and(s1,s2,...,sn)
# 
# A3: filter(predicate(i,x)), map(mapping(i,x)) (mapping returns list of nodes)
# 
# Q1: contains(other,...), containsNotEqual(#), reverseContains(#) (is contained in), reverseContainsNotEqual(#), isEqualTo(#), isDisjointTo(#) -- (do not return a selection)
# 
# Q2: exists(predicate(i,x)), forall(#) -- (do not return a selection)
# 
# Q3: count() (number of nodes in selection) -- (does not return a selection)
# 
# 
# Built-In Predicates:
# 
# B1: PredicateAnd(), PredicateOr(), PredicateXor(), PredicateImplication(), PredicateReverseImplication(), PredicateEquivalence(), PredicateNot()
# 
# B2: ContainedIn(other) (True, if x is contained in the other selection, e.g. if x satisfies the predicates of the other selection; useful for operations with a result, which is not necessarily contained in previous selection)
# 
# B3: HasContainedNodeProperties(other,properties = *,property_class = None) (True, if one of the specified properties of x is contained in the other selection)
# 
# B4: HasContainedNext(other,order = Preorder,distance = 1,all_following = False) (True, if one of the specified relatives of x is contained in the other selection)
# 
# B5: SameType() (True, if x has the same type as y)
# 
# B6: SameProperties(properties = *,property_class = None) (True, if the specified properties of x and y are equal)
# 
# 
# Searching Constructs:
# 
# C1: occurrencesOfConstruct(specification) (matches nodes with the specified structure of descendants)
# 
# C2: occurrencesOfPattern(specification) (matches nodes, which contain the specified relations of descendants)
# 
# C3: occurrencesOfIdiom(specification) (matches nodes, which contain an implementation of the specified purpose at a small scale)
# 
# C4: occurrencesOfModel(specification) (matches groups of nodes, which implement the specified purpose at a larger scale)
# 
# 
# Traversal and Reading the Result:
# 
# R1: foreach(iteration(i,x)) (alias for filter())
# 
# R2: result() (returns all nodes in selection as a list)
# 
# 
# Utility Functions:
# 
# U1: parse_query.path(x,y) (get a path from x to y)
# 
# 



callPredicate = lambda predicate,i,s,x,o,y: predicate({"i": i,"s": s,"x": x,"o": o,"y": y})

class parse_query(object):
	
	
	# Constructors:
	
	
	def __init__(s,ctx = None,sel = None):
		s.ctx = ctx if ctx is not None else ctx1[ct]
		s.sel = sel if sel is not None else s.ctx["nodes"] # do not modify s.sel
	
	copy = lambda o: parse_query(ctx = o.ctx,sel = list(o.sel)) # allowed to modify s.sel
	empty = lambda o: parse_query(ctx = o.ctx,sel = [])
	init = lambda o: parse_query(ctx = o.ctx)
	root = lambda o: parse_query(ctx = o.ctx,sel = [o.ctx["root"]])
	nodes = lambda o,nodes: parse_query(ctx = o.ctx,sel = nodes)
	
	
	# Selectors:
	
	
	def type(s,typee):
		nodes = None
		curr = s.ctx["types"]
		for x in typee:
			if x not in curr:
				return s.empty()
			
			nodes = list(curr[x][1])
			curr = curr[x][0]
		
		if not nodes and not curr.values():
			return s.empty()
		
		open = curr.values()
		while len(open):
			curr = open.pop()
			nodes += curr[1]
			open += curr[0].values()
		
		return s.setAnd(s.nodes(nodes))
	
	def clazz(s,clazz):
		if clazz not in s.ctx["class"]:
				return s.empty()
		
		r = s
		for x in s.ctx["class"][clazz]:
			r = r.type(x)
		
		return r
	
	def flag(s,flag):
		if flag not in s.ctx["flags"]:
				return s.empty()
		
		return s.setAnd(s.nodes(s.ctx["flags"][flag]))
	
	def group(s,group):
		if group not in s.ctx["groups"]:
				return s.empty()
		
		return s.setAnd(s.nodes(s.ctx["groups"][group]))
	
	
	# Properties (Navigate and Filter):
	
	
	def isNode(s):
		r = s.empty()
		r.sel = list(x for x in s.sel if isinstance(x,list) and x[0][:1] != ("data",))
		
		return r
	
	def isData(s):
		r = s.empty()
		r.sel = list(x for x in s.sel if not isinstance(x,list) or x[0][:1] == ("data",))
		
		return r
	
	def nodeProperties(s,properties = None,property_class = None):
		r = s.empty()
		
		if properties:
			r.sel = list(y for x in s.sel for p in properties if p in x[2] for y in x[2][p])
		else:
			r.sel = list(y for x in s.sel for p in x[2] for y in x[2][p])
		
		return r.isNode().unique()
	
	hasNodeProperties = lambda s,o,*arg,**kwarg: s.filter(lambda arg1: s.nodes([arg1["x"]]).nodeProperties(*arg,**kwarg).isNode().count())
	hasDataProperties = lambda s,o,*arg,**kwarg: s.filter(lambda arg1: s.nodes([arg1["x"]]).nodeProperties(*arg,**kwarg).isData().count())
	isNodePropertyOf = lambda s,o,*arg,**kwarg: o.nodeProperties(*arg,**kwarg).setAnd(s)
	reverseIsNodePropertyOf = lambda s,o,*arg,**kwarg: s.nodeProperties(*arg,**kwarg).setAnd(o)
	
	#dataPropertiesTextMatch(pattern,properties = *,property_class = None)
	#dataPropertiesTextRegex(#)
	
	
	# Navigation and Selection:
	
	
	#pick(order = Preorder,indices)
	#slice(order = Preorder,index,count = +Infinity)
	
	def next(s,order = None,*arg,**kwarg):
		if order is None: order = Preorder
		
		return order.next(s,*arg,**kwarg)
	
	parent = lambda s,*arg,**kwarg: s.next(*arg,order = s.Ancestor(**kwarg),**kwarg)
	children = lambda s,*arg,**kwarg: s.next(*arg,order = s.Descendant(**kwarg),**kwarg)
	leftSibling = lambda s,*arg,**kwarg: s.next(*arg,order = s.Sibling(reverse = True,**kwarg),**kwarg)
	rightSibling = lambda s,*arg,**kwarg: s.next(*arg,order = s.Sibling(**kwarg),**kwarg)
	adjacentSibling = lambda s,*arg,**kwarg: s.leftSibling(*arg,**kwarg).setOr(s.rightSibling(*arg,**kwarg))
	
	ancestors = lambda s,*arg,**kwarg: s.parent(*arg,all_following = True,**kwarg)
	descendants = lambda s,*arg,**kwarg: s.children(*arg,all_following = True,**kwarg)
	leftSiblings = lambda s,*arg,**kwarg: s.leftSibling(*arg,all_following = True,**kwarg)
	rightSiblings = lambda s,*arg,**kwarg: s.rightSibling(*arg,all_following = True,**kwarg)
	allSiblings = lambda s,*arg,**kwarg: s.rightSibling(*arg,all_following = True,wrap_at_end = True,**kwarg)
	
	#search(predicate(i,x,y),order = Preorder,count = 1,start = None,search_in_other = None,abort_before_miss = False,abort_after_miss = False)
	
	#topNodes()
	#leftmostNode()
	#rightmostNode()
	#bottomNodes()
	
	lowestCommonAncestor = lambda s: s.ancestors(include_original_selection = True).search(lambda x: s.nodes([x]).children().setAnd(s).count() > 1,start = s.root())
	
	#leftmostDescendant()
	#rightmostDescendant()
	#bottomDescendants()
	
	#indices(order = Preorder)
	
	
	# Set Algebra, Filter and Query:
	
	
	def unique(s):
		r = s.empty()
		r_ids = set()
		r.sel = []
		for x in s.sel:
			if id(x) not in r_ids:
				r_ids |= {id(x)}
				r.sel += [x]
		
		return r
	
	def setAnd(s,o):
		if s.sel is o.sel or o.sel is o.ctx["nodes"]: return s
		if s.sel is s.ctx["nodes"]: return o
		if s.count() < o.count(): t = s; s = o; o = t # small set, large loop
		
		o_ids = set(id(x) for x in o.sel)
		r = s.empty()
		r.sel = list(x for x in s.sel if id(x) in o_ids)
		
		return r
	
	def setOr(s,o):
		if s.count() > o.count(): t = s; s = o; o = t # small set, large loop
		
		r = s.copy()
		r_ids = set(id(x) for x in r.sel)
		r.sel += list(x for x in o.sel if id(x) not in r_ids)
		
		return r
	
	setXor = lambda s,o: s.setOr(o).setMinus(s.setAnd(o))
	
	def setMinus(s,o):
		
		o_ids = set(id(x) for x in o.sel)
		r = s.empty()
		r.sel = list(x for x in s.sel if id(x) not in o_ids)
		
		return r
	
	setNot = lambda s: s.init().setMinus(s)
	
	def filter(s,predicate,abort_after_miss = False,abort_after_match = False):
		r = s.empty()
		
		i = 0
		for x in s.sel:
			if callPredicate(predicate,i,s,x,None,None):
				r.sel.append(x)
			
			if abort_after_miss and not r.count(): return r
			if abort_after_match and r.count(): return r
			
			i += 1
		
		return r
	
	def map(s,mapping):
		r = s.empty()
		
		i = 0
		for x in s.sel:
			r.sel += callPredicate(mapping,i,s,x,None,None)
			i += 1
		
		return r.unique() # also works with arbitrary content
	
	contains = lambda s,o: s.count() >= o.count() and not o.setMinus(s).count()
	containsNotEqual = lambda s,o: s.count() > o.count() and not s.contains(o)
	reverseContains = lambda s,o: s.count() <= o.count() and not s.setMinus(o).count()
	reverseContainsNotEqual = lambda s,o: s.count() < o.count() and not s.reverseContains(o)
	isEqualTo = lambda s,o: s.count() == o.count() and set(id(x) for x in s.sel) == set(id(x) for x in o.sel)
	isDisjointTo = lambda s,o: not set(id(x) for x in s.sel) & set(id(x) for x in o.sel)
	
	exists = lambda s,predicate: s.filter(predicate,abort_after_match = True).count()
	forall = lambda s,predicate: s.filter(predicate,abort_after_miss = True).count() == s.count()
	
	count = lambda s: len(s.sel)
	
	
	# Traversal and Reading the Result:
	
	
	foreach = lambda s,predicate: s.filter(predicate)
	result = lambda s: s.sel
	
	
	# Built-In Orders:
	
	
	class Preorder(object):
		
		def __init__(order,reverse = False,wrap_at_end = False,**kwarg):
			order.reverse = reverse
			order.wrap_at_end = wrap_at_end
	
	class Postorder(object):
		
		def __init__(order,reverse = False,wrap_at_end = False,**kwarg):
			order.reverse = reverse
			order.wrap_at_end = wrap_at_end
	
	class PrintedTokens(object):
		
		def __init__(order,reverse = False,wrap_at_end = False,**kwarg):
			order.reverse = reverse
			order.wrap_at_end = wrap_at_end
	
	class Descendant(object):
		
		def __init__(order,reverse = False,**kwarg):
			order.reverse = reverse
		
		def step(order,s,reverse,r,c):
			if reverse ^ order.reverse: # parent
				c.sel = list(s.ctx["parents"][id(x)] for x in c.sel if id(x) in s.ctx["parents"])
			else: # children
				c.sel = list(y for x in c.sel for y in x[1] if isinstance(y,list))
			r.sel += c.sel
			
			return c.sel
		
		def next(order,s,reverse = False,distance = 1,all_following = False,include_original_selection = False,**kwarg):
			r = s.copy() if include_original_selection else s.empty()
			c = s.copy()
			left = 1 if all_following else distance
			while left and order.step(s,reverse,r,c):
				if not all_following:
					left -= 1
			
			return r.unique()
	
	class Ancestor(Descendant):
		
		def __init__(order,reverse = False,**kwarg):
			super(parse_query.Ancestor,order).__init__(not reverse,**kwarg)
	
	class Sibling(object):
		
		def __init__(order,reverse = False,wrap_at_end = False,**kwarg):
			order.reverse = reverse
			order.wrap_at_end = wrap_at_end
		
		def oneAdjacent(order,s,reverse,distance,rsel,siblings,index):
			distance = max(len(siblings),distance)
			
			if reverse ^ order.reverse: # left
				start = index - distance
				end = index
				rsel += list(reversed(siblings[max(0,start):end]))
			else: # right
				start = index + 1
				end = index + 1 + distance
				rsel += siblings[start:end]
			
			if order.wrap_at_end:
				if start < 0:
					rsel += list(reversed(siblings[start:]))
				if end > len(siblings):
					rsel += siblings[:end - len(siblings)]
		
		def allFollowing(order,s,reverse,distance,rsel,siblings,index):
			if reverse ^ order.reverse: # left
				rsel += list(reversed(siblings[:index]))
				if order.wrap_at_end:
					rsel += list(reversed(siblings[index:]))
			else: # right
				rsel += siblings[index + 1:]
				if order.wrap_at_end:
					rsel += siblings[:index + 1]
		
		def next(order,s,reverse = False,distance = 1,all_following = False,include_original_selection = False,**kwarg):
			operation = order.allFollowing if all_following else order.oneAdjacent
			
			r = s.copy() if include_original_selection else s.empty()
			for x in s.sel:
				if x in s.ctx["parents"][id(x)]:
					siblings = tuple(y for y in s.ctx["parents"][id(x)][1] if isinstance(y,list) and y[0][0] not in {"comment","whitespace","keyword"})
					index = tuple(i for i,z in enumerate(siblings) if z is x) or ()
					
					if not index:
						continue
					
					operation(order,s,reverse,distance,r.sel,siblings,index[0])
			
			return r.unique()
	
	class Outgoing(object):
		
		def __init__(order,reverse = False,depth_first = False,**kwarg):
			order.reverse = reverse
			order.depth_first = depth_first
	
	class Incoming(Outgoing):
		
		def __init__(order,reverse = False,**kwarg):
			super(parse_query.Incoming,order).__init__(not reverse,**kwarg)
	
	
	# Built-In Predicates:
	
	
	#PredicateAnd()
	#PredicateOr()
	#PredicateXor()
	#PredicateImplication()
	#PredicateReverseImplication()
	#PredicateEquivalence()
	#PredicateNot()
	
	class ContainedIn(object):
		
		def __init__(predicate,o,**kwarg):
			predicate.o = o
	
	#HasContainedNodeProperties(o,properties = *,property_class = None)
	#HasContainedNext(o,order = Preorder,distance = 1,all_following = False)
	#SameType()
	#SameProperties(properties = *,property_class = None)
	
	
	# Utility Functions:
	
	
	def path(s,o):
		assert s.count() == 1
		assert o.count() == 1
		
		a1 = s.ancestors(include_original_selection = True)
		a2 = o.ancestors(include_original_selection = True)
		lca = a1.setAnd(a2).sel[0]
		
		path = a1.setMinus(a2).sel + [lca] + list(reversed(a2.setMinus(a1).sel))
		
		return path



