


	global get_card_size
	
	def get_card_size(card):
		global disable_draw_prs
		
		
		#TODO move ( le -> l -> le )
		
		
		if card["mode"] == "paragraph":
			w1 = ixs[2] + 2 * ixs[1] # fixed size
		elif card["mode"] == "typewriter":
			#w1 = xs[3] - xs[2] - 2 * ixs[0] # maximum size
			w1 = ixs[2] + 2 * ixs[1] # fixed size
		
		
		disable_draw_prs = True
		(x1,y1,w1,h1) = layout_card(card,0,0,w1,0,0,0)
		disable_draw_prs = False
		
		
		x1 = xs[3] - ixs[0] - w1
		h1 = y1
		
		
		return (x1,0,w1,h1)



	def layout_card(card,x1,y1,w1,h1,x2,y2):
		
		
		#TODO clip
		
		
		frect(x1,x1 + w1,y1,y1 + h1 - 1,wht)
		srect(x1,x1 + w1,y1,y1 + h1 - 1,bl1)
		if x1 + w1 - 1 <= x2:
			frect(x1 + w1 - 1,x2 + 2,y1 + h1 - 3,y1 + h1 - 1,bl1)
		else:
			frect(x2,x1 + w1,y1 + h1 - 2,y1 + h1,bl1)
		frect(x2 + .5,x2 + 1.5,y1 + h1 - 1,y2 - 5,bl1)
		ftri(x2 + 1,y2 - 5,5,1./2 * math.pi,bl1)
		
		
		y = y1
		x = x1
		w = w1
		
		y += iys[1]
		x += ixs[1]
		w -= 2 * ixs[1]
		
		## TODO:
		if card["mode"] == "paragraph":
			y += 150
		elif card["mode"] == "typewriter":
			y += 50
		
		
		y += iys[1]
		y += 2
		
		return (x1,y,w1,h1)



	def layout_editor():
		global scroll1_resize,ast_selected1,ast_selected2,buttons1
		
		
		measure["layout_editor"] = pygame.time.Clock()
		
		
		# decorate lines
		
		
		if not ("no parse" in files[ct][3] and files[ct][3]["no parse"])\
		and not ("highlighted" in ctx1[ct] and ctx1[ct]["highlighted"]):
			ctx1[ct]["highlighted"] = True
			
			
			lines[ct] = copy.deepcopy(ctx1[ct]["lines"])
			
			
			def highlight(x,name):
				pos = ctx1[ct]["positions"][id(x)]
				seg1 = get_segment(lines[ct][pos[0][0]],pos[0][1])
				lines[ct][pos[0][0]][seg1[0]:seg1[0] + 1] = [
					lines[ct][pos[0][0]][seg1[0]][:seg1[1]],
					[("mark","draw anchor",1,name),[]], # type, tokens
					lines[ct][pos[0][0]][seg1[0]][seg1[1]:],
				]
				seg2 = get_segment(lines[ct][pos[1][0]],pos[1][1])
				lines[ct][pos[1][0]][seg2[0]:seg2[0] + 1] = [
					lines[ct][pos[1][0]][seg2[0]][:seg2[1]],
					[("mark","draw anchor",-1,name),[],{"type": "bounding box","fill": yl1,"stroke": yl2}], # type, tokens, props
					lines[ct][pos[1][0]][seg2[0]][seg2[1]:],
				]
			
			
			if u"Abfrage.choose" in state:
				if state[u"Abfrage.choose"][0]: # Division durch eine Variable
					statements = tuple(("statement","expression","op",statement,"binary") for statement in ("/","//","%",))
					ast = tuple(
						x for statement in statements\
						for x in parse_query(ctx1[ct]).type(statement).sel
					)
					for i,x in enumerate(ast):
						if ast_descendants(x[2]["x"][1:],("identifier",),ctx1[ct]):
							highlight(x,"%s-%d-%d" % (u"Abfrage.choose",0,i))
				
				if state[u"Abfrage.choose"][1]: # Datenzugriff mit einer Variablen als Schlüssel
					statements = tuple(("statement","expression",statement) for statement in ("[",))
					ast = tuple(
						x for statement in statements\
						for x in parse_query(ctx1[ct]).type(statement).sel
					)
					for i,x in enumerate(ast):
						if ast_descendants(x[2]["x"][1:],("identifier",),ctx1[ct]):
							highlight(x,"%s-%d-%d" % (u"Abfrage.choose",1,i))
				
				if state[u"Abfrage.choose"][2]: # Vergleich mit einem Literal
					
					statements = tuple(("statement","expression","op",statement,"binary") for statement in ("in","not in","<","<=",">",">=","!=","==",))
					
					q = parse_query(ctx1[ct])
					q.sel = list(
						x for statement in statements\
						for x in q.type(statement).sel
					)
					q = q.unique()
					
					def f1(arg1):
						
						x = arg1["x"]
						q1 = arg1["s"].nodes([x])
						
						if q1.isNodePropertyOf(q1.parent().type(("statement","control","for")),("x",)).count():
							return False
						
						q1 = q1.descendants(include_original_selection = True)
						q1 = q1.setMinus(q.init().type(("statement","expression","[")).descendants(include_original_selection = True))
						q1 = q1.setMinus(q.init().type(("statement","expression","(")).descendants(include_original_selection = True))
						q1 = q1.type(("literal",))
						
						return q1.count()
					
					q = q.filter(f1)
					
					for i,x in enumerate(q.sel):
						highlight(x,"%s-%d-%d" % (u"Abfrage.choose",2,i))
				
				if state[u"Abfrage.choose"][3]: # Funktionsaufruf innerhalb einer Schleife
					statements = tuple(("statement","expression",statement) for statement in ("(",))
					ast = tuple(
						x for statement in statements\
						for x in parse_query(ctx1[ct]).type(statement).sel
					)
					for i,x in enumerate(ast):
						ancestor = [None,x]
						while ancestor:
							ancestor = ast_ancestor(ancestor[-1],(("statement","control","for"),("statement","control","while")),ctx1[ct])
							
							if ancestor\
							and (
								ancestor[-1][0][:3] == ("statement","control","while")\
								or id(ancestor[-2]) in (id(z) for z in ancestor[-1][2]["do"])\
							):
								highlight(x,"%s-%d-%d" % (u"Abfrage.choose",3,i))
								break
			
			for i in xrange(3):
				if u"Richtlinien.choose" in state\
				and state[u"Richtlinien.choose"][i]:
					pass
			
			for i in xrange(3):
				if u"Schemata.choose" in state\
				and state[u"Schemata.choose"][i]:
					pass
			
			for i in xrange(2):
				if u"Orthogonale Bereiche.placeholder %d" % i in state\
				and state[u"Orthogonale Bereiche.placeholder %d" % i]:
					pass
				else:
					pass
			
			if u"Komplexitätsklassen.show cards" in state\
			and state[u"Komplexitätsklassen.show cards"]:
				pass
			
			if u"Datenfluss.show cards" in state\
			and state[u"Datenfluss.show cards"]:
				pass
			
			if u"Erläuterung.show cards" in state\
			and state[u"Erläuterung.show cards"]:
				pass
		
		
		# find nodes in selection
		
		
		ast_selected1[0] = None
		ast_selected2 = []
		
		
		# TODO request for accordion
		
		
		if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
			
			
			select = selects[ct]
			
			select_i1 = 0
			if select[0][0] > select[1][0]\
			or (\
				select[0][0] == select[1][0]\
				and select[0][1] > select[1][1]\
			):
				select_i1 = 1
			select_i2 = (select_i1 + 1) % 2
			
			
			for x in ctx1[ct]["nodes"]:
				pos = ctx1[ct]["positions"][id(x)]
				if pos[1][0] < select[select_i2][0]: # end < end
					pass
				elif pos[1][0] == select[select_i2][0]\
				and pos[1][1] < select[select_i2][1]:
					pass
				elif pos[0][0] > select[select_i1][0]: # start > start
					break
				elif pos[0][0] == select[select_i1][0]\
				and pos[0][1] > select[select_i1][1]:
					break
				elif x[0][0] not in {"comment","whitespace","keyword"}:
					ast_selected1[0] = x
			
			for x in ast_selected1[0][1] if ast_selected1[0] else ctx1[ct]["ast"]:
				if not isinstance(x,list):
					continue
				
				pos = ctx1[ct]["positions"][id(x)]
				if pos[1][0] < select[select_i1][0]: # end <= start
					pass
				elif pos[1][0] == select[select_i1][0]\
				and pos[1][1] <= select[select_i1][1]:
					pass
				elif pos[0][0] > select[select_i2][0]: # start >= end
					break
				elif pos[0][0] == select[select_i2][0]\
				and pos[0][1] >= select[select_i2][1]:
					break
				elif x[0][0] not in {"comment","whitespace","keyword"}:
					ast_selected2 += [x]
			
			if not ast_selected1[1]:
				ast_selected1[1] = ast_selected1[0]
		
		
		#print ast_selected1[0] if ast_selected1 else None
		#print [x[0] for x in ast_selected2]
		
		
		# layout editor
		
		
		clip(xs[2],xs[3],ys[5],ys[6])
		
		select = selects[ct]
		
		select_i1 = 0
		if select[0][0] > select[1][0]\
		or (\
			select[0][0] == select[1][0]\
			and select[0][1] > select[1][1]\
		):
			select_i1 = 1
		select_i2 = (select_i1 + 1) % 2
		
		select_s = False
		
		
		scrolls1[ct][0] = min(eh - f3[2][1],scrolls1[ct][0])
		scrolls1[ct][0] = max(0,scrolls1[ct][0])
		
		if not scroll1_resize:
			scrolls1[ct][1] = -1
		
		if not scroll1_resize\
		and scrolls1[ct][1] < 0:
			for i,line in enumerate(lines[ct]):
				cl = caret_line[i]
				
				if cl[2] >= scrolls1[ct][0]:
					scrolls1[ct][1] = i
					scrolls1[ct][2] = cl[2] - scrolls1[ct][0]
					break
		
		if not scroll1_resize\
		and scrolls1[ct][1] < 0:
			scrolls1[ct][1] = len(lines[ct]) - 1
			scrolls1[ct][2] = 0
		
		if scroll1_resize\
		and scrolls1[ct][1] >= 0:
			i = scrolls1[ct][1]
			cl = caret_line[i]
			
			scrolls1[ct][0] = cl[2] - scrolls1[ct][2]
		
		scroll1_resize = False
		
		if scroll_to_caret:
			cl = caret_line[select[1][0]]
			cr = cl[0] + (select[1][1] + min(ec,cl[1] * 8) - cl[1]) // ec
			cr = caret_row[cr]
			scrolls1[ct][0] = max(cr[2] + cr[3] + ys[5] - ys[6],scrolls1[ct][0])
			
			cl = caret_line[select[1][0]]
			cr = cl[0] + max(0,select[1][1] + min(ec,cl[1] * 8) - cl[1] - 1) // ec
			cs = cards_row[cr]
			cr = caret_row[cr]
			scrolls1[ct][0] = min(cr[2] + cs[0],scrolls1[ct][0])
		
		
		# draw decorations
		
		
		measure["draw decorations"] = pygame.time.Clock()
		
		
		sa = {}
		
		draw_sorted = []
		sort_index = 0
		
		clipping = 0
		for i,line in enumerate(lines[ct]):
			
			cl = caret_line[i]
			y = ys[5] - scrolls1[ct][0] + cl[2]
			
			if clipping == 0\
			and y + cl[3] >= ys[5]:
				clipping = 1
			
			if y >= ys[6]\
			and not sa:
				clipping = 2
			
			
			textt = "".join(get_text(line))
			marks = get_marks(i,line)
			
			ind = inds[i]
			
			for n in xrange(max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1):
				
				cs = cards_row[cl[0] + n]
				cr = caret_row[cl[0] + n]
				y = ys[5] - scrolls1[ct][0] + cr[2] + cs[0]
				
				
				# bounding box
				
				
				if not n:
					for j in sa:
						pos = (xs[2] + f3[2][0] * min(ec,ind[1] * 8),)
						sa[j][0] = min(pos[0],sa[j][0])
						sa[j][1] = max(pos[0],sa[j][1])
				else:
					for j in sa:
						sa[j][5] = True
				
				
				# iterate segments
				
				
				markss = [[list(mark)] for mark in marks if max(1,mark[0]) > n * ec and mark[0] <= (n + 1) * ec]
				for mark in markss:
					
					mark[0][0] -= n * ec
					
					if mark[0][1]\
					and mark[0][1][0][1] == "draw anchor":
						
						j = mark[0][1][0][3]
						pos = (xs[2] + f3[2][0] * mark[0][0],y)
						if mark[0][1][0][2] == 1\
						and y < ys[6]:
							sa[j] = [pos[0],pos[0],pos[1],pos[1],[pos],False,None,sort_index]
							sort_index += 1
						
						if j in sa:
							if mark[0][1][0][2] == 2\
							or mark[0][1][0][2] == -1:
								sa[j][0] = min(pos[0],sa[j][0])
								sa[j][1] = max(pos[0],sa[j][1])
								sa[j][3] = pos[1]
								sa[j][4] += [pos]
							if mark[0][1][0][2] == -1:
								
								if clipping == 1:
									sa[j][6] = mark[0][1][2]
									draw_sorted += [sa[j]]
								
								del sa[j]
				
				
				# bounding box
				
				
				if n == max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1 - 1:
					for j in sa:
						texttt = (u" " * min(ec,ind[1] * 8) + ind[0])[n * ec:(n + 1) * ec]
						pos = (xs[2] + f3[2][0] * len(texttt),)
						sa[j][0] = min(pos[0],sa[j][0])
						sa[j][1] = max(pos[0],sa[j][1])
				else:
					for j in sa:
						sa[j][5] = True
		
		draw_sorted.sort(key = lambda x: x[7])
		for sa_j in draw_sorted:
			if sa_j[5]:
				rect1 = (sa_j[4][0][0],xs[3],sa_j[2],sa_j[2] + f3[2][1])
				rect2 = (xs[2],xs[3],sa_j[2] + f3[2][1],sa_j[3])
				rect3 = (xs[2],sa_j[4][-1][0],sa_j[3],sa_j[3] + f3[2][1])
			else:
				rect1 = (sa_j[4][0][0],sa_j[1],sa_j[2],sa_j[2] + f3[2][1])
				rect2 = (sa_j[0],sa_j[1],sa_j[2] + f3[2][1],sa_j[3])
				rect3 = (sa_j[0],sa_j[4][-1][0],sa_j[3],sa_j[3] + f3[2][1])
			
			if "stroke" in sa_j[6]:
				srect(*(rect1 + (sa_j[6]["stroke"],)),lw = 5)
				srect(*(rect2 + (sa_j[6]["stroke"],)),lw = 5 if rect2[2] < rect2[3] else 3)
				srect(*(rect3 + (sa_j[6]["stroke"],)),lw = 5)
			
			if "fill" in sa_j[6]:
				frect(*(rect1 + (sa_j[6]["fill"],)))
				frect(*(rect2 + (sa_j[6]["fill"],)))
				frect(*(rect3 + (sa_j[6]["fill"],)))
		
		
		measure["draw decorations"] = measure["draw decorations"].tick()
		
		
		# draw text content
		
		
		measure["draw text content"] = pygame.time.Clock()
		
		
		sc = [blk]
		sf = [f3]
		
		clipping = 0
		for i,line in enumerate(lines[ct]):
			
			cl = caret_line[i]
			y = ys[5] - scrolls1[ct][0] + cl[2]
			
			if clipping == 0\
			and y + cl[3] >= ys[5]:
				clipping = 1
			
			if y >= ys[6]:
				break
			
			
			textt = "".join(get_text(line))
			marks = get_marks(i,line)
			
			ind = inds[i]
			
			select_c1 = -1
			if i == select[select_i1][0]:
				select_c1 = select[select_i1][1] + min(ec,ind[1] * 8) - ind[1]
			
			select_c2 = -1
			if i == select[select_i2][0]:
				select_c2 = select[select_i2][1] + min(ec,ind[1] * 8) - ind[1]
			
			for n in xrange(max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1):
				
				cs = cards_row[cl[0] + n]
				cr = caret_row[cl[0] + n]
				y = ys[5] - scrolls1[ct][0] + cr[2] + cs[0]
				
				if clipping == 1:
					
					
					# draw selection bg
					
					
					if select_c1 != select_c2 or select_s:
						if select_c1 >= 0 and select_c1 <= ec\
						and select_c2 >= 0 and select_c2 <= ec:
							frect(xs[2] + f3[2][0] * select_c1,xs[3] if select_c2 == ec else xs[2] + f3[2][0] * select_c2,y,y + f3[2][1],bl1)
						elif select_c1 >= 0 and select_c1 <= ec:
							frect(xs[2] + f3[2][0] * select_c1,xs[3],y,y + f3[2][1],bl1)
						elif select_c2 >= 0 and select_c2 <= ec:
							frect(xs[2],xs[3] if select_c2 == ec else xs[2] + f3[2][0] * select_c2,y,y + f3[2][1],bl1)
						elif select_s:
							frect(xs[2],xs[3],y,y + f3[2][1],bl1)
					
					
					# draw caret bg
					
					
					if select_c1 >= 0 and select_c1 <= ec:
						frect(xs[2] + f3[2][0] * select_c1,xs[2] + f3[2][0] * select_c1 + 2,y,y + f3[2][1],bl1)
					if select_c2 >= 0 and select_c2 <= ec:
						frect(xs[2] + f3[2][0] * select_c2,xs[2] + f3[2][0] * select_c2 + 2,y,y + f3[2][1],bl1)
				
				
				# iterate segments
				
				
				markss = [[max(min(ec,ind[1] * 8),n * ec),None]]
				markss += [mark for mark in marks if max(1,mark[0]) > n * ec and mark[0] <= (n + 1) * ec]
				markss += [[(n + 1) * ec,None]]
				markss = [[list(markss[j]),list(markss[j + 1])] for j in xrange(len(markss) - 1)]
				for mark in markss:
					
					if mark[0][1]\
					and mark[0][1][0][1] == "text formatting":
						
						if mark[0][1][0][2] == 1:
							sc += [sc[-1]]
							sf += [sf[-1]]
						elif mark[0][1][0][2] == -1\
						and 1 < len(sc)\
						and 1 < len(sf):
							del sc[-1]
							del sf[-1]
						
						if 2 < len(mark[0][1])\
						and "sc" in mark[0][1][2]:
							sc[-1] = mark[0][1][2]["sc"]
						if 2 < len(mark[0][1])\
						and "sf" in mark[0][1][2]:
							sf[-1] = mark[0][1][2]["sf"]
					
					if mark[0][0] != mark[1][0]:
						
						
						# draw text segments
						
						
						texttt = (u" " * min(ec,ind[1] * 8) + ind[0])[n * ec:(n + 1) * ec]
						
						mark[0][0] -= n * ec
						mark[1][0] -= n * ec
						mark[1][0] = min(len(texttt),mark[1][0])
						
						if clipping == 1:
							if select_c1 >= mark[0][0] and select_c1 <= mark[1][0]\
							and select_c2 >= mark[0][0] and select_c2 <= mark[1][0]:
								text(xs[2] + f3[2][0] * mark[0][0],y,sc[-1],sf[-1],texttt[mark[0][0]:select_c1])
								text(xs[2] + f3[2][0] * select_c1,y,wht,sf[-1],texttt[select_c1:select_c2])
								text(xs[2] + f3[2][0] * select_c2,y,sc[-1],sf[-1],texttt[select_c2:mark[1][0]])
							elif select_c1 >= mark[0][0] and select_c1 <= mark[1][0]:
								text(xs[2] + f3[2][0] * mark[0][0],y,sc[-1],sf[-1],texttt[mark[0][0]:select_c1])
								text(xs[2] + f3[2][0] * select_c1,y,wht,sf[-1],texttt[select_c1:mark[1][0]])
							elif select_c2 >= mark[0][0] and select_c2 <= mark[1][0]:
								text(xs[2] + f3[2][0] * mark[0][0],y,wht,sf[-1],texttt[mark[0][0]:select_c2])
								text(xs[2] + f3[2][0] * select_c2,y,sc[-1],sf[-1],texttt[select_c2:mark[1][0]])
							elif select_s:
								text(xs[2] + f3[2][0] * mark[0][0],y,wht,sf[-1],texttt[mark[0][0]:mark[1][0]])
							else:
								text(xs[2] + f3[2][0] * mark[0][0],y,sc[-1],sf[-1],texttt[mark[0][0]:mark[1][0]])
						
						
						# update select state
						
						
						if select_c1 >= mark[0][0] and select_c1 <= mark[1][0]\
						and select_c2 >= mark[0][0] and select_c2 <= mark[1][0]:
							select_s = False
						elif select_c1 >= mark[0][0] and select_c1 <= mark[1][0]:
							select_s = True
						elif select_c2 >= mark[0][0] and select_c2 <= mark[1][0]:
							select_s = False
				
				
				select_c1 -= ec
				select_c2 -= ec
		
		
		measure["draw text content"] = measure["draw text content"].tick()
		
		
		# draw cards
		
		
		clipping = 0
		for i,line in enumerate(lines[ct]):
			
			cl = caret_line[i]
			y = ys[5] - scrolls1[ct][0] + cl[2]
			
			if clipping == 0\
			and y + cl[3] >= ys[5]:
				clipping = 1
			
			if y >= ys[6]:
				break
			
			if clipping != 1:
				continue
			
			
			ind = inds[i]
			
			for n in xrange(max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1):
				
				cs = cards_row[cl[0] + n]
				cr = caret_row[cl[0] + n]
				y1 = ys[5] - scrolls1[ct][0] + cr[2]
				y2 = y1 + cs[0]
				
				
				# iterate cards
				
				
				for i,c in enumerate(cs[1]):
					
					if not i: y1 += iys[0]
					
					layout_card(c[0],c[1][0],y1,c[1][2],c[1][3],c[2][0],y2)
					
					y1 += c[1][3]
					y1 += iys[0]
		
		
		measure["layout_editor"] = measure["layout_editor"].tick()
	
	
	
	def layout_editor_sides():
		global buttons1
		
		
		measure["layout_editor_sides"] = pygame.time.Clock()
		
		
		clip(xs[1],xs[4],ys[5],ys[6])
		
		select = selects[ct]
		
		frect(xs[1],xs[2] - 2,ys[5],ys[6],gr1)
		
		for i,line in enumerate(lines[ct]):
			
			cl = caret_line[i]
			y = ys[5] - scrolls1[ct][0] + cl[2]
			
			if y >= ys[6]:
				break
			
			ind = inds[i]
			
			if y + cl[3] >= ys[5]:
				text(xs[1],y,blk,f4 if i == select[1][0] else f3,unicode(i + 1).rjust(lc + 1))
		
		
		frect(xs[3] + 2,xs[4],ys[5],ys[6],gr3)
		
		
		h_screen = ys[6] - ys[5]
		h_doc = eh
		h_bar = max(5,int(round(min(1,float(h_screen) / h_doc) * h_screen)))
		h_sp_doc = max(1,h_doc - h_screen)
		h_sp_bar = max(0,h_screen - h_bar)
		
		y_bar = int(round(min(1,float(scrolls1[ct][0]) / h_sp_doc) * h_sp_bar))
		y_mid = min(1,float(scrolls1[ct][0]) / h_sp_doc) * h_screen
		
		frect(xs[3] + 2,xs[4] + 1,ys[5] + y_bar - 1,ys[5] + y_bar + h_bar + 1,gr1)
		
		
		if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
			
			
			minimap_lines = set(id(line[2]) for line in minimap)
			
			ast_selected = ast_selected2 or [ast_selected1[1]]
			ast_selected_ids = set()
			for x in ast_selected:
				ancestors = ast_ancestor(x,(),ctx1[ct])
				ancestors = (z for z in ancestors if id(z) in minimap_lines)
				try: ast_selected_ids |= {id(next(ancestors))}
				except: pass
			
			
			if not ("minimap arranged" in ctx1[ct] and ctx1[ct]["minimap arranged"]):
				ctx1[ct]["minimap arranged"] = True
				
				
				y = 0
				for i,line in enumerate(minimap):
					pos = ctx1[ct]["positions"][id(line[2])]
					cl = caret_line[pos[0][0]]
					cr = cl[0] + (pos[0][1] + min(ec,cl[1] * 8) - cl[1]) // ec
					cs = cards_row[cr]
					cr = caret_row[cr]
					pos = cr[2] + cs[0] + f3[2][1] // 2
					pos = float(pos) / h_doc * h_screen
					
					y = max(pos,y)
					y = min(h_screen,y)
					
					line[1] = y
				
				
				def arrange(j1,j2):
					if j1 >= j2: return
					
					indent = minimap[j1][0] + 1 if j1 >= 0 else 0
					y1 = minimap[j1][1] if j1 >= 0 else 0
					y2 = minimap[j2][1] if j2 < len(minimap) else h_screen
					children = list(j1 + 1 + i for i,line in enumerate(minimap[j1 + 1:j2]) if line[0] == indent)
					extended = [j1] + children + [j2]
					#mindist = min(
					#	(minimap[extended[i + 1]][1] if extended[i + 1] < len(minimap) else h_screen) -\
					#	(minimap[extended[i]][1] if extended[i] >= 0 else 0)\
					#	for i in xrange(len(extended) - 1)
					#)
					
					#if (y2 - y1) <= h_bar\
					#and (float(mindist) * 4 < h_bar or not h_sp_bar): # distribute equally
					if (y2 - y1) <= h_bar: # distribute equally
						
						n = max(1,(j2 - j1) - sum(1 for i in xrange(j1 + 1,j2) if i and minimap[i - 1][0] < minimap[i][0]))
						h = max(0,float(y2 - y1) / n)
						k = 0
						for i in xrange(j1 + 1,j2):
							if not (i and minimap[i - 1][0] < minimap[i][0]):
								k += 1
							minimap[i][1] = y1 + h * k
					
					else: # descend tree
						
						for i in xrange(1,len(extended) - 1):
							arrange(extended[i],extended[i + 1])
				
				
				arrange(-1,len(minimap))
			
			
			n = max(1,len(minimap) - sum(1 for i in xrange(len(minimap) - 1) if minimap[i][0] < minimap[i + 1][0]))
			alpha = min(1,float(h_screen) / n * scroll3)
			
			
			x = xs[3] + 8
			for i in xrange(mc + 1):
				frect(x,x + 1,ys[5],ys[6],wht)
				x += 6
			
			
			indent = 0
			y = ys[5] + minimap[0][1] if minimap else 0
			y = (y - ys[5] - y_mid) * scroll3 + ys[5] + y_mid
			y_start = {}
			y_line = {0:[None,None,None,"structure"]}
			y_end = {}
			selected_y1 = ys[6]
			selected_y2 = ys[5]
			selected_indent = -1
			for i,line in enumerate(minimap):
				
				x_prev = xs[3] + 8
				
				if line[0] not in y_start:
					indent = line[0]
					y_start[line[0]] = y
				else:
					y = ys[5] + line[1]
					y = (y - ys[5] - y_mid) * scroll3 + ys[5] + y_mid
				
				y_end[line[0]] = y
				
				if selected_y2 == ys[6]\
				and line[0] <= selected_indent:
					selected_y2 = y
				
				
				if id(line[2]) in ast_selected_ids:
					if selected_y1 == ys[6]:
						selected_y1 = y
					selected_y2 = ys[6]
					selected_indent = line[0]
				
				x = x_prev + line[0] * 6
				bold = 1 if selected_y1 <= y and y < selected_y2 else 0
				if alpha < .5 and bold:
					frect(x + 1,x + 7,int(round(y)),int(round(y)) + 1,fmt[line[3]][0],alpha * 2)
				else:
					frect(x + 1,x + 7 + bold,int(round(y)),int(round(y)) + 1 + bold,fmt[line[3]][0],alpha)
				
				
				if i == len(minimap) - 1:
					line = list(line)
					line[0] = -1
				
				if line[0] < indent:
					#print line[0], indent, y_start, y_end
					for j in xrange(line[0] + 1,indent + 1):
						
						if j in y_start\
						and j in y_line\
						and y_start[j] != y_end[j]:
							x = x_prev + j * 6
							bold1 = 1 if selected_y1 <= y_start[j] and y_end[j] < selected_y2 else 0
							bold2 = bold1 if alpha >= .5 else 0
							frect(x,x + 1 + bold1,int(round(y_start[j])),int(round(y_end[j])) + 1 + bold2,fmt[y_line[j][3]][0])
						
						if j in y_start: del y_start[j]
					
					indent = line[0]
				
				y_line[line[0] + 1] = line
		
		
		srect(xs[3] + 2,xs[4] + 1,ys[5] + y_bar - 1,ys[5] + y_bar + h_bar + 1,bl1)
		
		
		buttons1[3] += [((xs[1],xs[3],ys[5],ys[6]),("editor",))]
		buttons1[3] += [((xs[3],xs[4],ys[5],ys[6]),("minimap",))]
		
		
		measure["layout_editor_sides"] = measure["layout_editor_sides"].tick()


