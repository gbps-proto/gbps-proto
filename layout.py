


def resize():
	global surf,surf_mask
	
	if ctx1[ct]\
	and not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
		ctx1[ct]["minimap arranged"] = False
	
	pygame.display.set_mode((width,height),pygame.DOUBLEBUF | pygame.RESIZABLE,32)
	
	data = pygame.surfarray.pixels2d(pygame.display.get_surface()).data
	surf = cairo.ImageSurface.create_for_data(data,cairo.FORMAT_RGB24,width,height)
	
	data = array.array("c",chr(0) * width * height * 4)
	surf_mask = cairo.ImageSurface.create_for_data(data,cairo.FORMAT_ARGB32,width,height,width * 4)



def validate_editor1():
	global inds
	
	inds = [None] * len(lines[ct]) # indents
	for i,line in enumerate(lines[ct]):
		textt = "".join(get_text(line))
		match = re.search("(\\t*)",textt)
		ind = len(match.group(1))
		inds[i] = (textt[ind:],ind)



def validate_editor2():
	global ec,er
	
	ec = max(1,(xs[3] - xs[2] - 2) // f3[2][0]) # editor cols
	er = 0
	for i,line in enumerate(lines[ct]):
		
		ind = inds[i]
		
		er += max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1 # editor rows



def validate_editor3():
	global eh,caret_line,caret_row,cards_row
	
	caret_line = [None] * len(lines[ct])
	caret_row = [None] * er
	cards_row = [None] * er
	
	j = 0
	eh = 0
	for i,line in enumerate(lines[ct]):
		
		
		marks = get_marks(i,line)
		
		ind = inds[i]
		
		
		caret_line[i] = [j,ind[1],eh,0] # starting at row, indent, y, total height of rows + cards
		
		for n in xrange(max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1):
			
			
			caret_row[j] = [i,ind[1],eh,0,n] # line, indent, y, total height of row + cards, row of line
			cards_row[j] = [0,[]] # total height of cards without row, cards
			
			
			h = 0
			markss = [[list(mark)] for mark in marks if max(1,mark[0]) > n * ec and mark[0] <= (n + 1) * ec]
			for mark in markss:
				
				mark[0][0] -= n * ec
				
				if mark[0][1]\
				and mark[0][1][0][1] == "card static":
					
					pos = (xs[2] + f3[2][0] * mark[0][0],0)
					
					if not h: h += iys[0]
					
					c1 = (mark[0][1][2],get_card_size(mark[0][1][2]),pos)
					cards_row[j][1] += [c1]
					
					h += c1[1][3]
					h += iys[0]
			
			
			eh += h
			eh += f3[2][1]
			
			caret_row[j][3] = eh - caret_row[j][2]
			cards_row[j][0] = h
			
			j += 1
		
		caret_line[i][3] = eh - caret_line[i][2]



def validate_minimap():
	global minimap,mc
	
	minimap = []
	mc = 16 # minimap cols
	
	if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
		
		def traverse_recur(ast,minimap,depth = 0):
			global mc
			
			mc = min(32,max(mc,depth + 1))
			
			for x in ast:
				if isinstance(x,list):
					
					
					typee = None
					if x[0][:3] in set(("statement","expression",statement) for statement in ("(","await","for","if"))\
					or x[0][:3] in set(("statement","control",statement) for statement in ("assert","break","case","continue","for","goto","if","raise","return","switch","throw","try","while","yield"))\
					or x[0][:4] == ("statement","expression","op","new"):
						typee = "control"
					elif x[0][:3] in set(("statement","expression",statement) for statement in ("[",".","op"))\
					or x[0][:3] == ("statement","control","del")\
					or x[0][:1] == ("identifier",):
						typee = "data"
					elif x[0][:1] in {("literal",),("brackets",),("statement",)}:
						typee = "structure"
					
					if typee:
						minimap += [[depth,0,x,typee]]
						len_prev = len(minimap)
						
						traverse_recur(x[1],minimap,depth + 1)
						
						if x[0][:1] == ("brackets",)\
						and len_prev == len(minimap):
							del minimap[-1]
		
		
		traverse_recur(ctx1[ct]["ast"],minimap)



# "text formatting"  state
# "text decoration"  state

# "draw inline"      pos, extent, nocaret
# "draw spacing"     pos, extent, nocaret
# "draw anchor"      anchors, nocaret

# "card active"      range, nested, extern
# "card static"      pos, extern

# "readonly"         range, nested, readonly
# "rewrite"          range, nested, readonly
# "configure"        range, nested



def get_flags(selectt):
	flags = set()
	
	for i,x in enumerate(line):
		if isinstance(x,list):
			if x[0][1] == "text formatting":
				flags |= {"state"}
			elif x[0][1] == "text decoration":
				flags |= {"state"}
			elif x[0][1] == "draw inline":
				flags |= {"pos","extent","nocaret"}
			elif x[0][1] == "draw spacing":
				flags |= {"pos","extent","nocaret"}
			elif x[0][1] == "draw anchor":
				flags |= {"anchors","nocaret"}
			elif x[0][1] == "card active":
				flags |= {"range","nested","extern"}
			elif x[0][1] == "card static":
				flags |= {"pos","extern"}
			elif x[0][1] == "readonly":
				flags |= {"range","nested","readonly"}
			elif x[0][1] == "rewrite":
				flags |= {"range","nested","readonly"}
			elif x[0][1] == "configure":
				flags |= {"range","nested"}
	
	return flags



def get_text(line,rewrite = True,configure = False):
	textt = []
	
	depth_rewrite = 0
	depth_configure = 0
	
	for x in line:
		if isinstance(x,list):
			if x[0][1] == "text formatting":
				pass
			elif x[0][1] == "text decoration":
				pass
			elif x[0][1] == "draw inline":
				pass
			elif x[0][1] == "draw spacing":
				pass
			elif x[0][1] == "draw anchor":
				pass
			elif x[0][1] == "card active":
				pass
			elif x[0][1] == "card static":
				pass
			elif x[0][1] == "readonly":
				pass
			elif x[0][1] == "rewrite":
				depth_rewrite += x[0][2]
				
				if not rewrite\
				and x[0][2] > 0:
					textt += get_text(x[1],rewrite,configure)
			elif x[0][1] == "configure":
				depth_configure += x[0][2]
				
				if configure\
				and x[0][2] > 0:
					textt += get_text(x[1],rewrite,configure)
		else:
			if not rewrite\
			and depth_rewrite > 0:
				pass
			if configure\
			and depth_configure > 0:
				pass
			else:
				textt += [x]
	
	return textt



def get_segment(line,off):
	end = (0,0)
	
	for i,x in enumerate(line):
		if isinstance(x,list):
			if x[0][1] == "text formatting":
				pass
			elif x[0][1] == "text decoration":
				pass
			elif x[0][1] == "draw inline":
				pass
			elif x[0][1] == "draw spacing":
				pass
			elif x[0][1] == "draw anchor":
				pass
			elif x[0][1] == "card active":
				pass
			elif x[0][1] == "card static":
				pass
			elif x[0][1] == "readonly":
				pass
			elif x[0][1] == "rewrite":
				pass
			elif x[0][1] == "configure":
				pass
		else:
			end = (i,len(x))
			
			if off < len(x):
				return (i,off)
			
			off -= len(x)
	
	return end



def get_marks(i,line):
	marks = []
	
	ind = inds[i]
	
	i = min(ec,ind[1] * 8) - ind[1]
	for x in line:
		if isinstance(x,list):
			marks += [[i,x]]
			
			if x[0][1] == "text formatting":
				pass
			elif x[0][1] == "text decoration":
				pass
			elif x[0][1] == "draw inline":
				pass
			elif x[0][1] == "draw spacing":
				pass
			elif x[0][1] == "draw anchor":
				pass
			elif x[0][1] == "card active":
				pass
			elif x[0][1] == "card static":
				pass
			elif x[0][1] == "readonly":
				pass
			elif x[0][1] == "rewrite":
				pass
			elif x[0][1] == "configure":
				pass
		else:
			i += len(x)
	
	return marks



blk = (0,0,0)
wht = (1,1,1)
gr1 = (.92,.92,.92)
gr2 = (.92,.92,.92)
gr3 = (.98,.98,.98)
grn = (.4,.8,.4)
org = (1,.5,0)
red = (1,0,0)
yl1 = (1,1,.5)
yl2 = (1,1,.75)
bl1 = (.1,.3,.6)
bl2 = (.25,.5,1)
bl3 = (.7,.8,1)
bl4 = (.85,.9,.95)

def ccn(n):
	return [(.8,0,.2),(.9,.6,0),(0,.6,0),(.25,.5,1)][n % 4]


# font face, size, (char width, line height, char height), weight

f1 = ("Droid Sans",13,(0,16,9),cairo.FONT_WEIGHT_NORMAL)
f2 = ("Droid Sans",13,(0,16,9),cairo.FONT_WEIGHT_BOLD)
f3 = ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)
f4 = ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)

def blank(n): # blank line, shortcut for (line height - char height) + n * line height
	return (n + 1) * f1[2][1] - f1[2][2]


fmt = {
	"default": [blk,f3],
	#"keyword": [(.4,.2,.2),f4],
	#"call": [(.1,.4,.4),None],
	"identifier": [(.1,.25,.1),None],
	#"literal": [(.5,.4,.5),None],
	"comment": [(.1,.3,.6),None],
	
	"keyword": [blk,f4],
	"call": [(.09,.53,.62),None],
	"literal": [(.67,.48,.13),None],
	
	"structure": [(.85,.8,.72),None],
	#"control": [(1,0,0),None],
	#"data": [(.6,.8,.3),None],
	"control": [(.25,.5,1),None],
	"data": [(.7,.8,1),None],
}



measure = {}



def layout(update = {0}): ## TODO caret_line,caret_row,inds inconsistency
	global scroll2,buttons1,buttons3_1,buttons3_2
	global txs,tys,ixs,iys,xs,ys
	
	
	measure["layout"] = pygame.time.Clock()
	
	measure["layout toplevel 1"] = pygame.time.Clock()
	
	
	ctx = cairo.Context(surf)
	ctx_mask = cairo.Context(surf_mask)
	
	
	execfile("layout_editor.py")
	execfile("layout_accordion.py")
	
	
	execfile("draw_prs.py")
	execfile("draw_cons.py")
	
	
	if 0 in update or 3 in update:
		validate_editor1()
	
	
	#lc = int(math.ceil(math.log10(len(lines[ct]) + 1))) # lnum cols
	lc = len(unicode(len(lines[ct]))) # lnum cols
	
	
	txs = (4,100) # tab xs
	tys = (4,30) # tab ys
	axs = (10,) # accordion xs
	ays = (10,22) # accordion ys
	cys = (4,22) # context menu ys
	ixs = (20,10,280) # info card xs
	iys = (20,10) # info card ys
	
	
	xs = (0,1,(lc + 2) * f3[2][0] + 2,width - txs[0] - 302 - (mc + 1) * 6 - 8,width - txs[0] - 302,width - txs[0] - 301,width - 301,width - 300,width - 1,width) # layout xs
	
	tc = max(1,(xs[6] - xs[1] - txs[0]) // (txs[0] + txs[1])) # tab cols
	tr = (len(titles) - 1) // tc + 1 # tab rows
	th = tys[0] + (tys[0] + tys[1]) * tr # tab height
	
	ys = (0,1,th + 1,th + 2,th + tys[1],th + tys[1] + 1,height - 2,height - 1,height) # layout ys
	
	sds_in = (3,-5,-5,3) # shadow dists in
	sds_out = (-3,5,5,-3) # shadow dists out
	sb_in = 6 # shadow blur size in
	sb_out = 6 # shadow blur size out
	
	
	titles[:] = [title or files[n][1] or u"Neuer Tab" for n,title in enumerate(titles)] # this has effect on ah !
	
	
	measure["layout toplevel 1"] = measure["layout toplevel 1"].tick()
	
	measure["layout toplevel 2"] = pygame.time.Clock()
	
	
	if 0 in update or 3 in update:
		validate_editor2()
	
	
	if 0 in update or 3 in update:
		validate_editor3()
	
	
	measure["layout toplevel 2"] = measure["layout toplevel 2"].tick()
	
	measure["layout toplevel 3"] = pygame.time.Clock()
	
	
	if anim_tick >= 0\
	and ca == 13:
		scroll2 = [0,-1]
	
	if scroll2[1] >= 0:
		scroll2[0] = scroll2[1]
	
	ah = -1 # accordion height
	ahs = [None] * len(tools)
	for n,tool in enumerate(tools):
		ah_prev = ah
		
		if anim_tick >= 0:
			if n == ca:
				tool_ctrs[n] += anim_tick
				tool_ctrs[n] = min(150,tool_ctrs[n])
			else:
				tool_ctrs[n] -= anim_tick
				tool_ctrs[n] = max(0,tool_ctrs[n])
		
		ahs[n] = (
			0 if tool_ctrs[n] == 150 else get_accordion_height(n,False),
			0 if tool_ctrs[n] == 0 else get_accordion_height(n,True),
		)
		
		ah += ays[1]
		ah += ays[0]
		ah += (ahs[n][1] * tool_ctrs[n] + ahs[n][0] * (150 - tool_ctrs[n])) // 150
		ah += ays[0]
		
		if anim_tick >= 0\
		and n == ca:
			if ca != 13: scroll2[0] = max(ah + ys[1] - ys[6],scroll2[0])
			scroll2[0] = min(ah_prev + 1,scroll2[0])
	
	scroll2[0] = min(ah + ys[1] - ys[6],scroll2[0])
	scroll2[0] = max(0,scroll2[0])
	
	if scroll2[1] < 0\
	or anim_tick >= 0:
		scroll2[1] = scroll2[0]
	
	
	#print update, anim_tick,tool_ctrs, ca, scroll2
	
	
	measure["layout toplevel 3"] = measure["layout toplevel 3"].tick()
	
	
	# clear
	
	
	if 0 in update: # complete redraw
		clip(xs[0],xs[9],ys[0],ys[8])
		
		frect(xs[0],xs[9],ys[0],ys[8],wht)
		
		buttons1 = [[] for i in xrange(5)]
	
	if 1 in update: # update tabs
		
		buttons1[1] = []
	
	if 2 in update: # update active tab
		pass
	
	if 3 in update: # update editor
		clip(xs[1],xs[4],ys[5],ys[6])
		
		frect(xs[1],xs[4],ys[5],ys[6],wht)
		
		buttons1[3] = []
	
	if 4 in update: # update accordion
		clip(xs[7],xs[8],ys[1],ys[6])
		
		frect(xs[7],xs[8],ys[1],ys[6],wht)
		
		buttons1[4] = []
	
	if 5 in update: # update context menu
		pass
	
	buttons3_1 = []
	buttons3_2 = []
	
	
	# tabs
	
	
	if 0 in update or 1 in update:
		
		
		clip(xs[1],xs[6],ys[1],ys[2])
		
		shadow_rect(xs[1],xs[6],ys[1],ys[2])
		shadow_in(xs[1],xs[6],ys[1],ys[2])
		
		
		clip(xs[5],xs[6],ys[2],ys[6])
		
		shadow_rect(xs[5],xs[6],ys[2],ys[6])
		shadow_out(xs[5] + sds_out[0] - sb_out,xs[5],ys[2],ys[6])
		
		
		m = 0
		for n,title in enumerate(titles):
			
			if m == rearrange2:
				m += 1
			
			if n == rearrange1:
				continue
			
			
			clip(xs[1],xs[6],ys[1],ys[2])
			
			i = m % tc
			j = m // tc
			(x,y) = (xs[1] + txs[0] + (txs[0] + txs[1]) * i,ys[1] + tys[0] + (tys[0] + tys[1]) * j)
			
			shadow_out(x,x + txs[1],y,y + tys[1])
			
			srect(x,x + txs[1],y,y + tys[1],blk)
			frect(x + 1,x + txs[1] - 1,y + 1,y + tys[1] - 1,gr1 if n == ct else wht)
			
			
			# tab label
			
			
			clip(x + 1,min(xs[6],x + txs[1] - 1),y + 1,min(ys[2],y + tys[1] - 1))
			
			text_fit(x + 10,y + (tys[1] - f1[2][2] + 1) // 2,txs[1] - 20,blk,f1,title)
			
			
			m += 1
		
		if rearrange1 >= 0:
			
			
			clip(xs[1],xs[6],ys[1],ys[2] + tys[1] // 2)
			
			n = rearrange1
			
			(x,y) = rearrange_pos2
			
			srect(x,x + txs[1],y,y + tys[1],blk,.75)
			frect(x + 1,x + txs[1] - 1,y + 1,y + tys[1] - 1,gr1 if n == ct else wht,.75)
		
		
		for n,title in enumerate(titles):
			i = n % tc
			j = n // tc
			(x,y) = (xs[1] + txs[0] + (txs[0] + txs[1]) * i,ys[1] + tys[0] + (tys[0] + tys[1]) * j)
			
			buttons1[1] += [((x - txs[0] // 2,x + txs[1] + txs[0] // 2,y - tys[0] // 2,y + tys[1] + tys[0] // 2),(actionTab,n))]
	
	
	# active tab
	
	
	if 0 in update or 1 in update or 2 in update:
		
		
		clip(xs[1],xs[4],ys[3],ys[4])
		
		frect(xs[1],xs[4],ys[3],ys[4],gr1)
		text(xs[1] + 9,ys[3] + (tys[1] - f1[2][2] + 1) // 2 - 1,blk,f1,titles[ct])
	
	
	# editor text
	
	
	if 0 in update or 3 in update:
		
		
		layout_editor()
	
	
	# editor sides
	
	
	if 0 in update or 3 in update:
		
		
		layout_editor_sides()
	
	
	# accordion
	
	
	if 0 in update or 4 in update:
		
		
		y = ys[1] - scroll2[0] - 1
		for n,tool in enumerate(tools):
			
			
			clip(xs[7],xs[8],ys[1],ys[6])
			
			frect(xs[7],xs[8],y,y + ays[1],gr1)
			text(xs[7] + 9,y + (ays[1] - f1[2][2] + 1) // 2,blk,f2 if n == ca else f1,tool)
			frect(xs[7],xs[8],y,y + 1,blk)
			frect(xs[7],xs[8],y + ays[1] - 1,y + ays[1],blk)
			
			y += ays[1]
			y_prev = y
			y += ays[0]
			y += (ahs[n][1] * tool_ctrs[n] + ahs[n][0] * (150 - tool_ctrs[n])) // 150
			y += ays[0]
			
			
			clip(xs[7],xs[8],y_prev,y)
			
			y_prev += ays[0]
			layout_accordion(n,tool_ctrs[n] > 75,y_prev)
			y_prev -= ays[0]
			frect(xs[7],xs[8],y_prev,y,wht,1 - abs(tool_ctrs[n] - 75) / 75.)
		
		
		y = ys[1] - 1
		for n,tool in enumerate(tools):
			y_prev = y
			
			y += ays[1]
			y += ays[0]
			y += (ahs[n][1] * tool_ctrs[n] + ahs[n][0] * (150 - tool_ctrs[n])) // 150
			y += ays[0]
			
			if n == len(tools) - 1:
				y = max(ys[6],y)
			
			if n != ca:
				buttons1[4] += [((xs[7],xs[8],y_prev - scroll2[0],y - scroll2[0]),(actionAccordion,n))]
	
	
	# main layout
	
	
	if 0 in update or True:
		
		
		clip(xs[0],xs[9],ys[0],ys[8])
		
		srect(xs[0],xs[9],ys[0],ys[7],blk)
		frect(xs[4],xs[4] + 1,ys[2],ys[6],blk)
		frect(xs[6],xs[6] + 1,ys[1],ys[6],blk)
		frect(xs[1],xs[4],ys[2],ys[2] + 1,blk)
		frect(xs[1],xs[4],ys[4],ys[4] + 1,blk)
		srect(xs[0],xs[9],ys[7],ys[8],wht)
	
	
	# context menu
	
	
	buttons3_1 += [((xs[1],xs[6],ys[1],ys[4]),[
		(u"1. Tab speichern",(actionSave,{"set_ct": True})), #TODO set ct
		(u"2. Alle Tabs speichern",(actionSaveAll,None)),
		(u"3. Tab schließen",(actionClose,{"set_ct": True})), #TODO set ct
		(u"4. Tab wiederherstellen",(actionRestore,None)),
		(u"5. Neuer Tab",(actionNew,None)),
		(u"6. Tab öffnen",(actionOpen,None)),
		(u"7. Titel festlegen",(actionTitle,{"set_ct": True})), #TODO set ct
	])]
	
	buttons3_1 += [((xs[1],xs[4],ys[5],ys[6]),[
		(u"1. Rückgängig",(actionUndo,None)),
		(u"2. Wiederholen",(actionRedo,None)),
		(u"3. Ausschneiden",(actionCut,None)),
		(u"4. Kopieren",(actionCopy,None)),
		(u"5. Einfügen",(actionPaste,None)),
		(u"6. Alles markieren",(actionSelectAll,None)),
	])]
	
	button = ((xs[7],xs[8],ys[1],ys[6]),[
		(u"X. Alle Reiter schließen",(actionAccordion,13)),
		(u"0. Dateien / Tabs",(actionAccordion,0)),
		(u"1. Abfrage",(actionAccordion,1)),
		(u"2. Umschreiben",(actionAccordion,2)),
		(u"3. Merkmale",(actionAccordion,3)),
		(u"4. Konfigurieren",(actionAccordion,4)),
		(u"5. Richtlinien",(actionAccordion,5)),
		(u"6. Code-Schemata",(actionAccordion,6)),
		(u"7. Orthogonale Bereiche",(actionAccordion,7)),
		(u"8. Komplexitätsklassen",(actionAccordion,8)),
		(u"9. Datenfluss",(actionAccordion,9)),
		(u"10. Erläuterung",(actionAccordion,10)),
		(u"11. Knoten-Eigenschaften",(actionAccordion,11)),
		(u"12. Über das Programm",(actionAccordion,12)),
	])
	buttons3_1 += [button]
	
	if "advanced mode" in settings and settings["advanced mode"]:
		button = button[1]
		button += [
			(u"A. Interaktive Ausführung",(actionAccordion,14)),
			(u"A. Datei-Verlauf",(actionAccordion,15)),
			(u"A. Knoten-Navigator",(actionAccordion,16)),
			(u"A. Knoten-Liste",(actionAccordion,17)),
		]
	
	
	if 0 in update or 5 in update or True:
		
		
		if menu_actions:
			
			buttons1 = [[] for i in xrange(5)]
			buttons3_1 = []
			buttons3_2 = []
			
			y = cys[0] + len(menu_actions) * (cys[0] + cys[1])
			off = [
				100,
				-100,
				cys[0] + cys[1] // 2,
				cys[0] + cys[1] // 2 - y,
			]
			pos = list(menu_pos1)
			pos[0] = min(xs[9] + off[1],pos[0])
			pos[0] = max(off[0],pos[0])
			pos[1] = min(ys[7] + off[3],pos[1])
			pos[1] = max(off[2],pos[1])
			pos = [
				pos[0] - off[0],
				pos[0] - off[1],
				pos[1] - off[2],
				pos[1] - off[3],
			]
			
			
			clip(xs[0],xs[9],ys[0],ys[8])
			
			frect(pos[0],pos[1],pos[2],pos[3],wht)
			srect(pos[0],pos[1],pos[2],pos[3],blk)
			
			y = pos[2]
			for menu_action in menu_actions:
				hover = get_button(menu_pos2,[((pos[0],pos[1],y,y + cys[0] + cys[1]),)])
				text(pos[0] + 10,y + cys[0] + (cys[1] - f1[2][2] + 1) // 2,blk,f2 if hover else f1,menu_action[0])
				buttons3_2 += [((pos[0],pos[1],y,y + cys[0] + cys[1]),menu_action[1])]
				
				y += cys[0] + cys[1]
	
	
	# flip buffers
	
	
	pygame.display.flip()
	
	
	measure["layout"] = measure["layout"].tick()
	
	
	print "---"
	for m in measure.items(): print "%5d %s" % (m[1],m[0])


