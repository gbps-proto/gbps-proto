


def get_row(y): #TODO
	for i,cr in enumerate(caret_row):
		if y < cr[2] + cr[3]:
			cs = cards_row[i]
			return i - 1 if y < cr[2] + cs[0] // 2 else i
	
	return er - 1



def get_button(pos,buttons):
	for button in buttons:
		if pos[0] >= button[0][0]\
		and pos[0] < button[0][1]\
		and pos[1] >= button[0][2]\
		and pos[1] < button[0][3]:
			return button
	
	return None



def rearrange(i1,i2,l):
	tmp = l[i1]
	del l[i1]
	l[i2:i2] = [tmp]



## TODO KMOD_CTRL: caret: expand to node; home/end: first/last line



def validate_caret(i,rel = (0,0),forget = True):
	
	caret = selects[ct][i]
	
	if rel[0]: # vertical, restore column
		cl = caret_line[caret[0]]
		
		if caret[2] < 0:
			caret[2] = (caret[1] + min(ec,cl[1] * 8) - cl[1]) % ec
		
		j = (caret[1] + min(ec,cl[1] * 8) - cl[1]) // ec
		cc = caret[2]
		
		if rel[0] < 0\
		and cl[1] * 8 >= ec\
		and j == 1: # skip first row with nothing but indent
			j = 0
		
		cr = cl[0] + j + rel[0]
		cr = max(0,cr)
		cr = min(er - 1,cr)
		cr = caret_row[cr]
		
		caret[0] = cr[0]
		caret[1] = cr[4] * ec - min(ec,cr[1] * 8) + cr[1] + cc
		
		cl = caret_line[caret[0]]
		
		if rel[0] > 0\
		and cl[1] * 8 >= ec\
		and caret[1] < cl[1]: # skip first row with nothing but indent
			caret[1] += ec
	
	if rel[1]: # horizontal, wrap at ends
		cl = caret_line[caret[0]]
		
		caret[1] += rel[1]
		
		if caret[0] > 0 and caret[1] < cl[1]:
			caret[0] -= 1
			caret[1] = len("".join(get_text(lines[ct][caret[0]])))
		elif caret[0] < len(lines[ct]) - 1 and caret[1] > len("".join(get_text(lines[ct][caret[0]]))):
			caret[0] += 1
			caret[1] = 0
	
	# validate caret
	
	caret[0] = max(0,caret[0])
	caret[0] = min(len(lines[ct]) - 1,caret[0])
	
	cl = caret_line[caret[0]]
	
	caret[1] = max(cl[1],caret[1])
	caret[1] = min(len("".join(get_text(lines[ct][caret[0]]))),caret[1])
	
	if forget: # not vertical, forget column
		caret[2] = -1



def expand_selection(rel = 0):
	
	return # TODO remove node expansion # TODO add token/line/paragraph expansion on multi-click
	
	if request_parse: return
	
	
	select = selects[ct]
	
	
	if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
		
		pos = [0,0]
		poss = [None,None]
		
		def traverse_recur(ast):
			
			for x in ast:
				pos_prev = tuple(pos)
				
				if isinstance(x,list):
					if traverse_recur(x[1]):
						return True
				else:
					
					if x.count("\n"):
						pos[0] += x.count("\n")
						match = re.search("[^\\n]*\\Z",x)
						pos[1] = len(match.group(0))
					else:
						pos[1] += len(x)
					
					if not poss[0]:
						if pos[0] > select[1][0]\
						or (pos[0] == select[1][0] and pos[1] > select[1][1]):
							poss[0] = pos_prev
					
					if not poss[1]:
						if pos[0] > select[1][0]\
						or (pos[0] == select[1][0] and pos[1] >= select[1][1]):
							poss[1] = tuple(pos)
					
					if poss[0] and poss[1]:
						return True
		
		traverse_recur(ctx1[ct]["ast"])
		
		if poss[0] and poss[1]:
			if rel < 0:
				select[1] = [poss[0][0],poss[0][1],poss[0][1]]
			
			elif rel > 0:
				select[1] = [poss[1][0],poss[1][1],poss[1][1]]



execfile("evtloop_actions.py")



clock1 = pygame.time.Clock()
clock2 = pygame.time.Clock()
times1 = []
timer_set = False

scroll_to_caret = False # TODO

request_parse = True
request_resize = False
request_layout = {0}
request_save = False
discard = 2
while True:
	
	
	clock1.tick()
	
	
	if files[ct][1]:
		pass # TODO check external modification
	
	
	if request_parse:
		request_parse = False
		
		if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
			
			ast_selected1[1] = None
			
			ctx1[ct] = {
				"text": "\n".join("".join(get_text(line)) for line in lines[ct]),
				"highlighted": False,
				"minimap arranged": False,
			}
			parse(ctx1[ct],rewrite = True)
			printt(ctx1[ct])
		
		validate_minimap()
	
	
	if request_resize:
		request_resize = False
		
		resize()
	
	
	if max(anim_ctrs) and not timer_set: # start or shift animation
		pygame.time.set_timer(pygame.USEREVENT,50) # 20 fps
		timer_set = True
	
	
	if request_layout:
		if max(anim_ctrs) and anim_tick < 0: # start animation
			clock2.tick()
			anim_tick = 50 # 20 fps
		elif anim_tick >= 0: # continue animation
			anim_tick = clock2.tick()
		
		layout(request_layout)
		request_layout = set()
		
		scroll_to_caret = False
		
		validate_caret(0,forget = False)
		validate_caret(1,forget = False)
		
		
		if request_save:
			request_save = False
			
			save_tabs()
		else:
			pygame.time.set_timer(pygame.USEREVENT + 1,1000) # 1 second
		
		save_state() # TODO save_history
		
		
		time2 = clock1.tick()
		
		time1prev = max(times1) if times1 else 0
		while len(times1) > 1 and sum(times1) > 3000:
			times1 = times1[1:]
		times1 += [time2]
		times1 = times1[-10:]
		time1curr = max(times1) if times1 else 0
		
		if time1curr != time1prev:
			pygame.display.set_caption("%s [%d]" % (titlee,time1curr))
	
	
	if not max(anim_ctrs): # stop animation
		anim_tick = -1
	
	
	dirty_editor = False
	
	events = pygame.event.get()
	if not events: discard = 0
	elif discard: discard -= 1
	for e in events or [pygame.event.wait()]:
		
		
		buttons1_flat = tuple(y for x in buttons1 for y in x)
		
		
		if discard:
			if e.type == pygame.KEYDOWN:
				continue
			
			if e.type == pygame.MOUSEBUTTONDOWN:
				mousedown = get_button(e.pos,buttons1_flat)
				
				if e.button == 1:
					if mousedown\
					and mousedown[1][0] == actionTab:
						pass
					else:
						continue
				elif e.button in {4,5}:
					if get_button(e.pos,[((xs[1],xs[6],ys[1],ys[2]),)]):
						pass
					else:
						continue
				else:
					continue
			
			if e.type == pygame.MOUSEMOTION:
				continue
			
			if e.type == pygame.MOUSEBUTTONUP:
				mousedown = get_button(e.pos,buttons1_flat)
				
				if e.button == 1:
					if mousedown\
					and mousedown[1][0] == actionTab:
						pass
					else:
						continue
				else:
					continue
		
		
		if dirty_editor:
			dirty_editor = False
			
			validate_editor1()
			validate_editor2()
			validate_editor3()
		
		
		select = selects[ct]
		
		select_i1 = 0
		if select[0][0] > select[1][0]\
		or (\
			select[0][0] == select[1][0]\
			and select[0][1] > select[1][1]\
		):
			select_i1 = 1
		select_i2 = (select_i1 + 1) % 2
		
		
		if e.type == pygame.QUIT:
			exit()
		
		if e.type == pygame.KEYDOWN:
			print "KD",e.key,pygame.key.get_mods(),repr(e.unicode)
			
			if e.key == 27: # escape, quit
				exit()
			
			elif e.key == 273: # up
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT)\
				and select[0] != select[1]:
					select[:] = [select[select_i2],select[select_i1]]
				
				validate_caret(1,(-1,0),forget = False)
				
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
					select[0] = list(select[1])
				
				
				ast_selected1[1] = None
				
				scroll_to_caret = True
			
			elif e.key == 274: # down
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT)\
				and select[0] != select[1]:
					select[:] = [select[select_i1],select[select_i2]]
				
				validate_caret(1,(1,0),forget = False)
				
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
					select[0] = list(select[1])
				
				
				ast_selected1[1] = None
				
				scroll_to_caret = True
			
			elif e.key == 275: # right
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT)\
				and select[0] != select[1]:
					select[:] = [select[select_i1],select[select_i2]]
				
				validate_caret(1,(0,1))
				
				if pygame.key.get_mods() & pygame.KMOD_CTRL:
					expand_selection(1)
				
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
					select[0] = list(select[1])
				
				
				ast_selected1[1] = None
				
				scroll_to_caret = True
			
			elif e.key == 276: # left
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT)\
				and select[0] != select[1]:
					select[:] = [select[select_i2],select[select_i1]]
				
				validate_caret(1,(0,-1))
				
				if pygame.key.get_mods() & pygame.KMOD_CTRL:
					expand_selection(-1)
				
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
					select[0] = list(select[1])
				
				
				ast_selected1[1] = None
				
				scroll_to_caret = True
			
			elif e.key == 278: # home
				
				if pygame.key.get_mods() & pygame.KMOD_CTRL:
					cl = caret_line[0]
					
					select[1] = [0,cl[1],-1]
				else:
					cl = caret_line[select[1][0]]
					select[1][1] += min(ec,cl[1] * 8) - cl[1]
					
					select[1][1] = ((select[1][1] - 1) // ec) * ec
					
					select[1][1] -= min(ec,cl[1] * 8) - cl[1]
				
				validate_caret(1)
				
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
					select[0] = list(select[1])
				
				
				ast_selected1[1] = None
				
				scroll_to_caret = True
			
			elif e.key == 279: # end
				
				if pygame.key.get_mods() & pygame.KMOD_CTRL:
					select[1] = [len(lines[ct]) - 1,len("".join(get_text(lines[ct][-1]))),-1]
				else:
					cl = caret_line[select[1][0]]
					select[1][1] += min(ec,cl[1] * 8) - cl[1]
					
					select[1][1] = (select[1][1] // ec + 1) * ec
					
					select[1][1] -= min(ec,cl[1] * 8) - cl[1]
				
				validate_caret(1)
				
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
					select[0] = list(select[1])
				
				
				ast_selected1[1] = None
				
				scroll_to_caret = True
			
			elif e.key == 8: # backspace
				
				if select[0] == select[1]:
					if select[1][1] > 0: # remove char on left side
						select[0][1] -= 1
					elif select[1][0] > 0: # join with prev line
						select[0][0] -= 1
						select[0][1] = len("".join(get_text(lines[ct][select[1][0] - 1])))
				
				if select[0] != select[1]: # remove selection
					seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
					seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
					lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
						lines[ct][select[select_i1][0]][:seg1[0]] + [
							lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
							lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
						] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
					]
					select[select_i2] = list(select[select_i1])
				
				select[1][2] = -1
				
				
				request_parse = True
				dirty_editor = True
				
				scroll_to_caret = True
			
			elif e.key == 127: # delete
				
				if select[0] == select[1]:
					if select[1][1] < len("".join(get_text(lines[ct][select[1][0]]))): # remove char on right side
						select[1][1] += 1
					elif select[1][0] < len(lines[ct]) - 1: # join with next line
						
						cl = caret_line[select[1][0] + 1]
						
						select[1][0] += 1
						select[1][1] = cl[1]
				
				if select[0] != select[1]: # remove selection
					seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
					seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
					lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
						lines[ct][select[select_i1][0]][:seg1[0]] + [
							lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
							lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
						] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
					]
					select[select_i2] = list(select[select_i1])
				
				select[1][2] = -1
				
				
				request_parse = True
				dirty_editor = True
				
				scroll_to_caret = True
			
			elif e.key in {13,271}: # enter, split line
				
				if select[0] == select[1]:
					cl = caret_line[select[1][0]]
					
					line = lines[ct][select[1][0]]
					
					seg1 = get_segment(line,select[1][1])
					lines[ct][select[1][0]:select[1][0] + 1] = [
						line[:seg1[0]] + [line[seg1[0]][:seg1[1]]],
						["\t" * cl[1] + line[seg1[0]][seg1[1]:]] + line[seg1[0] + 1:],
					]
					
					select[1][0] += 1
					select[1][1] = cl[1]
					
					select[1][2] = -1
					
					select[0] = list(select[1])
					
					
					request_parse = True
					dirty_editor = True
				
				
				scroll_to_caret = True
			
			elif e.key == 9: # tab
				
				if not (pygame.key.get_mods() & pygame.KMOD_SHIFT): # indent selection
					for i in xrange(select[select_i1][0],select[select_i2][0] + 1):
						if isinstance(lines[ct][i][0],list):
							lines[ct][i] = [u""] + lines[ct][i]
						lines[ct][i][0] = "\t" + lines[ct][i][0]
					
					select[0][1] += 1
					select[1][1] += 1
				else: # unindent selection
					for i in xrange(select[select_i1][0],select[select_i2][0] + 1):
						if not isinstance(lines[ct][i][0],list)\
						and lines[ct][i][0][:1] == "\t":
							lines[ct][i][0] = lines[ct][i][0][1:]
							
							if i == select[0][0]:
								select[0][1] -= 1
								select[0][1] = max(0,select[0][1])
							
							if i == select[1][0]:
								select[1][1] -= 1
								select[1][1] = max(0,select[1][1])
				
				select[1][2] = -1
				
				
				request_parse = True
				dirty_editor = True
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 110: # 'n' new tab
				
				actionNew(None)
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 111: # 'o' open tab
				
				actionOpen(None)
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 115: # 's' save
				
				if pygame.key.get_mods() & pygame.KMOD_SHIFT:
					actionSave(None)
				else:
					actionSaveAll(None)
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 119: # 'w' close tab
				
				actionClose(None)
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 122: # 'z' undo
				
				actionUndo(None)
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 121: # 'y' redo
				
				actionRedo(None)
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 120: # 'x' cut
				
				actionCut(None)
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 99: # 'c' copy
				
				actionCopy(None)
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 118: # 'v' paste
				
				actionPaste(None)
			
			elif pygame.key.get_mods() & pygame.KMOD_CTRL\
			and e.key == 97: # 'a' select all
				
				actionSelectAll(None)
			
			elif not (pygame.key.get_mods() & pygame.KMOD_CTRL)\
			and len(e.unicode): # printable, prepend char
				
				if select[0] != select[1]: # remove selection
					seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
					seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
					lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
						lines[ct][select[select_i1][0]][:seg1[0]] + [
							lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
							lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
						] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
					]
					select[select_i2] = list(select[select_i1])
				
				line = lines[ct][select[1][0]]
				
				seg1 = get_segment(line,select[1][1])
				line[seg1[0]] = line[seg1[0]][:seg1[1]] + e.unicode + line[seg1[0]][seg1[1]:]
				
				select[1] = list(select[select_i1])
				select[1][1] += len(e.unicode)
				
				select[1][2] = -1
				
				select[0] = list(select[1])
				
				
				request_parse = True
				dirty_editor = True
				
				scroll_to_caret = True
			
			
			request_layout |= {3}
		
		if e.type == pygame.MOUSEBUTTONDOWN:
			#print "MD",e.button,e.pos
			if e.button == 1:
				mousedown1 = get_button(e.pos,buttons1_flat)
				
				if mousedown1\
				and mousedown1[1][0] == actionAccordion:
					mousedown1[1][0](mousedown1[1][1])
			
			elif e.button == 3:
				mousedown = get_button(e.pos,buttons3_1)
				if mousedown:
					menu_actions = mousedown[1]
					menu_pos1 = e.pos
					menu_mousedown = get_button(e.pos,buttons1_flat)
						
			
			elif e.button in {4,5}:
				if get_button(e.pos,[((xs[1],xs[6],ys[1],ys[2]),)]): # tabs
					
					actionTab((ct + [-1,1][e.button - 4]) % len(titles))
				
				elif get_button(e.pos,[((xs[1],xs[3],ys[5],ys[6]),)]): # editor
					scrolls1[ct][0] += [-100,100][e.button - 4]
					
					
					request_layout |= {3}
				
				elif get_button(e.pos,[((xs[3],xs[4],ys[5],ys[6]),)]): # minimap
					if pygame.key.get_mods() & pygame.KMOD_CTRL:
						h_screen = ys[6] - ys[5]
						h_doc = eh
						
						scroll3 *= [3./2,2./3][e.button - 4]
						scroll3 = min(float(h_doc) / h_screen,scroll3)
						scroll3 = max(1,scroll3)
					else:
						scrolls1[ct][0] += [-10,10][e.button - 4]
					
					
					request_layout |= {3}
				
				elif get_button(e.pos,[((xs[7],xs[8],ys[1],ys[6]),)]): # accordion
					scroll2[0] += [-100,100][e.button - 4]
					scroll2[1] = -1
					
					
					request_layout |= {4}
		
		if e.type == pygame.MOUSEBUTTONDOWN\
		or e.type == pygame.MOUSEMOTION:
			#print "MM",e.pos
			if mousedown1\
			and mousedown1[1][0] == "editor":
				y = e.pos[1] - ys[5] + scrolls1[ct][0]
				cr = get_row(y)
				cr = max(0,cr)
				cr = min(er - 1,cr)
				cr = caret_row[cr]
				
				cc = (e.pos[0] - xs[2] + f3[2][0] // 2) // f3[2][0]
				cc = max(0,cc)
				cc = min(ec,cc)
				
				select[1][0] = cr[0]
				select[1][1] = cr[4] * ec - min(ec,cr[1] * 8) + cr[1] + cc
				
				validate_caret(1)
				
				if e.type == pygame.MOUSEBUTTONDOWN\
				and e.button == 1\
				and not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
					select[0] = list(select[1])
				
				ast_selected1[1] = None
				
				
				request_layout |= {3}
			
			elif mousedown1\
			and mousedown1[1][0] == "minimap":
				h_screen = ys[6] - ys[5]
				h_doc = eh
				h_bar = max(5,int(round(min(1,float(h_screen) / h_doc) * h_screen)))
				h_sp_doc = max(0,h_doc - h_screen)
				h_sp_bar = max(1,h_screen - h_bar)
				
				y_bar = e.pos[1] - ys[5] - h_bar / 2.
				scrolls1[ct][0] = int(round(min(1,float(y_bar) / h_sp_bar) * h_sp_doc))
				
				# TODO CTRL + MU set ast_selected1
				# TODO CTRL + SCROLL y-zoom minimap
				
				
				request_layout |= {3}
			
			if menu_actions:
				menu_pos2 = e.pos
				
				
				request_layout |= {5}
		
		if e.type == pygame.MOUSEMOTION:
			#print "MM",e.pos
			if mousedown1\
			and mousedown1[1][0] == actionTab:
				if rearrange1 < 0:
					rearrange_pos1 = [
						e.pos[0] - e.rel[0] - mousedown1[0][0] - txs[0] // 2,
						e.pos[1] - e.rel[1] - mousedown1[0][2] - tys[0] // 2,
					]
				
				rearrange1 = mousedown1[1][1]
				rearrange2 = mousedown1[1][1]
				
				rearrange_pos2 = [
					e.pos[0] - rearrange_pos1[0],
					e.pos[1] - rearrange_pos1[1],
				]
				rearrange_pos2[0] = max(xs[1] - txs[1] // 2 + txs[0] // 2,rearrange_pos2[0])
				rearrange_pos2[0] = min(xs[6] - txs[1] // 2 - txs[0] // 2 - 1,rearrange_pos2[0])
				rearrange_pos2[1] = max(ys[1] - tys[1] // 2 + tys[0] // 2,rearrange_pos2[1])
				rearrange_pos2[1] = min(ys[2] - tys[1] // 2 - tys[0] // 2 - 1,rearrange_pos2[1])
				pos = [
					rearrange_pos2[0] + txs[1] // 2,
					rearrange_pos2[1] + tys[1] // 2,
				]
				
				mousedown = get_button(pos,buttons1_flat)
				if mousedown\
				and mousedown[1][0] == actionTab:
					rearrange2 = mousedown[1][1]
				
				
				request_layout |= {1}
		
		if e.type == pygame.MOUSEBUTTONUP:
			#print "MU",e.button,e.pos
			if e.button == 1:
				mousedown = get_button(e.pos,buttons1_flat)
				if mousedown1\
				and mousedown\
				and mousedown1[0] == mousedown[0]:
					if mousedown1[1][0] == "editor":
						if False and pygame.key.get_mods() & pygame.KMOD_CTRL: # TODO remove node expansion
							if select_i1:
								select[:] = [select[1],select[0]]
							expand_selection(1)
							select[:] = [select[1],select[0]]
							expand_selection(-1)
							if not select_i1:
								select[:] = [select[1],select[0]]
							
							ast_selected1[1] = None
							
							request_layout |= {3}
					
					if mousedown1[1][0] == "switch":
						state[mousedown1[1][1]] ^= 1
						
						request_layout |= {4}
						
						if isinstance(mousedown1[1][4],types.FunctionType):
							mousedown1[1][4](mousedown1[1][5])
					
					if mousedown1[1][0] == "choose":
						if not mousedown1[1][3]\
						and not state[mousedown1[1][1]][mousedown1[1][2]]: # not selected
							for value in state[mousedown1[1][1]]:
								state[mousedown1[1][1]][value] = 0
						
						state[mousedown1[1][1]][mousedown1[1][2]] ^= 1
						
						request_layout |= {4}
						
						if isinstance(mousedown1[1][4],types.FunctionType):
							mousedown1[1][4](mousedown1[1][5])
					
					if mousedown1[1][0] == "button":
						if isinstance(mousedown1[1][4],types.FunctionType):
							mousedown1[1][4](mousedown1[1][5])
					
					if isinstance(mousedown1[1][0],types.FunctionType)\
					and mousedown1[1][0] != actionAccordion:
						mousedown1[1][0](mousedown1[1][1])
					
					## TODO ...
				
				if rearrange1 >= 0:
					rearrange(rearrange1,rearrange2,files)
					rearrange(rearrange1,rearrange2,titles)
					rearrange(rearrange1,rearrange2,lines)
					rearrange(rearrange1,rearrange2,selects)
					rearrange(rearrange1,rearrange2,scrolls1)
					rearrange(rearrange1,rearrange2,histories)
					rearrange(rearrange1,rearrange2,ctx1)
					
					actionTab(rearrange2)
					
					rearrange1 = -1
					rearrange2 = -1
				
				mousedown1 = None
			
			if e.button in {1,3} and menu_actions:
				
				mousedown = get_button(e.pos,buttons3_2)
				if mousedown\
				and isinstance(mousedown[1][0],types.FunctionType):
					
					if menu_mousedown\
					and menu_mousedown[1][0] == actionTab\
					and isinstance(mousedown[1][1],dict)\
					and mousedown[1][1]["set_ct"]:
						menu_mousedown[1][0](menu_mousedown[1][1])
					
					mousedown[1][0](mousedown[1][1])
			
			if menu_actions:
				menu_actions = None
				
				
				request_layout |= {0}
		
		if e.type == pygame.VIDEORESIZE:
			(width,height) = e.size
			select[1][2] = -1
			
			
			discard = 2 # discard events until validate layout
			dirty_editor = True
			
			request_resize = True
			
			request_layout |= {0}
			
			scroll1_resize = True
		
		if e.type == pygame.USEREVENT:
			for i,anim_ctr in enumerate(anim_ctrs): # continue animation
				if anim_ctrs[i]:
					request_layout |= {i}
				
				anim_ctrs[i] -= 1
				anim_ctrs[i] = max(0,anim_ctrs[i])
			
			if not max(anim_ctrs): # stop animation
				pygame.time.set_timer(pygame.USEREVENT,0)
				timer_set = False
		
		if e.type == pygame.USEREVENT + 1: # save tabs and update accordion
			
			pygame.time.set_timer(pygame.USEREVENT + 1,0) # stop timer
			
			
			request_layout |= {4}
			
			request_save = True


