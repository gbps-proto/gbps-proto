# -*- coding: utf-8 -*-



import array
import cairo
import copy
import cPickle
import json
import math
import pygame
import re
import types
import webbrowser
import wx



#sudo apt-get update; sudo apt-get install -y meld python2 python-cairo python-pygame python-wxgtk3.0; sudo apt-get clean
#while python2 <(bash preprocess.bash gbps_proto.py); do sleep 1; done



titlee = u"gbps-proto rev2108"
welcome = u"--- Welcome to %s " % titlee
print
print
print welcome + u"-" * (80 - len(welcome))
print
print



(width,height) = (800,600) # window dimensions

settings = {}
try:
	with open("settings.json") as f:
		settings = json.load(f)
except: pass

ct = 0 # current tab
#ct = 1
files = [
	# path, file, sha256
	
	## TODO:
	# compare sha256 on: open, tab select, save
	[None,None,None,{"no parse": True,"permanent": True}],
	[None,None,None,{}],
	[None,None,None,{}],
	[None,None,None,{}],
	[None,None,None,{}],
	[None,None,None,{}],
	[None,None,None,{}],
	[None,None,None,{}],
	[None,None,None,{}],
	[None,None,None,{}],
	[None,None,None,{"no parse": True}], # TODO
]
files_init = tuple(id(x) for x in files)
#files_index = set() # TODO open same file only once NO!
titles = [
	u"0. Willkommen",
	u"1. Abfrage",
	u"2. Umschreiben / Generieren",
	u"3. Merkmale",
	u"4. Konfigurieren",
	u"5. Richtlinien",
	u"6. Code-Schemata",
	u"7. Orthogonale Bereiche",
	u"8. Komplexitätsklassen",
	u"9. Datenfluss",
	u"10. Erläuterung des Programmcodes",
]
lines = [
	[
		[[("mark","text formatting",0),[],{"sc": (.1,.3,.6)}],u""],
		[u""],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"==========================================="],
		[u" Herzlich Willkommen und vielen Dank dafür,"],
		[u" dass Sie gbps-proto gestartet haben."],
		[u"==========================================="],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u""],
		[u""],
		[u""],
		[u"- Bitte maximieren Sie das Fenster, um diesen Text besser lesen zu können."],
		[u""],
		[u"- Gbps-proto ist ein Prototyp des graphgestützten Programmiersystems, einem Werkzeugkasten zur Herstellung leistungsfähigerer Software. Die enthaltenen Funktionen geben einen Ausblick auf die Möglichkeiten dieser neuen Technologie. Es handelt sich um kein vollständiges Produkt, insbesondere sind weder Datenbank noch Programmanalyse enthalten. Viele der enthaltenen Werkzeuge funktionieren nur im Rahmen dieser Demonstration."],
		[u""],
		[u"- Bitte verwenden Sie gbps-proto nicht im produktiven Umfeld."],
		[u""],
		[u""],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"==========================================="],
		[u" Einführung in die Benutzeroberfläche:"],
		[u"==========================================="],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u""],
		[u""],
		[u""],
		[u"- Am oberen Rand des Fensters sehen Sie die Tab-Leiste mit allen geöffneten Tabs (Dateien). Mit einem Klick oder durch drehen am Mausrad wechseln Sie in einen anderen Tab. Um die Reihenfolge zu ändern, ziehen Sie einen Tab an eine andere Position. Halten Sie die rechte Maustaste, um ein Kontextmenü zu sehen. Ziehen Sie mit gehaltener Maustaste und lassen Sie auf dem gewünschten Eintrag los."],
		[u""],
		[u"- Rechts sehen Sie die Werkzeug-Schnellwahl mit Kurz-Infos in den geschlossenen Reitern. Mit einem Klick auf einen Reiter öffnen Sie diesen. Nutzen Sie das Mausrad um zu scrollen. Halten Sie die rechte Maustaste, um alle Werkzeuge in einem Kontextmenü zu sehen. Ziehen Sie mit gehaltener Maustaste und lassen Sie auf dem gewünschten Eintrag los. Wählen Sie \"X. Alle Reiter schließen\", um alle anderen Reiter zu schließen und so alle Kurz-Infos im Blick zu haben."],
		[u""],
		[u"- Der übrige Bereich, der sich von links unten über die Fenstermitte erstreckt, ist der Editor. Dort wird der Inhalt des gewählten Tabs gezeigt. Bei Tabs mit Programmcode befindet sich am rechten Rand des Editors eine verkleinerte Darstellung des Tabinhalts (Minimap). Nutzen Sie die Minimap wie eine Scroll-Leiste. Halten Sie die \"STRG\"-Taste für ein modifiziertes Verhalten bei Mausklicks und drehen des Mausrads. Verkleinern Sie dazu ggf. das Fenster. Der Editor unterstützt Copy-and-Paste sowie alle gängigen Maus- bzw. Tastaturbefehle."],
		[u""],
		[u"- Im Reiter \"0. Dateien / Tabs\" in der Werkzeug-Schnellwahl finden Sie Möglichkeiten für das Öffnen und Speichern von Dateien. Der Zustand von gbps-proto sowie alle geöffneten Tabs werden beim Beenden des Programms intern gespeichert und beim nächsten Programmstart wiederhergestellt. Nutzen Sie die Funktionen \"Tab speichern\", \"Tab speichern unter\" und \"Alle Tabs speichern\", um geöffnete Tabs in das Dateisystem der Festplatte zu schreiben. Außerhalb dieses Programms können jedoch keine Änderungen rückgängig gemacht werden."],
		[u""],
		[u""],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"==========================================="],
		[u" Einführung in die einzelnen Werkzeuge:"],
		[u"==========================================="],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u""],
		[u""],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"1. Abfrage.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"1. Abfrage\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Abfragen eine Auswahl. Die Ergebnisse werden im Editor hervorgehoben und in einer Tabelle aufgelistet. Mit einem Klick auf die Tabelle springen Sie im Editor zum entsprechenden Eintrag."],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"2. Umschreiben / Generieren.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"2. Umschreiben / Generieren\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Abfragen eine Auswahl. Die zur Auswahl passenden Stellen werden im Editor umgeschrieben. In dieser Demonstration können umgeschriebene Bereiche nicht bearbeitet werden. Deaktivieren Sie das Werkzeug, um den ursprünglichen Programmtext zu bearbeiten. Die umgeschriebenen Bereiche werden im Editor hervorgehoben und in Tabellen aufgelistet. Mit einem Klick auf eine Tabelle springen Sie im Editor zum entsprechenden Eintrag."],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"3. Merkmale.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"3. Merkmale\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Der Programmcode im Editor wird auf Vorkommen von Schleifen, Arithmetik und Datenzugriffen hin untersucht. Die Ergebnisse werden in Tabellen aufgelistet. In den Tabellen werden auch die Eigenschaften der gefundenen Vorkommen verglichen. Mit einem Klick auf eine Tabelle springen Sie im Editor zum entsprechenden Eintrag."],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"4. Konfigurieren.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"4. Konfigurieren\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Konfigurationen eine Auswahl. Die zur Auswahl passenden Stellen werden intern umgeschrieben und im Editor hervorgehoben. Es wird allerdings weiterhin der ursprüngliche Programmtext gezeigt. Die resultierende Umsetzung sehen Sie mit \"Resultierenden Programmtext einblenden\". Bei \"Tab speichern\", \"Tab speichern unter\" und \"Alle Tabs speichern\" wird ebenfalls der resultierende Programmtext verwendet."],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"5. Richtlinien.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"5. Richtlinien\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Richtlinien eine Auswahl. Der Programmcode im Editor wird auf Konformität zu den ausgewählten Richtlinien hin untersucht. Nicht-konforme Stellen werden im Editor deutlich hervorgehoben. In Kreisdiagrammen wird das Maß der Konformität des Programmcodes zu den ausgewählten Richtlinien dargestellt."],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"6. Code-Schemata.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"6. Code-Schemata\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Schemata eine Auswahl. Die zur Auswahl passenden Stellen werden im Editor vereinfacht dargestellt. Nicht-konforme Stellen werden im Editor deutlich hervorgehoben. In Kreisdiagrammen wird das Maß der Konformität des Programmcodes zu den ausgewählten Schemata dargestellt."],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"7. Orthogonale Bereiche.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"7. Orthogonale Bereiche\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Markieren Sie einen Bereich im Editor und klicken Sie auf \"Auswahl zu Gruppe ... hinzufügen\". Die gruppierten Bereiche werden in einer Tabelle katalogisiert. Mit \"Platzhalter für Gruppe ... aktivieren\" werden sie im Editor vereinfacht dargestellt."],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"8. Komplexitätsklassen.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"8. Komplexitätsklassen\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Der Programmcode im Editor wird auf Zeitkomplexität hin untersucht. Setzen Sie den Tastaturcursor (Caret) im Editor auf eine Position innerhalb einer Schleife, um die Ergebnisse zu sehen."],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"9. Datenfluss.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"9. Datenfluss\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Der Programmcode im Editor wird auf Datenfluss hin untersucht. Setzen Sie den Tastaturcursor (Caret) im Editor auf eine Variable, um die Ergebnisse zu sehen."],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"10. Erläuterung des Programmcodes.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"10. Erläuterung des Programmcodes\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Der Programmcode im Editor wird auf Zweck und Funktionsweise hin untersucht. Setzen Sie den Tastaturcursor (Caret) im Editor auf eine Position innerhalb einer Funktion, um die Ergebnisse zu sehen."],
		[u""],
		[u""],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"- Nochmals vielen Dank dafür, dass Sie sich für gbps-proto interessieren. Im Reiter \"12. Über dieses Programm\" in der Werkzeug-Schnellwahl finden Sie einen Link zu weiterem Informationsmaterial sowie eine Möglichkeit, mir eine E-Mail zu schreiben."],
		[u""],
		[u""],
		[u""],
	],
	[
		[u""],
		[u""],
		[u"# Division durch eine Variable"],
		[u""],
		[u""],
		[u"quotient = dividend / 10"],
		[u""],
		[u"quotient = dividend / divisor"],
		[u""],
		[u"rest = dividend % (10 * divisor)"],
		[u""],
		[u""],
		[u"# Datenzugriff mit einer Variablen als Schlüssel"],
		[u""],
		[u""],
		[u"eintrag = datenstruktur[\"graphbased\"]"],
		[u""],
		[u"eintrag = datenstruktur[schlüssel]"],
		[u""],
		[u"eintrag = datenstruktur[10 * schlüssel]"],
		[u""],
		[u""],
		[u"# Vergleich mit einem Literal"],
		[u""],
		[u""],
		[u"if variable1 == variable2:"],
		[u"\tpass"],
		[u""],
		[u"if variable1 > 10:"],
		[u"\tpass"],
		[u""],
		[u"if variable1 in {\"graph\",\"based\"}:"],
		[u"\tpass"],
		[u""],
		[u""],
		[u"# Funktionsaufruf innerhalb einer Schleife"],
		[u""],
		[u""],
		[u"rückgabewert = funktion(parameter)"],
		[u""],
		[u"for (zähler,eintrag) in enumerate(liste):"],
		[u"\tfunktion(zähler)"],
		[u""],
		[u"while True:"],
		[u"\tif funktion(parameter): pass"],
		[u""],
		[u""],
	],
	[
		[u""],
		[u""],
		[u"# Einfaches Beispiel"],
		[u""],
		[u""],
		[u"xxx"], # 
		[u""],
		[u""],
		[u"# Code mit orthogonalen Bestandteilen erzeugen"],
		[u""],
		[u""],
		[u"code erzeugen {"], # switch-case mit orthogonalen Bestandteilen
		[u"\t(a,z),"],
		[u"\t(a,b,x,y),"],
		[u"\t(c,z),"],
		[u"\t(x,y),"],
		[u"}"],
		[u""],
		[u""],
		[u"# Domänenspezifische Befehle umsetzen"],
		[u""],
		[u""],
		[u"physikalische_größen {"],
		[u"\tgeschwindigkeit = 10 m / 2 s"], # geschwindigkeit = größe(wert = 10,einheit = "meter").div(größe(wert = 2,einheit = "sekunden"))
		[u"}"],
		[u""],
		#[u"geldbeträge {"],
		#[u"\tkontostand += 100.00 Euro"], # kontostand = kontostand.add(betrag = 10000,währung = "Euro")
		#[u"}"],
		[u""],
		[u"geschäftslogik {"],
		[u"\tif kurs >= mindestkurs:"], # geschäftslogik.add(regel(bedingung = lambda: kurs >= mindestkurs,signale = (kurs,mindestkurs),aktion = aktion))
		[u"\t\tverkaufen()"], # def aktion(): verkaufen()
		[u"}"],
		[u""],
		[u""],
	],
	[
		[u""],
		[u""], # immer die äußerste Schleife
		[u"# Schleifen"], # Iterierung einer Liste, Enthält Funktionsaufruf, Keine polynomielle Laufzeit
		[u""],
		[u""],
		[u""],
		[u""], # immer die äußerste Operation
		[u"# Arithmetik"], # Enthält Konstante, Enthält Addition, Enthält Multiplikation
		[u""],
		[u""],
		[u""],
		[u""], # pro [; =,+=,++  # x[y].z = 123 gilt als schreiben
		[u"# Datenzugriffe"], # Datenstruktur lesen, Datenstruktur schreiben, Bedingte Ausführung
		[u""],
		[u""],
		[u""],
		[u""],
	],
	[
		[u""],
		[u""],
		[u"# Einfaches Beispiel"],
		[u""],
		[u""],
		[u"xxx"], # 
		[u""],
		[u""],
		[u"# Fehlertoleranz für Datenzugriffe einfügen"], # spezifischer identifier, nur dict
		[u""],
		[u""],
		[u"eintrag = datenstruktur[\"graphbased\"]"],
		[u""],
		[u"eintrag = datenstruktur[-10]"],
		[u""],
		[u"eintrag = datenstruktur[index][schlüssel].eigenschaft"],
		[u""],
		[u""],
		[u"# Unbehandelte Fälle aufzeichnen"], # betrifft nur ifs mit elif und ohne else
		[u""],
		[u""],
		[u" "],
		[u""],
		[u""],
	],
	[
		[u""],
		[u""],
		[u"# Einfaches Beispiel"],
		[u""],
		[u""],
		[u""],
		[u""],
		[u"# Laufzeit explizit begrenzen"],
		[u""],
		[u""],
		[u""],
		[u""],
		[u"# Daten konsistent halten"],
		[u""],
		[u""],
		[u""],
		[u""],
	],
	[
		[u""],
		[u""],
		[u"# Einfaches Beispiel"],
		[u""],
		[u""],
		[u""],
		[u""],
		[u"# Entwurfsmuster"],
		[u""],
		[u""],
		[u""],
		[u""],
		[u"# Duplikate mit Variationen"],
		[u""],
		[u""],
		[u""],
		[u""],
	],
	[
		[u""],
		[u""],
		[u""], # einzelnes Beispiel
		[u""], # Switch-Case mit orthogonalen Inhalten
		[u""],
		[u""],
		[u""],
	],
	[
		[u""],
		[u""],
		[u"# Einfaches Beispiel"],
		[u""],
		[u""],
		[u""],
		[u""],
		[u"# Polynomielle Laufzeit"],
		[u""],
		[u""],
		[u""],
		[u""],
		[u"# Rekursion und exponentielle Laufzeit"],
		[u""],
		[u""],
		[u""],
		[u""],
	],
	[
		[u""],
		[u""],
		[u""], # einzelnes Beispiel
		[u""], # Daten-Konsistenz
		[u""],
		[u""],
		[u""],
	],
	[ ##### Ereignisbehandlung, Graphsuche, Zustandsautomat
	# Idiome, mehrfache Verwendung von z.B. if
	# Eigenschaften und Nuancen
		[u"for (key,val) in enumerate(list):"],
		[u"\t",[("mark","draw anchor",1,"a1"),[]],u"if val == \"string\":"],
		[u"\t\tlist[key] += [(123,)]",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,1,.5),"stroke": (1,1,.75)}]],
		[u""],
		[u"a + b * c + d"],
		[u""],
		[u""],
		[u""],
		[u"for (key,val) in enumerate(list):"],
		[u"\tif ",[("mark","card static",0),[],{"mode": "paragraph","content": u"Dies ist ein Fließtext. Dies ist ein Fließtext. Dies ist ein Fließtext."}],u"val ",[("mark","card static",0),[],{"mode": "typewriter","content": [
			[u"for (key,val) in enumerate(list):"],
			[u"\tif val == \"string\":"],
			[u"\t\tlist[key] += [(123,)]"],
		]}],u"== ",[("mark","draw anchor",1,"a1"),[]],u"\"string\"",[("mark","card static",0),[],{"mode": "paragraph","content": u"Dies ist auch ein Fließtext. Dies ist auch ein Fließtext. Dies ist auch ein Fließtext."}],u":"],
		[u"\t\tlist[key]",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,.5,0),"stroke": (1,0,0)}],u" += [(123,)]"],
		[u""],
		[u"a + b * c + d"],
		[u""],
		[u""],
		[u""],
		[u"\t\tctx.set_source_rgb(0,00,00.,.00,00.00)"],
		[u"\t\tctx.set_source_rgb(uu0,uu00,uu00.,uu.00,uu00.00)"],
		[u"\t\tctx.set_source_rgb(.,..,...,00..,..00,00..00)"],
		[u"\t\tctx.select_font_face(\"Monospace\")"],
		[u"\t\tctx.select_font_face(uu\"Monospace\")"],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],[("mark","text formatting",0),[],{"sc": (.4,.2,.2)}],u"\t\tfo = ctx.get_font_options22() keyword",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}]],
		[[("mark","text formatting",0),[],{"sc": (.1,.4,.4)}],u"\t\tfo = ctx.get_font_options22() call"],
		[[("mark","text formatting",0),[],{"sc": (.1,.25,.1)}],u"\t\tfo = ctx.get_font_options22() identifier"],
		[[("mark","text formatting",0),[],{"sc": (.5,.4,.5)}],u"\t\tfo = ctx.get_font_options22() literal"],
		[[("mark","text formatting",0),[],{"sc": (.1,.3,.6)}],u"\t\tfo = ctx.get_font_options22() comment"],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],[("mark","text formatting",0),[],{"sc": (.4,.2,.2)}],u"\t\t",[("mark","draw anchor",1,"a1"),[]],u"a11 fo = ctx.get_font_options22() keyword",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}]],
		[[("mark","text formatting",0),[],{"sc": (.1,.4,.4)}],u"\t\tfo = ctx.get_font_options22() call"],
		[[("mark","text formatting",0),[],{"sc": (.1,.25,.1)}],u"\t\tfo = ctx.get_font_options22() identifier",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,1,.5),"stroke": (1,1,.75)}]],
		[[("mark","text formatting",0),[],{"sc": (.5,.4,.5)}],u"\t\t",[("mark","draw anchor",1,"a1"),[]],u"fo = ctx.get_font_options22() literal"],
		[[("mark","text formatting",0),[],{"sc": (.1,.3,.6)}],u"\t\tfo = ctx.get_font_options22() comment a12",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,1,.5),"stroke": (1,1,.75)}]],
		[u""],
		[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],[("mark","text formatting",0),[],{"sc": (.4,.2,.2)}],u"\t\t",[("mark","draw anchor",1,"a1"),[]],u"a21 fo = ctx.get_font_options22() keyword",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}]],
		[[("mark","text formatting",0),[],{"sc": (.1,.4,.4)}],u"\t\tfo = ctx.get_font_options22() call"],
		[[("mark","text formatting",0),[],{"sc": (.1,.25,.1)}],u"\t\tfo = ctx.get_font_options22() identifier",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,.5,0),"stroke": (1,0,0)}],[("mark","text formatting",0),[],{"sc": (.5,.4,.5)}],u"",[("mark","draw anchor",1,"a1"),[]],u"fo = ctx.get_font_options22() literal"],
		[[("mark","text formatting",0),[],{"sc": (.1,.3,.6)}],u"\t\tfo = ctx.get_font_options22() comment a22",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,.5,0),"stroke": (1,0,0)}]],
		[u""],
		[u""],
		[u""],
		[u"ctx.set_source_rgb(0,0,0)"],
		[u"ctx.select_font_face(\"Monospace\")"],
		[u"fo = ctx.get_font_options()"],
		[u"fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)"],
		[u"ctx.set_font_options(fo)đſðđł€ŋŧłł³¹¼¼½{¼¬”«"],
		[u"ctx.set_source_rgb(0,0,0)"],
		[u"ctx.select_font_face(\"Monospace\")"],
		[u"fo = ctx.get_font_options()"],
		[u"fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)"],
		[u"ctx.set_font_options(fo)đſðđł€ŋŧłł³¹¼¼½{¼¬”«"],
	],
]

lines[-2] = lines[-1] #TODO

selects = [[[0,0,0],[0,0,0]] for i in xrange(11)]
scrolls1 = [[0,-1,0] for i in xrange(11)]
histories = [None] * 11 #TODO

ctx1 = [None] * 11  #  TODO access only via parse_query

try:
#if True:
	with open("tabs.pkl","rb") as f:
		
		#print "loading..."
		
		pkl = cPickle.load(f)
		
		for n,filee in reversed(tuple(enumerate(files))):
			if "permanent" in filee[3] and filee[3]["permanent"]:
				pkl["files"] = [files[n]] + pkl["files"]
				pkl["titles"] = [titles[n]] + pkl["titles"]
				pkl["lines"] = [lines[n]] + pkl["lines"]
				pkl["selects"] = [selects[n]] + pkl["selects"]
				pkl["scrolls1"] = [scrolls1[n]] + pkl["scrolls1"]
				pkl["histories"] = [histories[n]] + pkl["histories"]
		
		ct = pkl["ct"]
		files = pkl["files"]
		titles = pkl["titles"]
		lines = pkl["lines"]
		selects = pkl["selects"]
		scrolls1 = pkl["scrolls1"]
		histories = pkl["histories"]
		ctx1 = [None] * len(titles)
		
		#print "loaded"
except: pass

def save_tabs():
	try:
	#if True:
		with open("tabs.pkl","wb") as f:
			
			#print "saving..."
			
			pkl = {}
			pkl["files"] = list(files)
			pkl["titles"] = list(titles)
			pkl["lines"] = list(lines)
			pkl["selects"] = list(selects)
			pkl["scrolls1"] = list(scrolls1)
			pkl["histories"] = list(histories)
			
			sel = lines[ct]
			
			i = 0
			for n,filee in enumerate(files):
				if "permanent" in filee[3] and filee[3]["permanent"]:
					
					del pkl["files"][n - i]
					del pkl["titles"][n - i]
					del pkl["lines"][n - i]
					del pkl["selects"][n - i]
					del pkl["scrolls1"][n - i]
					del pkl["histories"][n - i]
					
					if ct == n:
						pkl["ct"] = files_init.index(id(filee))
					
					i += 1
			
			for n,filee in enumerate(pkl["files"]):
				if pkl["lines"][n] is sel:
					pkl["ct"] = n + i
			
			cPickle.dump(pkl,f)
			
			#print "saved"
	
	except: pass

closed_tabs = [] # TODO renamed
closed_tabs_index = {}

scroll1_resize = True
ast_selected1 = [None,None] # lowest node containing selection
ast_selected2 = [] # nodes in ast_selected1, overlapping selection


ca = 13 # current accordion
tools = [
	u"0. Dateien / Tabs",
	u"1. Abfrage",
	u"2. Umschreiben / Generieren",
	u"3. Merkmale",
	u"4. Konfigurieren",
	u"5. Richtlinien",
	u"6. Code-Schemata",
	u"7. Orthogonale Bereiche",
	u"8. Komplexitätsklassen",
	u"9. Datenfluss",
	u"10. Erläuterung des Programmcodes",
	u"11. Knoten-Eigenschaften",
	u"12. Über dieses Programm",
	u"X. Alle Reiter schließen",
]

if "advanced mode" in settings and settings["advanced mode"]:
	tools += [
		u"A. Interaktive Ausführung",
		u"A. Datei-Verlauf",
		u"A. Knoten-Navigator",
		u"A. Knoten-Liste",
	]

tool_ctrs = [0] * 18
scroll2 = [0,-1]
scroll3 = 1.


mousedown1 = None
buttons1 = [[] for i in xrange(5)]
buttons3_1 = []
buttons3_2 = []

rearrange1 = -1
rearrange2 = -1
rearrange_pos1 = [0,0]
rearrange_pos2 = [0,0]

menu_actions = None
menu_pos1 = [0,0]
menu_pos2 = [0,0]
menu_mousedown = None

anim_ctrs = [0,0,0,0,3]
anim_tick = -1

state = {}

try:
	with open("state.pkl","rb") as f:
		pkl = cPickle.load(f)
		ca = pkl["ca"]
		tool_ctrs = pkl["tool_ctrs"]
		scroll2 = pkl["scroll2"]
		state = pkl["state"]
		#width = pkl["width"]
		#height = pkl["height"]
		closed_tabs = pkl["closed_tabs"]
except: pass

def save_state():
	try:
		with open("state.pkl","wb") as f:
			pkl = {}
			pkl["ca"] = ca
			pkl["tool_ctrs"] = tool_ctrs
			pkl["scroll2"] = scroll2
			pkl["state"] = state
			#pkl["width"] = width
			#pkl["height"] = height
			pkl["closed_tabs"] = closed_tabs
			cPickle.dump(pkl,f)
	except: pass

tab_history = [] # store tab history permanently # TODO
tab_history_index = [] # TODO



execfile("layout.py")
execfile("parse.py")
execfile("parse_query.py")



tpls = {
	"rewrite 0": {"text": u"a\n\tb\n\t\tc"},
	"rewrite 1": {"text": u"a\n\tb\n\t\tc"},
	"rewrite 2-1": {"text": u""},
	"rewrite 2-2": {"text": u""},
	"rewrite 2-3": {"text": u"geschäftslogik.add(regel(bedingung = lambda: a,signale = (b),aktion = aktion))"},
	"rewrite 2-4": {"text": u"def aktion(): c"},
	"config 0-1": {"text": u"((c in b if isinstance(b,dict) or isinstance(b,set) else len(b) > (c if c >= 0 else -c - 1)) and a)"},
	"config 0-2": {"text": u"hasattr(b,c) and a"},
	"config 1-1": {"text": u"(a) >= (b) - tolerance and (a) <= (b) + tolerance"},
	"config 1-2": {"text": u"(a) < (b) - tolerance or (a) > (b) + tolerance"},
	"config 1-3": {"text": u"(a) >= (b) - tolerance"},
	"config 1-4": {"text": u"(a) <= (b) + tolerance"},
	"config 2": {"text": u"if interrupt: break"},
}
for x in tpls:
	parse(tpls[x])



pygame.init()
pygame.mixer.quit()

#cursor1 = pygame.cursors.load_xbm("cursor31.xbm","cursor12.xbm")
#pygame.mouse.set_cursor(*cursor1)

surf_tile1 = cairo.ImageSurface.create_from_png("tile1.png")
surf_tile2 = cairo.ImageSurface.create_from_png("tile2.png")

resize()

pygame.scrap.init()
pygame.display.set_caption(titlee)
pygame.key.set_repeat(200,50)


wx_app = wx.App()


#print wx.MessageBox(u"Die Datei wurde von einem anderen Programm verändert. Möchten Sie die Datei neu laden?\n\nDadurch geht der aktuelle Tabinhalt verloren.",u"Datei neu laden",wx.YES_NO | wx.NO_DEFAULT | wx.ICON_WARNING)
#print wx.MessageBox(u"Die Datei wurde von einem anderen Programm verändert. Möchten Sie die Datei überschreiben?\n\nDadurch gehen die Änderungen des anderen Programms verloren.",u"Datei überschreiben",wx.YES_NO | wx.NO_DEFAULT | wx.ICON_WARNING)
##print wx.MessageBox(u"Die Datei zum Tab existiert nicht mehr. Möchten Sie die Datei an einem anderen Ort speichern?",u"Datei an einem anderen Ort speichern",wx.YES_NO | wx.NO_DEFAULT | wx.ICON_WARNING)
#print wx.MessageBox(u"Die neu geöffnete Datei (B) ist identisch zur Datei (A), zuletzt bearbeitet am XX.XX.XXXX um XX:XX. Möchten Sie den Datei-Verlauf von (A) nach (B) verschieben?\n\n(A) /////(Datei existiert nicht mehr)\n\n(B) //////",u"Datei-Verlauf verschieben",wx.YES_NO | wx.YES_DEFAULT | wx.ICON_WARNING)
wx.Frame(None).Destroy()


execfile("evtloop.py")


