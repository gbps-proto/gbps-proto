


	# text layout
	
	
	def text_paragraph((x,y,w),color,(ff,fs,fe,fw),textt,action = None,arg = None,usepadding = True,rightalign = False): # box, color, font, text, action, arg, usepadding, rightalign
		global buttons1
		
		y_prev = y
		
		textt = textt.split(" ")
		
		i = 0
		while i < len(textt):
			if i: y += f1[2][1]
			
			j = len(textt)
			while j - 1 > i:
				if text_extents((ff,fs,fe,fw)," ".join(textt[i:j])) <= w:
					break
				j -= 1
			
			align = w - text_extents((ff,fs,fe,fw)," ".join(textt[i:j])) if rightalign else 0
			
			text(x + align,y,color,(ff,fs,fe,fw)," ".join(textt[i:j]))
			
			i = j
		
		y += f1[2][2]
		
		if action:
			padding = blank(1) // 2 if usepadding else 0
			buttons1[4] += [((x,x + w,y_prev - padding,y + padding),("button",None,None,None,action,arg))]
		
		return y - y_prev
	
	
	def text_center((x,y,w),color,(ff,fs,fe,fw),textt): # box, color, font, text
		
		extent = text_extents((ff,fs,fe,fw),textt)
		
		if extent > w:
			text_fit(x,y,w,color,(ff,fs,fe,fw),textt)
		else:
			text(x + (w - extent) // 2,y,color,(ff,fs,fe,fw),textt)
		
		return f1[2][2]
	
	
	# simple objects
	
	
	def hline((x,y,w)): # box
		frect(x,x + w,y,y + 1,gr1)
		return 1
	
	
	def switch((x,y,w),name,label,action = None,arg = None,enabled = True): # box, name, label, action, arg, enabled
		global buttons1,state
		
		y_prev = y
		x_prev = x
		w_prev = w
		x += 10
		w -= 10
		
		if state[name]:
			frect(x + 10,x + 20,y,y + 9,bl1 if enabled else gr1)
			srect(x,x + 20,y,y + 9,bl1 if enabled else gr1)
			
			x += 40
			w -= 40
			y += text_paragraph((x,y,w),blk if enabled else gr1,f2,label + (u" (An)" if enabled else u" (Inaktiv)"))
		else:
			frect(x,x + 10,y,y + 9,gr1)
			srect(x,x + 20,y,y + 9,gr1)
			
			x += 40
			w -= 40
			y += text_paragraph((x,y,w),blk if enabled else gr1,f1,label + (u" (Aus)" if enabled else u" (Inaktiv)"))
		
		if enabled\
		and not disable_draw_prs:
			buttons1[4] += [((x_prev,x_prev + w_prev,y_prev - blank(1) // 2,y + blank(1) // 2),("switch",name,None,None,action,arg))]
		
		return y - y_prev
	
	
	def choose((x,y,w),name,value,label,ismulti = True,action = None,arg = None,enabled = True): # box, name, value, label, is-multi, action, arg, enabled
		global buttons1,state
		
		y_prev = y
		x_prev = x
		w_prev = w
		x += 10
		w -= 10
		
		if state[name][value]:
			frect(x + 10,x + 20,y,y + 9,bl1 if enabled else gr1)
			
			x += 40
			w -= 40
			y += text_paragraph((x,y,w),blk if enabled else gr1,f2,label + (u" (Ausgewählt)" if enabled else u" (Inaktiv)"))
		else:
			srect(x + 10,x + 20,y,y + 9,gr1)
			
			x += 40
			w -= 40
			y += text_paragraph((x,y,w),blk if enabled else gr1,f1,label + (u" (Nicht ausgewählt)" if enabled else u" (Inaktiv)"))
		
		if enabled\
		and not disable_draw_prs:
			buttons1[4] += [((x_prev,x_prev + w_prev,y_prev - blank(1) // 2,y + blank(1) // 2),("choose",name,value,ismulti,action,arg))]
		
		return y - y_prev
	
	
	def button((x,y,w),label,action,arg = None,size = 0,enabled = True): # box, label, action, arg, size, enabled
		global buttons1
		
		if not size:
			size = w - 20
		
		m = x + w // 2
		
		if enabled:
			frect(m - size // 2,m + size // 2,y,y + tys[1],gr1)
		srect(m - size // 2,m + size // 2,y,y + tys[1],blk if enabled else gr1)
		text_center((m - size // 2 + 10,y + (tys[1] - f1[2][2] + 1) // 2,size - 20),blk if enabled else gr1,f1,label)
		
		if enabled\
		and not disable_draw_prs:
			buttons1[4] += [((m - size // 2,m + size // 2,y,y + tys[1]),("button",None,None,None,action,arg))]
		
		return tys[1]
	
	
	def hbar_chart((x,y,w),(n,d,v)): # box, numerator, denominator, default-value
		if d > 0:
			v = float(n) / d
		if v != 0:
			v = max(.25 - math.atan(2) / 2 / math.pi,v)
		if v != 1:
			v = min(.75 + math.atan(2) / 2 / math.pi,v)
		
		if not disable_draw_prs:
			if v == 1:
				frect(x,x + w - 20,y + 6,y + 8,grn)
				
				ctx.set_line_width(2)
				ctx.set_source_rgb(*grn)
				ctx.move_to(x + w - 10,y + 3)
				ctx.line_to(x + w - 6,y + 7)
				ctx.line_to(x + w,y + 1)
				ctx.stroke()
			else:
				m = round(x + (w - 20) * v)
				if v != 0:
					m += 1
					frect(x,m - 2,y + 6,y + 8,grn)
				frect(m,x + w - 20,y,y + 8,red)
				frect(m + 1,x + w - 20 - 1,y + 1,y + 7,org)
				
				ctx.set_line_width(2)
				ctx.set_source_rgb(*red)
				ctx.move_to(x + w - 9,y)
				ctx.line_to(x + w - 1,y + 8)
				ctx.move_to(x + w - 1,y)
				ctx.line_to(x + w - 9,y + 8)
				ctx.stroke()
				
				ctx.set_line_width(1)
				ctx.set_source_rgb(*org)
				ctx.move_to(x + w - 9,y)
				ctx.line_to(x + w - 1,y + 8)
				ctx.move_to(x + w - 1,y)
				ctx.line_to(x + w - 9,y + 8)
				ctx.stroke()
		
		return 8
	
	
	# page layout
	
	
	def indent((x,y,w),liness,linetobottom = False,action = None): # box, lines, line-to-bottom, action
		y_prev = y
		
		indent = 0
		y_start = {}
		y_end = {}
		for i,line in enumerate(liness):
			if i: y += blank(1)
			
			if line[0] >= 0 and line[1] and line[0] not in y_start:
				indent = line[0]
				y_start[line[0]] = y
			
			if line[0] >= 0:
				y += text_paragraph((x + 20 * line[0],y,w - 20 * line[0]),blk,line[2],line[3],action = action,arg = line)
			
			if line[0] >= 0 and line[1]:
				y_end[line[0]] = y
			
			if i == len(liness) - 1:
				if linetobottom:
					for j in xrange(line[0] + 1):
						y_end[j] = y
				
				line = list(line)
				line[0] = 0
			
			if line[0] >= 0 and line[0] < indent:
				#print line[0], indent, y_start, y_end
				for j in xrange(line[0] + 1,indent + 1):
					
					if j in y_start:
						frect(x + 20 * (j - 1) + 4.5,x + 20 * (j - 1) + 5.5,y_start[j],y_end[j],blk)
					
					if j in y_start: del y_start[j]
				
				indent = line[0]
		
		return y - y_prev
	
	
	def column((x,y,w),cols,conts): # box, cols (-1 is fill), content
		fixedwidth = sum(col for col in cols if col >= 0)
		remaining = w - fixedwidth
		nfills = sum(1 for col in cols if col < 0)
		fillwidth = remaining // nfills if nfills else 0
		lastfill = (-i - 1 for i,col in enumerate(reversed(cols)) if col < 0)
		
		cols = [(col if col >= 0 else fillwidth) for col in cols]
		
		try: cols[next(lastfill)] = remaining - fillwidth * (nfills - 1)
		except: pass
		
		#print w, fixedwidth, remaining, nfills, fillwidth, remaining - fillwidth * (nfills - 1)
		#print cols
		
		h = [0] * len(conts)
		for i,cont in enumerate(conts):
			if cont:
				h[i] = cont((x,y,cols[i]),i)
			x += cols[i]
		
		#print h
		
		return max(h)
	
	
	def table((x,y,w),cols,contss): # box, cols (-1 is fill), content
		y_prev = y
		x_prev = x
		
		fixedwidth = sum(col for col in cols if col >= 0)
		remaining = w - fixedwidth
		nfills = sum(1 for col in cols if col < 0)
		fillwidth = remaining // nfills if nfills else 0
		lastfill = (-i - 1 for i,col in enumerate(reversed(cols)) if col < 0)
		
		cols = [(col if col >= 0 else fillwidth) for col in cols]
		
		try: cols[next(lastfill)] = remaining - fillwidth * (nfills - 1)
		except: pass
		
		#print w, fixedwidth, remaining, nfills, fillwidth, remaining - fillwidth * (nfills - 1)
		#print cols
		
		for j,conts in enumerate(contss):
			x = x_prev
			
			h = [0] * len(conts)
			for i,cont in enumerate(conts):
				if cont:
					h[i] = cont((x,y,cols[i]),i,j)
				x += cols[i]
			
			y += max(h)
		
		return y - y_prev
	
	
	# complex objects
	
	
	def pie_chart((x,y,w),label,(n,d,v)): # box, label, numerator, denominator, default-value
		y_prev = y
		
		if d > 0:
			v = float(n) / d
		if v != 0:
			v = max(.25 - math.atan(2) / 2 / math.pi,v)
		if v != 1:
			v = min(.75 + math.atan(2) / 2 / math.pi,v)
		
		y += text_paragraph((x,y,w),blk,f1,label)
		y += blank(1)
		
		
		def cont1((x,y,w),i):
			if not disable_draw_prs:
				
				if v == 1:
					ctx.set_line_width(2)
					ctx.set_source_rgb(*grn)
					ctx.arc(x + 30,y + 24,23,0,2 * math.pi)
					ctx.stroke()
					
					ctx.set_line_width(2)
					ctx.set_source_rgb(*grn)
					ctx.move_to(x + 18,y + 22)
					ctx.line_to(x + 28,y + 32)
					ctx.line_to(x + 44,y + 16)
					ctx.stroke()
				
				else:
					ctx.set_line_width(8)
					ctx.set_source_rgb(*red)
					ctx.arc(x + 30,y + 30,26,3./2 * math.pi,(3./2 + 2 * (1 - v)) * math.pi)
					ctx.stroke()
					
					ctx.set_line_width(6)
					ctx.set_source_rgb(*org)
					ctx.arc(x + 30,y + 30,26,3./2 * math.pi,(3./2 + 2 * (1 - v)) * math.pi)
					ctx.stroke()
					
					if v != 0:
						
						verts = [
							[22 * math.cos(3./2 * math.pi),22 * math.sin(3./2 * math.pi)],
							[22 * math.cos((3./2 + 2 * (1 - v)) * math.pi),22 * math.sin((3./2 + 2 * (1 - v)) * math.pi)],
							[30 * math.cos(3./2 * math.pi),30 * math.sin(3./2 * math.pi)],
							[30 * math.cos((3./2 + 2 * (1 - v)) * math.pi),30 * math.sin((3./2 + 2 * (1 - v)) * math.pi)],
						]
						
						ctx.set_line_width(4)
						ctx.set_source_rgb(*red)
						ctx.move_to(x + 30 + verts[0][0],y + 30 + verts[0][1])
						ctx.line_to(x + 30 + verts[2][0],y + 30 + verts[2][1])
						ctx.move_to(x + 30 + verts[1][0],y + 30 + verts[1][1])
						ctx.line_to(x + 30 + verts[3][0],y + 30 + verts[3][1])
						ctx.stroke()
						
						ctx.set_line_width(10)
						ctx.set_source_rgb(*wht)
						ctx.arc(x + 30,y + 30,26,(3./2 + 2 * (1 - v)) * math.pi,3./2 * math.pi)
						ctx.stroke()
						
						ctx.set_line_width(2)
						ctx.set_source_rgb(*grn)
						ctx.arc(x + 30,y + 30,23,(3./2 + 2 * (1 - v)) * math.pi,3./2 * math.pi)
						ctx.stroke()
						
						verts = [
							[21 * math.cos(3./2 * math.pi),21 * math.sin(3./2 * math.pi)],
							[21 * math.cos((3./2 + 2 * (1 - v)) * math.pi),21 * math.sin((3./2 + 2 * (1 - v)) * math.pi)],
							[31 * math.cos(3./2 * math.pi),31 * math.sin(3./2 * math.pi)],
							[31 * math.cos((3./2 + 2 * (1 - v)) * math.pi),31 * math.sin((3./2 + 2 * (1 - v)) * math.pi)],
						]
						
						ctx.set_line_width(2)
						ctx.set_source_rgb(*wht)
						ctx.move_to(x + 30 + verts[0][0],y + 30 + verts[0][1])
						ctx.line_to(x + 30 + verts[2][0],y + 30 + verts[2][1])
						ctx.move_to(x + 30 + verts[1][0],y + 30 + verts[1][1])
						ctx.line_to(x + 30 + verts[3][0],y + 30 + verts[3][1])
						ctx.stroke()
					
					ctx.set_line_width(2)
					ctx.set_source_rgb(*red)
					ctx.move_to(x + 20,y + 20)
					ctx.line_to(x + 40,y + 40)
					ctx.move_to(x + 40,y + 20)
					ctx.line_to(x + 20,y + 40)
					ctx.stroke()
					
					ctx.set_line_width(1)
					ctx.set_source_rgb(*org)
					ctx.move_to(x + 20,y + 20)
					ctx.line_to(x + 40,y + 40)
					ctx.move_to(x + 40,y + 20)
					ctx.line_to(x + 20,y + 40)
					ctx.stroke()
					
					ctx.set_line_width(3)
					ctx.set_source_rgba(*(wht + (.75,)))
					ctx.move_to(x + 19,y + 19)
					ctx.line_to(x + 41,y + 41)
					ctx.move_to(x + 41,y + 19)
					ctx.line_to(x + 19,y + 41)
					ctx.stroke()
			
			return 48 if v == 1 else 60
		
		
		def cont2((x,y,w),i):
			y_prev = y
			
			fcirc(x + 5,y + 5,5,grn)
			
			y += text_paragraph((x + 20,y,w - 20),blk,f1,u"%d/%d konform" % (n,d))
			
			if v != 1:
				y += blank(1)
				
				fcirc(x + 5,y + 5,5,red)
				fcirc(x + 5,y + 5,4,org)
				
				y += text_paragraph((x + 20,y,w - 20),blk,f2,u"%d/%d nicht konform" % (d - n,d))
			
			return y - y_prev
		
		
		y += column((x,y,w),(10,60,20,-1),(None,cont1,None,cont2))
		
		return y - y_prev
	
	
	def feature_table((x,y,w),features,occurrences): # box, features, occurrences #TODO use table()
		y_prev = y
		x_prev = x
		w_prev = w
		
		y_starts = [0] * len(features)
		
		for i,feature in enumerate(features):
			y += text_paragraph((x,y,w),blk,f2,feature)
			y += blank(1)
			
			y_starts[i] = y
			
			x += 20
			w -= 20
		
		y_end = y + 5
		x = x_prev
		w = w_prev
		
		for i,feature in enumerate(features):
			frect(x + 4,x + 6,y_starts[i],y_end,ccn(i))
			x += 20
		
		x = x_prev
		
		for i,feature in enumerate(features):
			ftri(x + 5,y + 5,5,1./2 * math.pi,ccn(i))
			x += 20
		
		y += 10
		
		for j,occurrence in enumerate(occurrences[:8]):
			x = x_prev
			y += blank(1)
			
			for i,feature in enumerate(occurrence[0]):
				if feature:
					fcirc(x + 5,y + 5,4,ccn(i))
				else:
					fcirc(x + 5,y + 5,.5,blk)
				x += 20
			
			x += 10
			
			y += text_paragraph((x,y,w - 130),blk,f1,occurrence[1])
		
		y += blank(1)
		x = x_prev
		
		for i,feature in enumerate(features):
			ftri(x + 5,y + 5,5,-1./2 * math.pi,ccn(i))
			x += 20
		
		y += 10
		x = x_prev
		
		if len(occurrences) > 8:
			y += blank(1)
			y += text_paragraph((x,y,w),blk,f1,u"Die Tabelle wurde auf 8 Einträge gekürzt, von zuvor %d Einträgen." % len(occurrences))
		
		return y - y_prev


