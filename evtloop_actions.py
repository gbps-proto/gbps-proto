

def actionNew(arg):
	pass

def actionOpen(arg):
	
	dialog = wx.FileDialog(None,u"Tab öffnen",style = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
	qtn = dialog.ShowModal() == wx.ID_OK
	wx.Frame(None).Destroy()
	if qtn:
		print dialog.GetPath()

def actionSave(arg): # arg: {set_ct}
	
	if ("permanent" in files[ct][3] and files[ct][3]["permanent"]):
		return False
	
	dialog = wx.FileDialog(None,u"Tab öffnen",style = wx.FD_SAVE)
	qtn = dialog.ShowModal() == wx.ID_OK
	wx.Frame(None).Destroy()
	if qtn:
		print dialog.GetPath()
	
	qtn = wx.MessageBox(u"Die ausgewählte Datei existiert bereits. Möchten Sie die Datei überschreiben?\n\nDadurch geht der aktuelle Inhalt der Datei verloren.",u"Datei überschreiben",wx.YES_NO | wx.YES_DEFAULT | wx.ICON_WARNING) == wx.YES
	wx.Frame(None).Destroy()
	if not qtn:
		return False

def actionSaveAll(arg):
	pass

def actionClose(arg): # arg: {set_ct}
	global ct,files,titles,lines,selects,scrolls1,histories,ctx1,closed_tabs,closed_tabs_index
	
	if ("permanent" in files[ct][3] and files[ct][3]["permanent"]):
		return False
	
	qtn = wx.MessageBox(u"Der aktuelle Tabinhalt wurde noch nicht gespeichert. Möchten Sie die Datei vor dem Schließen speichern?",u"Datei speichern",wx.YES_NO | wx.YES_DEFAULT | wx.ICON_QUESTION) == wx.YES
	wx.Frame(None).Destroy()
	if qtn\
	and not actionSave(None):
		return
	
	tab = {
		"files": files[ct],
		"titles": titles[ct],
		"lines": lines[ct],
		"selects": selects[ct],
		"scrolls1": scrolls1[ct],
		"histories": histories[ct],
		"ctx1": ctx1[ct],
	}
	closed_tabs += [tab]
	closed_tabs_index[files[ct][2]] = tab;
	
	del files[ct]
	del titles[ct]
	del lines[ct]
	del selects[ct]
	del scrolls1[ct]
	del histories[ct]
	del ctx1[ct]
	
	ct = min(ct,len(titles) - 1)
	
	if not len(titles):
		actionNew(None)
	
	
	actionTab(ct)

def actionRestore(arg):
	global ct,closed_tabs_index
	
	# TODO list of closed tabs -> restore, delete permanently
	
	if not closed_tabs:
		return
	
	tab = pop.closed_tabs()
	del closed_tabs_index[tab["files"][2]]
	
	files[ct + 1:ct + 1] = tab["files"]
	titles[ct + 1:ct + 1] = tab["titles"]
	lines[ct + 1:ct + 1] = tab["lines"]
	selects[ct + 1:ct + 1] = tab["selects"]
	scrolls1[ct + 1:ct + 1] = tab["scrolls1"]
	histories[ct + 1:ct + 1] = tab["histories"]
	ctx1[ct + 1:ct + 1] = tab["ctx1"]
	
	ct += 1
	
	
	actionTab(ct)

def actionTitle(arg): # arg: {set_ct}
	global request_layout
	
	if ("permanent" in files[ct][3] and files[ct][3]["permanent"]):
		return
	
	title = wx.GetTextFromUser(u"Titel festlegen",default_value = titles[ct])
	wx.Frame(None).Destroy()
	
	print repr(title)
	if title:
		titles[ct] = title
	
	request_layout |= {0}

def actionSettings(arg):
	pass

# ---

def actionUndo(arg):
	global request_parse,dirty_editor,scroll_to_caret,request_layout
	
	## TODO snapshot and patch
	
	
	request_parse = True
	dirty_editor = True
	
	scroll_to_caret = True
	
	request_layout |= {3}

def actionRedo(arg):
	global request_parse,dirty_editor,scroll_to_caret,request_layout
	
	## TODO snapshot and patch
	
	
	request_parse = True
	dirty_editor = True
	
	scroll_to_caret = True
	
	request_layout |= {3}

def actionCut(arg):
	
	actionCopy(True)

def actionCopy(cut):
	global request_parse,dirty_editor,scroll_to_caret,request_layout
	
	if select[0][0] == select[1][0]:
		textt = "".join(get_text(lines[ct][select[select_i1][0]]))[select[select_i1][1]:select[select_i2][1]]
	else:
		textt = "\n".join(
			("".join(get_text(lines[ct][select[select_i1][0]]))[select[select_i1][1]:],) +\
			tuple("".join(get_text(line)) for line in lines[ct][select[select_i1][0] + 1:select[select_i2][0]]) +\
			("".join(get_text(lines[ct][select[select_i2][0]]))[:select[select_i2][1]],)\
		)
	
	cl = caret_line[select[select_i1][0]]
	
	textt = "\t" * cl[1] + textt
	
	pygame.scrap.put("text/plain;charset=utf-8",textt.encode("utf-8","ignore"))
	pygame.scrap.put("UTF8_STRING",textt.encode("utf-8","ignore"))
	
	if cut\
	and select[0] != select[1]: # 'x' cut, remove selection
		seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
		seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
		lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
			lines[ct][select[select_i1][0]][:seg1[0]] + [
				lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
				lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
			] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
		]
		select[select_i2] = list(select[select_i1])
		
		
		request_parse = True
		dirty_editor = True
	
	
	scroll_to_caret = True
	
	request_layout |= {3}

def actionPaste(arg):
	global request_parse,dirty_editor,scroll_to_caret,request_layout
	
	if select[0] != select[1]: # remove selection
		seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
		seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
		lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
			lines[ct][select[select_i1][0]][:seg1[0]] + [
				lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
				lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
			] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
		]
		select[select_i2] = list(select[select_i1])
	
	textt = u""
	try: textt = pygame.scrap.get("text/plain;charset=utf-8").decode("utf-8","ignore")
	except: pass
	try: textt = pygame.scrap.get("UTF8_STRING").decode("utf-8","ignore")
	except: pass
	
	cl = caret_line[select[1][0]]
	
	textt = [[x] for x in textt.split("\n")]
	
	match = re.search("(\\t*)",textt[0][0])
	ind = len(match.group(1))
	ind = (textt[0][0][ind:],ind)
	
	textt[0] = [ind[0]]
	for i,x in enumerate(textt):
		if i:
			if cl[1] > ind[1]:
				x[0] = "\t" * (cl[1] - ind[1]) + x[0]
			elif ind[1] > cl[1]:
				for j in xrange(ind[1] - cl[1]):
					if x[0][:1] == "\t":
						x[0] = x[0][1:]
	pos = len(textt[-1][0])
	
	line = lines[ct][select[1][0]]
	
	seg1 = get_segment(line,select[1][1])
	textt[0] = line[:seg1[0]] + [line[seg1[0]][:seg1[1]]] + textt[0]
	textt[-1] = textt[-1] + [line[seg1[0]][seg1[1]:]] + line[seg1[0] + 1:]
	lines[ct][select[1][0]:select[1][0] + 1] = textt
	
	select[1] = list(select[select_i1])
	if len(textt) > 1:
		select[1][0] += len(textt) - 1
		select[1][1] = 0
	select[1][1] += pos
	
	select[1][2] = -1
	
	select[0] = list(select[1])
	
	
	request_parse = True
	dirty_editor = True
	
	scroll_to_caret = True
	
	request_layout |= {3}

def actionSelectAll(arg):
	global request_layout
	
	cl = caret_line[0]
	
	select[:] = [[0,cl[1],-1],[len(lines[ct]) - 1,len("".join(get_text(lines[ct][-1]))),-1]]
	
	
	request_layout |= {3}

# ---

def actionTab(arg):
	global ct,scroll3,times1,request_parse,discard,dirty_editor,request_layout,request_save,scroll1_resize
	
	ct = arg
	scroll3 = 1.
	times1 = []
	
	
	request_parse = True
	discard = 2 # discard events until reparse
	dirty_editor = True
	
	request_layout |= {0}
	
	request_save = True
	
	scroll1_resize = True

def actionAccordion(arg):
	global ca,timer_set,request_layout
	
	ca = arg
	anim_ctrs[4] = max(3,anim_ctrs[4]) # start animation
	
	pygame.time.set_timer(pygame.USEREVENT,0) # shift animation
	timer_set = False
	
	
	request_layout |= {4}


